//
//  StoryCameraVC.swift
//  AiyaCameraSwift
//
//  Created by Jasmin Patel on 03/01/18.
//  Copyright © 2018 Simform. All rights reserved.
//

import UIKit
import AiyaEffectSDK
import AVFoundation
import Pageboy
import TGPControls
import Toast_Swift
import Photos
import PhotosUI
import MobileCoreServices
import SCRecorder

enum CollectionMode {
    case effect
    case style
}

class EffectData {
    var name: String
    var image: UIImage
    var path: String

    init(name: String, image: UIImage, path: String) {
        self.name = name
        self.image = image
        self.path = path
    }
}

class StyleData {
    var name: String
    var image: UIImage
    var styleImage: UIImage?
    
    init(name: String, image: UIImage, styleImage: UIImage?) {
        self.name = name
        self.image = image
        self.styleImage = styleImage
    }
}

public enum CamaraMode
{
    case boom
    case video
    case image
    case rewind
    case handFree
    case timer
    case photoTimer
    case stopMotion
    case margeAllVideo
    case slideShow
}

extension TGPDiscreteSlider {
    var speedType: VideoSpeedType {
        set { }
        get {
            switch value {
            case 0:
                return .slow(scaleFactor: 3.0)
            case 1:
                return .slow(scaleFactor: 2.0)
//            case 2:
//                return .slow(scaleFactor: 2.0)
            case 2:
                return .normal
            case 3:
                return .fast(scaleFactor: 2.0)
            case 4:
                return .fast(scaleFactor: 3.0)
            case 5:
                return .fast(scaleFactor: 4.0)
            default:
                return .normal
            }
        }
    }
}

protocol OuttakesTakenDelegate: class {
    func didTakeOuttakes(_ message: String)
}

extension StoryCameraVC: UploadManagerDelegate {

    func uploadDidUpdateProgress(_ uploadModel: UploadTask, index: Int) {
        self.setProgressView(uploadModel: uploadModel)
        AppEventBus.post("ProgressTask", sender: uploadModel)
    }

    func setProgressView(uploadModel: UploadTask) {
        self.showHideProgressView()
        let remainingUploadtask = findRunnigTaskToUpload()
        if remainingUploadtask.count != 0 {
            let dictionary = Dictionary(grouping: remainingUploadtask, by: { $0.token! })
            print(dictionary)
            let remainingUploadtaskRemainng = Array(dictionary.values)
            var sectionValue: Int = 0
            for (section, item) in remainingUploadtaskRemainng.enumerated() {
                for (_, items) in item.enumerated() {
                    if items.id == uploadModel.id {
                        sectionValue = section
                    }
                }
            }
            var remainingUploadtask: [UploadTask] = []
            for (index, _) in remainingUploadtaskRemainng[sectionValue].enumerated() {
                let uploadModel = remainingUploadtaskRemainng[sectionValue][index]
                remainingUploadtask.append(uploadModel)
            }

            if (appDelegate?.connectedToNetwork())! {
                if StoryUploadManager.sharedManager.currrentUploadTask != nil {
                    if remainingUploadtask.count != 0 {
                        if StoryUploadManager.sharedManager.currrentUploadTask?.token == remainingUploadtask[0].token {
                            DispatchQueue.main.async {
                                self.storyUploadImageView?.image = StoryUploadManager.sharedManager.currrentUploadTask!.image
                            }
                        }
                        else {
                            DispatchQueue.main.async {
                                self.storyUploadImageView?.image = remainingUploadtask[0].image
                            }
                        }
                    }
                    else {
                        DispatchQueue.main.async {
                            self.storyUploadImageView?.image = StoryUploadManager.sharedManager.currrentUploadTask!.image
                        }
                    }
                }
            }

            var progres: Float = 0.0

            if remainingUploadtask.count != 0 {
                DispatchQueue.main.async {
                    self.storyUploadImageView?.image = remainingUploadtask[0].image
                }
            }

            for item in remainingUploadtask {
                progres = progres + item.progress
                if item.state == .Running {
                    DispatchQueue.main.async {
                        self.storyUploadImageView?.image = item.image
                    }
                }
            }

            progres = progres / (Float(remainingUploadtask.count))
            print("New Progress Persiontage : \(Float(progres / 100))")
            let compteed = remainingUploadtask.filter { $0.state == .Completed }

//            var remaining: [UploadTask] = []
//            for (_, task) in StoryUploadManager.sharedManager.uploadStoryQueue.enumerated() {
//                if StoryUploadManager.sharedManager.currrentUploadTask?.token == task.token {
//                    remaining.append(task)
//                }
//            }

            DispatchQueue.main.async {
                self.lblStoryCount.text = "\(compteed.count+1) / \(remainingUploadtask.count)"
                self.lblStoryPercentage.text = "\(Int((StoryUploadManager.sharedManager.currrentUploadTask!.progress)))%"

                // self.storyUploadProgress.updateProgress(CGFloat(progres / 100))
            }

        }
    }

    func findRunnigTaskToUpload() -> [UploadTask] {
        var remainingUploadtask: [UploadTask] = []
        for (_, task) in StoryUploadManager.sharedManager.uploadStoryQueue.enumerated() {
            remainingUploadtask.append(task)
        }
        return remainingUploadtask
    }
}

extension StoryCameraVC: OuttakesTakenDelegate {
    func didTakeOuttakes(_ message: String) {
        self.view.makeToast(message, duration: 2.0, position: .bottom)
    }

}

enum TimerType: Int {
    
    case timer = 1
    case segmentLength
    case pauseTimer
    case photoTimer
}

class SelectedTimer: SaveInUserDefaults {
    
    var value: String = ""
    var selectedRow: Int = 0
    
    init(value: String, selectedRow: Int) {
        super.init()
        self.value = value
        self.selectedRow = selectedRow
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func encode(with aCoder: NSCoder) {
        super.encode(with: aCoder)
    }
    
}

class StoryCameraVC: UIViewController {

    // MARK: IBOutlets
    
    @IBOutlet weak var settingsView: UIStackView!
    
    @IBOutlet weak var timerValueView: UIView!
    @IBOutlet weak var faceFiltersView: UIStackView!
    @IBOutlet weak var sceneFilterView: UIStackView!
    @IBOutlet weak var friendsView: UIStackView!
    @IBOutlet weak var goLiveView: UIStackView!
    @IBOutlet weak var slideShowView: UIStackView!

    @IBOutlet weak var photoTimerSelectedLabel: UILabel!
    @IBOutlet weak var pauseTimerSelectedLabel: UILabel!
    @IBOutlet weak var timerSelectedLabel: UILabel!
    @IBOutlet weak var segmentLengthSelectedLabel: UILabel!
    @IBOutlet weak var photoTimerSelectionLabel: UILabel!
    @IBOutlet weak var pauseTimerSelectionLabel: UILabel!
    @IBOutlet weak var timerSelectionLabel: UILabel!
    @IBOutlet weak var segmentLengthSelectionLabel: UILabel!
    @IBOutlet weak var photoTimerSelectionButton: UIButton!
    @IBOutlet weak var pauseTimerSelectionButton: UIButton!
    @IBOutlet weak var timerSelectionButton: UIButton!
    @IBOutlet weak var segmentLengthSelectionButton: UIButton!
    @IBOutlet weak var selectTimersView: UIView!
    @IBOutlet weak var timerPicker: PickerView! {
        didSet {
            timerPicker.dataSource = self
            timerPicker.delegate = self
            timerPicker.scrollingStyle = .default
            timerPicker.selectionStyle = .overlay
            timerPicker.tintColor = UIColor.white
            timerPicker.selectionTitle.text = "seconds"
        }
    }
    
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var slowVerticalBar: UIView!
    @IBOutlet weak var fastVerticalBar: UIView!
    @IBOutlet weak var enableMicrophoneButton: UIButton!
    @IBOutlet weak var enableCameraButton: UIButton!
    @IBOutlet weak var enableAccessView: UIView!
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var effectButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    var recoredButtonCenterPoint: CGPoint = CGPoint.init()
    var recoredViewCenterPoint: CGPoint = CGPoint.init()
    var recoredBlurCenterPoint: CGPoint = CGPoint.init()

    @IBOutlet weak var recordBlurView: UIVisualEffectView!
    @IBOutlet weak var recordButton: RPCircularProgress? {
        didSet {
            recordButton?.trackTintColor = UIColor.white
            recordButton?.progressTintColor = UIColor.red
            recordButton?.thicknessRatio = 0.15
        }
    }
    @IBOutlet weak var lblFriends: UILabel!

    var avAssestArr = [URL]()
    let mergedVIdeoName: String = "Mergged"
    var isRecordButtonPressed: Bool = false
    var isAlreadySatrted: Bool = true
    var partitionCountNumber: Int = 1
    var totalDurationOfOneSegment: Double = 0
    var videoArray = [AVAsset]()
    
    @IBOutlet weak var selectedTimerLabel: UILabel!
    var isCountDownStarted: Bool = false {
        didSet {
            if let pageVC = self.navigationController?.parentPageViewController {
                if isCountDownStarted {
                    nextButton.isHidden = true
                    previousButton.isHidden = true
                    lblFriends.isHidden = true
                    pageVC.isScrollEnabled = false
                    handsfreeButton.isUserInteractionEnabled = false
//                    recordButton?.isUserInteractionEnabled = false
                }
                else {
                    nextButton.isHidden = false
                    previousButton.isHidden = false
                    lblFriends.isHidden = false
                    pageVC.isScrollEnabled = true
                    handsfreeButton.isUserInteractionEnabled = true
//                    recordButton?.isUserInteractionEnabled = true
                }
            }
        }
    }

    var isRecordStart: Bool = false {
        didSet {
            if let pageVC = self.navigationController?.parentPageViewController {
                if isRecordStart {
                    nextButton.isHidden = true
                    previousButton.isHidden = true
                    lblFriends.isHidden = true
                    pageVC.isScrollEnabled = false
                    if currentCamaraMode == .handFree || currentCamaraMode == .timer {
                        handsfreeButton.isUserInteractionEnabled = false
                    }
                    else
                    {
                        handsfreeButton.isUserInteractionEnabled = true
                    }
                }
                else {
                    nextButton.isHidden = false
                    previousButton.isHidden = false
                    lblFriends.isHidden = false
                    pageVC.isScrollEnabled = true
                    handsfreeButton.isUserInteractionEnabled = true
                }
            }
        }
    }

    lazy var btnOk: UIButton = {
        let buttonTemp = UIButton.init(frame: CGRect.init(x: self.view.frame.width / 2 - 30, y: 20, width: 60, height: 50))
        buttonTemp.setTitle("OK", for: UIControlState())
        buttonTemp.addTarget(self, action: #selector(btnOKClick), for: .touchUpInside)

        return buttonTemp
    }()

    
    @objc func btnOKClick(sender: UIButton!) {
        if self.takenImages.count != 0 {
            DispatchQueue.main.async {
                let previewVC = StoryPreviewViewController(nibName: "StoryPreviewViewController", bundle: nil)
                previewVC.storyCamDelegate = self.storyCamDelegate
                previewVC.image = self.takenImages[0]
                previewVC.images = self.takenImages
                previewVC.outtakesDelegate = self
                self.navigationController?.pushViewController(previewVC, animated: false)
            }
        }
    }

    @IBOutlet weak var handsFreeLabel: UILabel!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var fast2xBar: UILabel!
    @IBOutlet weak var slow2xBar: UILabel!
    @IBOutlet weak var speedLabel: UILabel!
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var topStackView: UIStackView!
    @IBOutlet weak var outtakesButton: UIButton!
    @IBOutlet weak var skinFilterButton: UIButton!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var previousButton: UIButton!
    @IBOutlet weak var goLiveButton: UIButton!
    @IBOutlet weak var slideShowButton: UIButton!
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var zoomSliderView: UIView! {
        didSet {
//            zoomSliderView.isHidden = true
        }
    }
    @IBOutlet weak var muteButton: UIButton!
    @IBOutlet weak var muteLabel: UILabel!
    @IBOutlet weak var timerButton: UIButton!
    @IBOutlet weak var handsfreeButton: UIButton!
    @IBOutlet weak var blinkView: UIView!
    var blinkStatus: Bool = false
    @IBOutlet weak var redRecordingView: UIView!
    @IBOutlet weak var controlView: UIView!
    @IBOutlet weak var bottomCollectionView: NSLayoutConstraint!
    @IBOutlet weak var flipButton: UIButton!

    @IBOutlet weak var flashButton: UIButton! {
        didSet {
            flashButton.setImage(#imageLiteral(resourceName: "flashOff"), for: UIControlState.normal)
        }
    }

    @IBOutlet weak var flipLabel: UILabel!
//    @IBOutlet weak var flashButton: UIButton!
    @IBOutlet weak var flashLabel: UILabel!

    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var transparentView: TransparentGradientView!
    @IBOutlet weak var zoomSlider: VSSlider!
    @IBOutlet weak var speedSliderLabels: TGPCamelLabels! {
        didSet {
            speedSliderLabels.names = ["-3x", "-2x", "1x", "2x", "3x"]
            speedSlider.ticksListener = speedSliderLabels
        }
    }
    @IBOutlet weak var speedSlider: TGPDiscreteSlider! {
        didSet {
//            speedSlider.tickImage = #imageLiteral(resourceName: "dimondSmall")
//            speedSlider.thumbImage = #imageLiteral(resourceName: "dimond")
            speedSlider.addTarget(self, action: #selector(speedSliderValueChanged(_:)), for: UIControlEvents.valueChanged)
        }
    }

    // MARK: Variables
    
    fileprivate var timerType = TimerType.segmentLength
    fileprivate var selectedTimerValue = SelectedTimer(value: "-", selectedRow: 0)
    fileprivate var selectedSegmentLengthValue = SelectedTimer(value: "240", selectedRow: 10)
    fileprivate var selectedPauseTimerValue = SelectedTimer(value: "-", selectedRow: 0)
    fileprivate var selectedPhotoTimerValue = SelectedTimer(value: "-", selectedRow: 0)
    fileprivate var isMute: Bool = false
   
    fileprivate var currentCamaraMode: CamaraMode = .boom {
        didSet {
            if currentCamaraMode == .slideShow && takenSlideShowImages.count > 0 {
                if let pageVC = self.navigationController?.parentPageViewController {
                    pageVC.isScrollEnabled = false
                    DispatchQueue.main.async {
                        self.handsfreeButton.isUserInteractionEnabled = false
                        self.timerButton.isUserInteractionEnabled = false
                        self.muteButton.isUserInteractionEnabled = false
                        UIView.animate(withDuration: 0.3, animations: {
                            self.handsfreeButton.alpha = 0.5
                            self.timerButton.alpha = 0.5
                            self.muteButton.alpha = 0.5
                        })
                    }
                }
                if self.handsfreeButton.isSelected {
                    DispatchQueue.main.async {
                        self.handsFreeLabel.text = "Hands-On"
                        self.handsFreeLabel.textColor = UIColor.white
                        self.handsfreeButton.isSelected = !self.handsfreeButton.isSelected
                        UserDefaults.standard.set(self.handsfreeButton.isSelected, forKey: "isHandsfreeRecord")
                    }
                }
            }
            else if currentCamaraMode == .handFree {
                DispatchQueue.main.async {
                    self.handsFreeLabel.text = "Hands-Free"
                    self.handsFreeLabel.textColor = UIColor.green8215284
                    UserDefaults.standard.set(self.handsfreeButton.isSelected, forKey: "isHandsfreeRecord")
                }
            }
            else {
                if let pageVC = self.navigationController?.parentPageViewController {
                    pageVC.isScrollEnabled = true
                    DispatchQueue.main.async {
                        self.handsfreeButton.isUserInteractionEnabled = true
                        self.timerButton.isUserInteractionEnabled = true
                        self.muteButton.isUserInteractionEnabled = true
                        UIView.animate(withDuration: 0.3, animations: {
                            self.handsfreeButton.alpha = 1
                            self.timerButton.alpha = 1
                            self.muteButton.alpha = 1
                        })
                    }
                }
                DispatchQueue.main.async {
                    self.handsFreeLabel.text = "Hands-On"
                    self.handsFreeLabel.textColor = UIColor.white
                    self.handsfreeButton.isSelected = false
                    UserDefaults.standard.set(self.handsfreeButton.isSelected, forKey: "isHandsfreeRecord")
                }
            }
        }
    }
    
    fileprivate var camera: Camera?
    fileprivate var dataProcess: CameraDataProcess?
    fileprivate var saveDataProcess: CameraDataProcess?
    fileprivate var preview: Preview?
    fileprivate var writer: MediaWriter?
    fileprivate var handler: AYEffectHandler?
    fileprivate var isWriteData = false
    fileprivate var isSnapshoot = false
    fileprivate var isFirstImage = false
    var isViewAppear = false
    fileprivate var isShowEffectCollectionView = false
    fileprivate var effectData = [EffectData]()
    fileprivate var styleData = [StyleData]()
    var collectionMode: CollectionMode = .effect
    fileprivate var pinchGestureRecognizer: UIPinchGestureRecognizer?
    let minimumZoom: CGFloat = 1.0
    var maximumZoom: CGFloat = 3.0
    var lastZoomFactor: CGFloat = 1.0
    fileprivate var photoTapGestureRecognizer: UITapGestureRecognizer?
    fileprivate var changeCamaraModeSwapUPGestureRecognizer: UISwipeGestureRecognizer?

    fileprivate var longPressGestureRecognizer: UILongPressGestureRecognizer?
    fileprivate var longPressTimerGestureRecognizer: UILongPressGestureRecognizer?
    fileprivate var focusTapGestureRecognizer: UITapGestureRecognizer?
    fileprivate var _panStartPoint: CGPoint = .zero
    fileprivate var _panStartZoom: CGFloat = 0.0
    fileprivate var gestureView: UIView?
    fileprivate var focusView: FocusIndicatorView?
    fileprivate var currentCameraPosition = AVCaptureDevicePosition.front
    fileprivate var flashMode: AVCaptureTorchMode = .off
    public var takenImage: UIImage?
    public var takenImages: [UIImage] = []
    public var takenVideoUrl: SegmentVideos?
    fileprivate var takenVideoUrlsSnapshoot = false
    public var takenVideoUrls: [SegmentVideos] = []
    public var takenSlideShowImages: [SegmentVideos] = []
    fileprivate var videoSpeedType = VideoSpeedType.normal
    fileprivate var isSpeedChanged = false
    var timer: Timer?
    
    var isFirstTime = true

    weak var storyCamDelegate: StoryCameraDelegate?

    @IBOutlet weak var horizontalScrollView: HorizontalScrollView?
    var stringArray: [[String]] = [["NORMAL", "BOOMERANG", "REWIND", "HAND-FREE", "STOP-MOTION"]]

    @IBOutlet var stopMotionCollectionView: UICollectionView!

    fileprivate var isStopConnVideo = false

    fileprivate var currentPage: Int = 0 {
        didSet {
            if self.currentPage <= self.takenImages.count {
            }
        }
    }
    
    var firstPersiontage: Double! = 0.0
    var firstUploadCompletedSize: Double! = 0.0
    
    @IBOutlet weak var lblStoryCount: UILabel!
    @IBOutlet weak var lblStoryPercentage: UILabel!

    @IBOutlet weak var storyUploadView: UIView!
//    @IBOutlet weak var storyUploadProgress: RPCircularProgress! {
//        didSet {
//            storyUploadProgress.trackTintColor = UIColor.clear
//            storyUploadProgress.progressTintColor = UIColor.init(red: 80 / 255, green: 144 / 255, blue: 63 / 255, alpha: 1)
//            storyUploadProgress.thicknessRatio = 0.25
//            storyUploadProgress.updateProgress(0.0)
//        }
//    }
    @IBOutlet weak var storyUploadRoundView: UIView!

    @IBOutlet weak var storyUploadImageView: UIImageView! {
        didSet {
            storyUploadImageView.layer.cornerRadius = 1
        }
    }

    fileprivate var pageSize: CGSize {
        let layout = self.stopMotionCollectionView.collectionViewLayout as! UPCarouselFlowLayout
        var pageSize = layout.itemSize
        if layout.scrollDirection == .horizontal {
            pageSize.width += layout.minimumLineSpacing
        } else {
            pageSize.height += layout.minimumLineSpacing
        }
        return pageSize
    }


    @IBOutlet weak var sfCountdownView: CountdownView! {
        didSet {
            sfCountdownView.delegate = self
            sfCountdownView.backgroundAlpha = 0.2
            sfCountdownView.countdownColor = UIColor.white
            sfCountdownView.countdownFrom = 3
            sfCountdownView.finishText = "Start"
            sfCountdownView.updateAppearance()
            sfCountdownView.isHidden = true
        }
    }

    let spiceList: [LCTuple<Int>] = [(key: 5, value: "5 Seconds"),
                                     (key: 10, value: "10 Seconds"),
                                     (key: 20, value: "20 Seconds"),
                                     (key: 30, value: "30 Seconds"),
                                     (key: 60, value: "60 Seconds")]

    let timeList: [LCTuple<Int>] = [(key: 10, value: "5 Seconds"),
                                    (key: 20, value: "10 Seconds"),
                                    (key: 40, value: "20 Seconds"),
                                    (key: 60, value: "30 Seconds"),
                                    (key: 120, value: "60 Seconds")]

    var selectedTimeList: LCTuple<Int> = (key: 20, value: "10 Seconds")

    var videoSegmentSeconds: CGFloat = 240*2

    var timerValue = 0
    
    var pauseTimerValue = 0
    
    var photoTimerValue = 0
    
    var timerOptions = ["-",
                        "1","2","3","4","5","6","7","8","9","10",
                        "11","12","13","14","15","16","17","18","19","20",
                        "21","22","23","24","25","26","27","28","29","30",
                        "31","32","33","34","35","36","37","38","39","40",
                        "41","42","43","44","45","46","47","48","49","50",
                        "51","52","53","54","55","56","57","58","59","60"]
    var segmentLengthOptions = ["Custom",
                                "Boomerang",
                                "5",
                                "10",
                                "20",
                                "30",
                                "60",
                                "90",
                                "120",
                                "150",
                                "180",
                                "210",
                                "240"]
    var pauseTimerOptions = ["-",
                        "1","2","3","4","5","6","7","8","9","10",
                        "11","12","13","14","15","16","17","18","19","20",
                        "21","22","23","24","25","26","27","28","29","30",
                        "31","32","33","34","35","36","37","38","39","40",
                        "41","42","43","44","45","46","47","48","49","50",
                        "51","52","53","54","55","56","57","58","59","60"]
    
    var photoTimerOptions = ["-",
                             "1","2","3","4","5","6","7","8","9","10",
                             "11","12","13","14","15","16","17","18","19","20",
                             "21","22","23","24","25","26","27","28","29","30",
                             "31","32","33","34","35","36","37","38","39","40",
                             "41","42","43","44","45","46","47","48","49","50",
                             "51","52","53","54","55","56","57","58","59","60"]
    
    var controlViewGesture: UILongPressGestureRecognizer?
    
    // MARK: ViewController lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        UIApplication.shared.isIdleTimerDisabled = true
        setCameraSettings()
        initAiyaLicense()
        initMediaWriter()
        initPreviewScreen()
        initCameraDataProcess()
        registerCells()
        initResourceData()
        initHorizonalBottomSlider()
        view.bringSubview(toFront: baseView)
        view.bringSubview(toFront: blurView)
        view.bringSubview(toFront: enableAccessView)
        view.bringSubview(toFront: selectTimersView)
        setupRecordButton()
        setUpFocusView()
        setUpPinchToZoom()
        checkPermissions()
        setUpSwapUP()
        setupCountDownView()
        DispatchQueue.main.async {
            self.storyUploadRoundView.layer.cornerRadius = 1
            self.storyUploadRoundView.startBlink()
        }
        self.setupLayout()
        self.currentPage = 0
        recoredButtonCenterPoint = (recordButton?.center)!
        recoredViewCenterPoint = (redRecordingView?.center)!
        recoredBlurCenterPoint = recordBlurView.center
        
        let backgroundQueue = DispatchQueue.global()
        let deadline = DispatchTime.now() + .milliseconds(1000)
        backgroundQueue.asyncAfter(deadline: deadline, qos: .background) {
            DispatchQueue.main.async {
                if StoryUploadManager.sharedManager.uploadQueue.count == 0 && StoryUploadManager.sharedManager.uploadStoryQueue.count == 0 {
                    StoryUploadManager.sharedManager.checkUpload()
                }
            }
        }
//
//        StoryUploadManager.sharedManager.delegate = self
//        StoryUploadManager.sharedManager.uploadAllCompletionHandler = { [weak self] (isComplated) in
//            guard let strongSelf = self else { return }
//            strongSelf.showHideProgressView()
//            AppEventBus.post("UploadAllTask", sender: self)
//        }
//
//        StoryUploadManager.sharedManager.removeItemHandler = { [weak self] (isComplated) in
//            guard let strongSelf = self else { return }
//            strongSelf.showHideProgressView()
//            AppEventBus.post("RemoveTask", sender: self)
//        }
//
//        self.showHideProgressView()
//
//        AppEventBus.onMainThread(self, name: "UploadAllTask") { [weak self] (result) in
//            guard let strongSelf = self else { return }
//            strongSelf.showHideProgressView()
//        }
//
//        AppEventBus.onMainThread(self, name: "RemoveTask") { [weak self] (result) in
//            guard let strongSelf = self else { return }
//            strongSelf.showHideProgressView()
//        }
//
//        AppEventBus.onMainThread(self, name: "ProgressTask") { [weak self] (result) in
//            guard let strongSelf = self else { return }
//            let uploadModel: UploadTask = result.object as! UploadTask
//
//            strongSelf.setProgressView(uploadModel: uploadModel)
//        }
//
    }
    
    func setCameraSettings() {
        if let flashmode = UserDefaults.standard.value(forKey: "flashMode") as? Int {
            flashMode = AVCaptureTorchMode(rawValue: flashmode) ?? .off
        } else {
            flashMode = .off
            UserDefaults.standard.set(flashMode.rawValue, forKey: "flashMode")
        }
        setupFlashUI()
        if let cameraPosition = UserDefaults.standard.value(forKey: "cameraPosition") as? Int {
            currentCameraPosition = AVCaptureDevicePosition(rawValue: cameraPosition) ?? .front
        } else {
            currentCameraPosition = .front
            UserDefaults.standard.set(currentCameraPosition.rawValue, forKey: "cameraPosition")
        }
        self.setCameraPositionUI()
        if let isMicOn = UserDefaults.standard.value(forKey: "isMicOn") as? Bool {
            isMute = !isMicOn
        } else {
            isMute = false
            UserDefaults.standard.set(!isMute, forKey: "isMicOn")
        }
        setupMuteUI()
        
        if let selectedTimerValue = SelectedTimer.loadWithKey(key: "selectedTimerValue") as? SelectedTimer {
            self.selectedTimerValue = selectedTimerValue
        } else {
            self.selectedTimerValue = SelectedTimer(value: "-", selectedRow: 0)
            self.selectedTimerValue.saveWithKey(key: "selectedTimerValue")
        }
        if selectedTimerValue.selectedRow == 0 {
            timerValue = 0
        } else {
            timerValue = Int(selectedTimerValue.value) ?? 0
        }

        if let selectedPauseTimerValue = SelectedTimer.loadWithKey(key: "selectedPauseTimerValue") as? SelectedTimer {
            self.selectedPauseTimerValue = selectedPauseTimerValue
        } else {
            self.selectedPauseTimerValue = SelectedTimer(value: "-", selectedRow: 0)
            self.selectedPauseTimerValue.saveWithKey(key: "selectedPauseTimerValue")
        }
        if selectedPauseTimerValue.selectedRow == 0 {
            pauseTimerValue = 0
        } else {
            pauseTimerValue = Int(selectedPauseTimerValue.value) ?? 0
        }

        if let selectedPhotoTimerValue = SelectedTimer.loadWithKey(key: "selectedPhotoTimerValue") as? SelectedTimer {
            self.selectedPhotoTimerValue = selectedPhotoTimerValue
        } else {
            self.selectedPhotoTimerValue = SelectedTimer(value: "-", selectedRow: 0)
            self.selectedPhotoTimerValue.saveWithKey(key: "selectedPhotoTimerValue")
        }
        if selectedPhotoTimerValue.selectedRow == 0 {
            photoTimerValue = 0
        } else {
            photoTimerValue = Int(selectedPhotoTimerValue.value) ?? 0
        }
        
        if let selectedSegmentLengthValue = SelectedTimer.loadWithKey(key: "selectedSegmentLengthValue") as? SelectedTimer {
            self.selectedSegmentLengthValue = selectedSegmentLengthValue
        } else {
            self.selectedSegmentLengthValue = SelectedTimer(value: "240", selectedRow: (segmentLengthOptions.count - 1))
            self.selectedSegmentLengthValue.saveWithKey(key: "selectedSegmentLengthValue")
        }
        if let segmentLenghValue = Int(selectedSegmentLengthValue.value) {
            currentCamaraMode = .video
            videoSegmentSeconds = CGFloat(segmentLenghValue * 2)
            segmentLengthSelectedLabel.text = selectedSegmentLengthValue.value
        } else if selectedSegmentLengthValue.value == "Custom" {
            currentCamaraMode = .margeAllVideo
            segmentLengthSelectedLabel.text = "C"
        } else if selectedSegmentLengthValue.value == "Boomerang" {
            currentCamaraMode = .boom
            segmentLengthSelectedLabel.text = "B"
        }

        pauseTimerSelectedLabel.text = selectedPauseTimerValue.value
        timerSelectedLabel.text = selectedTimerValue.value
        photoTimerSelectedLabel.text = selectedPhotoTimerValue.value

        if let isHandsfreeRecord = UserDefaults.standard.value(forKey: "isHandsfreeRecord") as? Bool {
            self.handsfreeButton.isSelected = isHandsfreeRecord
            self.setHandsfreeUI()
        } else {
            currentCamaraMode = .video
            UserDefaults.standard.set(handsfreeButton.isSelected, forKey: "isHandsfreeRecord")
        }

    }

    func showHideProgressView() {
        if StoryUploadManager.sharedManager.uploadStoryQueue.count == 0 {
            self.storyUploadView.isHidden = true
        }
        else {
            self.storyUploadView.isHidden = false
        }
    }

    fileprivate func setupLayout() {
        let layout = self.stopMotionCollectionView.collectionViewLayout as! UPCarouselFlowLayout
        layout.spacingMode = UPCarouselFlowLayoutSpacingMode.fixed(spacing: 0.1)

        stopMotionCollectionView.register(UINib(nibName: ImageCollectionViewCell.identifier, bundle: nil), forCellWithReuseIdentifier: ImageCollectionViewCell.identifier)

    }

    func setupCountDownView() {
        sfCountdownView = CountdownView.init(frame: self.view.bounds)
        self.view.addSubview(sfCountdownView)
    }

    func changePermissionButtonColor() {
        if AppSettings.isCameraEnabled {
            enableCameraButton.setTitleColor(UIColor.gray, for: .normal)
        } else {
            enableCameraButton.setTitleColor(UIColor(red: 0, green: 122 / 255, blue: 1, alpha: 1.0), for: .normal)
        }
        if AppSettings.isMicrophoneEnabled {
            enableMicrophoneButton.setTitleColor(UIColor.gray, for: .normal)
        } else {
            enableMicrophoneButton.setTitleColor(UIColor(red: 0, green: 122 / 255, blue: 1, alpha: 1.0), for: .normal)
        }
    }

    func checkPermissions() {
        changePermissionButtonColor()
        if AppSettings.isCameraEnabled && AppSettings.isMicrophoneEnabled {
            blurView.isHidden = true
            enableAccessView.isHidden = true
            initCamera()
        } else {
            blurView.isHidden = false
            enableAccessView.isHidden = false
        }

    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        camera?.stopCapture()
        isViewAppear = false
//        handler?.pauseEffect()
    }

    func removeCameraControl() {
        if camera != nil {
            camera?.stopCapture()
            self.camera = nil
        }
    }
    
    func addCameraControl() {
//        if camera != nil{
//            self.viewWillAppear(false)
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.isIdleTimerDisabled = true
        isViewAppear = true
        camera?.setTorchOn(.off)
        camera?.startCapture()
        observeState()
        self.speedSlider.isUserInteractionEnabled = true
        self.recordButton?.updateProgress(0.0)
//        self.zoomSliderView.isHidden = true
        slow2xBar.isHidden = true
        fast2xBar.isHidden = true
        slowVerticalBar.isHidden = true
        fastVerticalBar.isHidden = true
        self.speedLabel.textColor = UIColor.red
        self.speedLabel.text = ""
        self.speedLabel.stopBlink()
        self.redRecordingView?.transform = .identity
        self.recordBlurView.transform = .identity
        self.redRecordingView.backgroundColor = UIColor.clear
    
//

//        self.camera?.zoomFactor = 1.0
//        self.zoomSlider.value = 1.0

    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        StoryDataManager.shared.delegate = self
        StoryDataManager.shared.startUpload()
        PostDataManager.shared.startUpload()
        if sfCountdownView == nil {
            setupCountDownView()
        }
        UIApplication.shared.isStatusBarHidden = true
//        handler?.resumeEffect()
        
        if isFirstTime {
            isFirstTime = false
            self.onEffectClick("", styleImage: styleData[1].styleImage)
            self.onEffectClick("", styleImage: styleData[0].styleImage)
        }
        let button = recordButton
        let redRecView = redRecordingView
        let blurRecordView = recordBlurView
        button?.center = recoredButtonCenterPoint
        redRecView?.center = recoredViewCenterPoint
        blurRecordView?.center = recoredBlurCenterPoint
        
        if currentCamaraMode == .slideShow {
            currentCamaraMode = .slideShow
        }
        
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.isStatusBarHidden = false
        UIApplication.shared.isIdleTimerDisabled = false
        removeObserveState()
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func deSelectButtonWith(_ buttons: [UIButton]) {
        for button in buttons {
            button.isSelected = false
        }
    }
    
    func hideLabels(_ buttons: [UILabel]) {
        for button in buttons {
            button.isHidden = true
        }
    }

    // MARK: IBActions
    @IBAction func onSlideShowClick(sender: Any) {
        slideShowButton.isSelected = !slideShowButton.isSelected
        currentCamaraMode = slideShowButton.isSelected ? .slideShow : .video
    }
    
    @IBAction func btnDoneClick(sender: Any) {
        guard !isWriteData else {
            return
        }
        self.totalDurationOfOneSegment = 0.0
        self.mergeVideoArray()
//        btnDone.isHidden = true
    }
    
    @IBAction func onUploadsClick(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Story", bundle: nil)
        if let storyUploadVC = storyboard.instantiateViewController(withIdentifier: "StoryUploadVC") as? StoryUploadVC {
            storyUploadVC.firstModalPersiontage = firstPersiontage
            storyUploadVC.firstModalUploadCompletedSize = firstUploadCompletedSize
            navigationController?.pushViewController(storyUploadVC, animated: true)
        }
        
        
    }
    
    @IBAction func onCloseTimerView(_ sender: UIButton) {
        selectTimersView.isHidden = true
        switch timerType {
        case .timer:
            selectedTimerValue = SelectedTimer(value: timerOptions[timerPicker.currentSelectedIndex], selectedRow: timerPicker.currentSelectedIndex)
            self.selectedTimerValue.saveWithKey(key: "selectedTimerValue")
            if selectedTimerValue.selectedRow == 0 {
                timerValue = 0
            } else {
                timerValue = Int(selectedTimerValue.value) ?? 0
                if photoTimerValue > 0 {
                    photoTimerValue = 0
                    selectedPhotoTimerValue = SelectedTimer(value: photoTimerOptions[0],
                                                            selectedRow: 0)
                    self.selectedPhotoTimerValue.saveWithKey(key: "selectedPhotoTimerValue")
                }
            }

        case .pauseTimer:
            selectedPauseTimerValue = SelectedTimer(value: pauseTimerOptions[timerPicker.currentSelectedIndex], selectedRow: timerPicker.currentSelectedIndex)
            self.selectedPauseTimerValue.saveWithKey(key: "selectedPauseTimerValue")
            if selectedPauseTimerValue.selectedRow == 0 {
                pauseTimerValue = 0
            } else {
                pauseTimerValue = Int(selectedPauseTimerValue.value) ?? 0
            }
        case .segmentLength:
            selectedSegmentLengthValue = SelectedTimer(value: segmentLengthOptions[timerPicker.currentSelectedIndex], selectedRow: timerPicker.currentSelectedIndex)
            self.selectedSegmentLengthValue.saveWithKey(key: "selectedSegmentLengthValue")
            if let segmentLenghValue = Int(selectedSegmentLengthValue.value) {
                currentCamaraMode = .video
                videoSegmentSeconds = CGFloat(segmentLenghValue * 2)
                segmentLengthSelectedLabel.text = selectedSegmentLengthValue.value
            } else if selectedSegmentLengthValue.value == "Custom" {
                currentCamaraMode = .margeAllVideo
                segmentLengthSelectedLabel.text = "C"
                if timerValue > 0 {
                    timerValue = 0
                    resetCountDown()
                }
                if photoTimerValue > 0 {
                    photoTimerValue = 0
                    resetPhotoCountDown()
                }
            } else if selectedSegmentLengthValue.value == "Boomerang" {
                currentCamaraMode = .boom
                segmentLengthSelectedLabel.text = "B"
                if timerValue > 0 {
                    timerValue = 0
                    resetCountDown()
                }
                if photoTimerValue > 0 {
                    photoTimerValue = 0
                    resetPhotoCountDown()
                }
            }
        case .photoTimer:
            selectedPhotoTimerValue = SelectedTimer(value: photoTimerOptions[timerPicker.currentSelectedIndex], selectedRow: timerPicker.currentSelectedIndex)
            self.selectedPhotoTimerValue.saveWithKey(key: "selectedPhotoTimerValue")
            if selectedPhotoTimerValue.selectedRow == 0 {
                photoTimerValue = 0
            } else {
                photoTimerValue = Int(selectedPhotoTimerValue.value) ?? 0
                if timerValue > 0 {
                    timerValue = 0
                    selectedTimerValue = SelectedTimer(value: timerOptions[0],
                                                       selectedRow: 0)
                    self.selectedTimerValue.saveWithKey(key: "selectedTimerValue")
                }
            }
            if (timerValue > 0 && self.handsfreeButton.isSelected) || (photoTimerValue > 0 && self.handsfreeButton.isSelected) {
                self.currentCamaraMode = .video
            }

        }
        pauseTimerSelectedLabel.text = selectedPauseTimerValue.value
        timerSelectedLabel.text = selectedTimerValue.value
        photoTimerSelectedLabel.text = selectedPhotoTimerValue.value
        if (timerValue > 0 && self.handsfreeButton.isSelected) || (photoTimerValue > 0 && self.handsfreeButton.isSelected) {
            self.currentCamaraMode = .video
        } else if ((currentCamaraMode == .margeAllVideo || currentCamaraMode == .boom) && self.handsfreeButton.isSelected) {
            
            self.handsFreeLabel.text = "Hands-On"
            self.handsFreeLabel.textColor = UIColor.white
            self.handsfreeButton.isSelected = !self.handsfreeButton.isSelected
            UserDefaults.standard.set(handsfreeButton.isSelected, forKey: "isHandsfreeRecord")
        }
        
    }
    
    @IBAction func onTimerClick(_ sender: UIButton) {
        
        switch timerType {
        case .timer:
            selectedTimerValue = SelectedTimer(value: timerOptions[timerPicker.currentSelectedIndex], selectedRow: timerPicker.currentSelectedIndex)
            self.selectedTimerValue.saveWithKey(key: "selectedTimerValue")
            if selectedTimerValue.selectedRow == 0 {
                timerValue = 0
            } else {
                timerValue = Int(selectedTimerValue.value) ?? 0
                if photoTimerValue > 0 {
                    photoTimerValue = 0
                    selectedPhotoTimerValue = SelectedTimer(value: photoTimerOptions[0],
                                                            selectedRow: 0)
                    self.selectedPhotoTimerValue.saveWithKey(key: "selectedPhotoTimerValue")
                }
                if selectedSegmentLengthValue.value == "Custom" || selectedSegmentLengthValue.value == "Boomerang" {
                    self.currentCamaraMode = .video
                    self.handsFreeLabel.text = "Hands-On"
                    self.handsFreeLabel.textColor = UIColor.white
                    self.handsfreeButton.isSelected = false
                    UserDefaults.standard.set(handsfreeButton.isSelected, forKey: "isHandsfreeRecord")
                    self.selectedSegmentLengthValue = SelectedTimer(value: "240", selectedRow: (segmentLengthOptions.count - 1))
                    self.selectedSegmentLengthValue.saveWithKey(key: "selectedSegmentLengthValue")
                    videoSegmentSeconds = CGFloat((Int(self.selectedSegmentLengthValue.value) ?? 240) * 2)
                }
            }
        case .pauseTimer:
            selectedPauseTimerValue = SelectedTimer(value: pauseTimerOptions[timerPicker.currentSelectedIndex], selectedRow: timerPicker.currentSelectedIndex)
            self.selectedPauseTimerValue.saveWithKey(key: "selectedPauseTimerValue")
            if selectedPauseTimerValue.selectedRow == 0 {
                pauseTimerValue = 0
            } else {
                pauseTimerValue = Int(selectedPauseTimerValue.value) ?? 0
            }
        case .segmentLength:
            selectedSegmentLengthValue = SelectedTimer(value: segmentLengthOptions[timerPicker.currentSelectedIndex], selectedRow: timerPicker.currentSelectedIndex)
            self.selectedSegmentLengthValue.saveWithKey(key: "selectedSegmentLengthValue")
            if let segmentLenghValue = Int(selectedSegmentLengthValue.value) {
                currentCamaraMode = .video
                videoSegmentSeconds = CGFloat(segmentLenghValue * 2)
            } else if selectedSegmentLengthValue.value == "Custom" {
                if timerValue > 0 {
                    timerValue = 0
                    resetCountDown()
                }
                if photoTimerValue > 0 {
                    photoTimerValue = 0
                    resetPhotoCountDown()
                }
                currentCamaraMode = .margeAllVideo
            } else if selectedSegmentLengthValue.value == "Boomerang" {
                if timerValue > 0 {
                    timerValue = 0
                    resetCountDown()
                }
                if photoTimerValue > 0 {
                    photoTimerValue = 0
                    resetPhotoCountDown()
                }
                currentCamaraMode = .boom
            }
        case .photoTimer:
            selectedPhotoTimerValue = SelectedTimer(value: photoTimerOptions[timerPicker.currentSelectedIndex], selectedRow: timerPicker.currentSelectedIndex)
            self.selectedPhotoTimerValue.saveWithKey(key: "selectedPhotoTimerValue")
            if selectedPhotoTimerValue.selectedRow == 0 {
                photoTimerValue = 0
            } else {
                photoTimerValue = Int(selectedPhotoTimerValue.value) ?? 0
                if timerValue > 0 {
                    timerValue = 0
                    selectedTimerValue = SelectedTimer(value: timerOptions[0],
                                                       selectedRow: 0)
                    self.selectedTimerValue.saveWithKey(key: "selectedTimerValue")
                }
                if selectedSegmentLengthValue.value == "Custom" || selectedSegmentLengthValue.value == "Boomerang" {
                    self.currentCamaraMode = .video
                    self.handsFreeLabel.text = "Hands-On"
                    self.handsFreeLabel.textColor = UIColor.white
                    self.handsfreeButton.isSelected = false
                    UserDefaults.standard.set(handsfreeButton.isSelected, forKey: "isHandsfreeRecord")
                    self.selectedSegmentLengthValue = SelectedTimer(value: "240", selectedRow: (segmentLengthOptions.count - 1))
                    self.selectedSegmentLengthValue.saveWithKey(key: "selectedSegmentLengthValue")
                    videoSegmentSeconds = CGFloat((Int(self.selectedSegmentLengthValue.value) ?? 240) * 2)
                }
            }
        }

        sender.isSelected = !sender.isSelected
        timerType = TimerType(rawValue: sender.tag) ?? TimerType.timer
        timerPicker.reloadPickerView()
        switch timerType {
        case .timer:
            
            deSelectButtonWith([pauseTimerSelectionButton,
                                segmentLengthSelectionButton,
                                photoTimerSelectionButton])
            timerSelectionLabel.isHidden = false
            hideLabels([pauseTimerSelectionLabel,
                        segmentLengthSelectionLabel,
                        photoTimerSelectionLabel])
            timerPicker.selectRow(selectedTimerValue.selectedRow, animated: true)
            
        case .pauseTimer:
            deSelectButtonWith([timerSelectionButton,
                                segmentLengthSelectionButton,
                                photoTimerSelectionButton])
            pauseTimerSelectionLabel.isHidden = false
            hideLabels([timerSelectionLabel,
                        segmentLengthSelectionLabel,
                        photoTimerSelectionLabel])
            timerPicker.selectRow(selectedPauseTimerValue.selectedRow, animated: true)
            
        case .segmentLength:
            deSelectButtonWith([pauseTimerSelectionButton,
                                timerSelectionButton,
                                photoTimerSelectionButton])
            segmentLengthSelectionLabel.isHidden = false
            hideLabels([pauseTimerSelectionLabel,
                        timerSelectionLabel,
                        photoTimerSelectionLabel])
            
            timerPicker.selectRow(selectedSegmentLengthValue.selectedRow, animated: true)
         
        case .photoTimer:
            
            deSelectButtonWith([timerSelectionButton,
                                segmentLengthSelectionButton,
                                pauseTimerSelectionButton])
            photoTimerSelectionLabel.isHidden = false
            hideLabels([timerSelectionLabel,
                        segmentLengthSelectionLabel,
                        pauseTimerSelectionLabel])
            timerPicker.selectRow(selectedPhotoTimerValue.selectedRow, animated: true)

        }

        
    }
    
    @IBAction func onUpgrade(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Story", bundle: nil)
        let upgradeViewController = storyboard.instantiateViewController(withIdentifier: "UpgradeViewController") as! UpgradeViewController
        navigationController?.pushViewController(upgradeViewController, animated: true)
    }

    @IBAction func onStorySettings(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Story", bundle: nil)
        let storySettingsVC = storyboard.instantiateViewController(withIdentifier: "StorySettingsOptionsVC") as! StorySettingsOptionsVC
        storySettingsVC.firstPersiontage = firstPersiontage
        storySettingsVC.firstUploadCompletedSize = firstUploadCompletedSize
        navigationController?.pushViewController(storySettingsVC, animated: true)
    }

    @IBAction func timerButtonClicked(_ sender: UIButton) {
        guard !isWriteData else {
            return
        }
        guard !isCountDownStarted else {
            return
        }
        view.bringSubview(toFront: selectTimersView)
        selectTimersView.isHidden = false
        timerPicker.reloadPickerView()
        switch timerType {
        case .timer:
            deSelectButtonWith([pauseTimerSelectionButton,
                                segmentLengthSelectionButton,
                                photoTimerSelectionButton])
            timerPicker.selectRow(selectedTimerValue.selectedRow, animated: true)
        case .pauseTimer:
            deSelectButtonWith([timerSelectionButton,
                                segmentLengthSelectionButton,
                                photoTimerSelectionButton])
            timerPicker.selectRow(selectedPauseTimerValue.selectedRow, animated: true)
        case .segmentLength:
            deSelectButtonWith([pauseTimerSelectionButton,
                                timerSelectionButton,
                                photoTimerSelectionButton])
            timerPicker.selectRow(selectedSegmentLengthValue.selectedRow, animated: true)
        case .photoTimer:
            deSelectButtonWith([timerSelectionButton,
                                segmentLengthSelectionButton,
                                pauseTimerSelectionButton])
            timerPicker.selectRow(selectedPhotoTimerValue.selectedRow, animated: true)

        }
        return
        guard !isWriteData else {
            return
        }

        if timerValue != 0 {
            self.isCountDownStarted = false
            self.sfCountdownView.isHidden = true
            self.sfCountdownView.stop()
            timerValue = 0
//            timerButton.tintColor = UIColor.white
            handsfreeButton.isSelected = false
            currentCamaraMode = .video

            selectedTimerLabel.text = ""
            timerButton.setImage(#imageLiteral(resourceName: "timer"), for: UIControlState.normal)
            return
        }

        setupCustomPopover(for: sender)

    }

    func zoomSliderValueChanged(_ sender: Any) {
        camera?.zoomFactor = CGFloat(zoomSlider.value)
    }

    func speedSliderValueChanged(_ sender: Any) {
        videoSpeedType = speedSlider.speedType
        guard isWriteData else {
            return
        }
        switch speedSlider.value {
        case 0:
            self.speedLabel.text = "Slow 3x"
            self.speedLabel.startBlink()
            break
        case 1:
            self.speedLabel.text = "Slow 2x"
            self.speedLabel.startBlink()
            break
        case 2:
            self.speedLabel.text = ""
            self.speedLabel.stopBlink()
            break
        case 3:
            self.speedLabel.text = "Fast 2x"
            self.speedLabel.startBlink()
            break
        case 4:
            self.speedLabel.text = "Fast 3x"
            self.speedLabel.startBlink()
            break
        default:
            self.speedLabel.text = ""
            self.speedLabel.stopBlink()
            break
        }
    }

    @IBAction func effectButtonClicked(_ sender: UIButton) {
        if isShowEffectCollectionView {
            hidenCollectionView()
            isShowEffectCollectionView = false
        } else {
            showCollectionView()
            collectionView?.reloadData()
            isShowEffectCollectionView = true
        }
    }
    
    @IBAction func sceneFilterButtonClicked(_ sender: UIButton) {
        if isShowEffectCollectionView {
            hidenCollectionView()
            isShowEffectCollectionView = false
        } else {
            showCollectionView(CollectionMode.style)
            collectionView?.reloadData()
            isShowEffectCollectionView = true
        }
    }

    func showCollectionView(_ mode: CollectionMode = .effect) {
        self.collectionMode = mode
        collectionView.reloadData()
        bottomCollectionView.constant = 140
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }

    func hidenCollectionView() {
        bottomCollectionView.constant = -240
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }

    func setupCustomPopover(for sender: UIView) {
        self.isCountDownStarted = false
        self.sfCountdownView.isHidden = true
        self.sfCountdownView.stop()
        // Init a popover with a callbżck closure after selecting data
        let popover = LCPopover<Int>(for: timerLabel, title: "") { [weak self] tuple in
            guard let strongSelf = self, let tuple = tuple else { return }
            strongSelf.timerValue = tuple.key
//            strongSelf.timerButton.tintColor = UIColor.green8215284
            strongSelf.selectedTimerLabel.text = "\(tuple.key)"
            strongSelf.timerButton.setImage(#imageLiteral(resourceName: "storyTimer"), for: UIControlState.normal)
            if strongSelf.handsfreeButton.isSelected {
                strongSelf.currentCamaraMode = .video
            }
        }
        // Assign data to the dataList
        popover.dataList = spiceList
        popover.selectedColor = UIColor.white246
        popover.rawHeight = 52.0
        // Set popover properties
        popover.size = CGSize(width: 150, height: 312)
        popover.arrowDirection = .up
        popover.backgroundColor = .white
        popover.borderColor = .clear
        popover.borderWidth = 0
        popover.setTitleSection = "Countdown"
        popover.barHeight = 0
        popover.sectionTitleColor = .white
        popover.sectionBackgroundColor = .green8215284
        popover.titleFont = UIFont.boldSystemFont(ofSize: 12)
        popover.textFont = UIFont(name: "SFUIText-Semibold", size: 12) ?? UIFont.systemFont(ofSize: 17)
        popover.textColor = UIColor.black253246
        present(popover, animated: true, completion: nil)

    }

    func setupTimeSegmentCustomPopover(for sender: UIView) {
        self.isCountDownStarted = false
        if sfCountdownView != nil {
            self.sfCountdownView.isHidden = true
            self.sfCountdownView.stop()
        }
        // Init a popover with a callbżck closure after selecting data
        let popover = LCPopover<Int>(for: sender, title: "") { [weak self] tuple in
            guard let strongSelf = self, let tuple = tuple else { return }
            strongSelf.videoSegmentSeconds = CGFloat.init(tuple.key)
            strongSelf.selectedTimeList = (key: tuple.key, value: tuple.value)
        }
        // Assign data to the dataList
        popover.dataList = timeList
        popover.selectedData = selectedTimeList
        popover.selectedColor = UIColor.selectedGreen
        popover.rawHeight = 52.0
        // Set popover properties
        popover.size = CGSize(width: 150, height: 312)
        popover.arrowDirection = .up
        popover.sectionTitleColor = .green8215284
        popover.sectionBackgroundColor = .white
        popover.backgroundColor = .green8215284
        popover.view.backgroundColor = .green8215284
        popover.borderColor = .clear
        popover.borderWidth = 0
        popover.barHeight = 0
        popover.setTitleSection = "Segment Length"
        popover.titleFont = UIFont.boldSystemFont(ofSize: 12)
        popover.textFont = UIFont(name: "SFUIText-Semibold", size: 12) ?? UIFont.systemFont(ofSize: 17)
        popover.textColor = UIColor.white
        present(popover, animated: true, completion: nil)

    }


    @IBAction func muteButtonClicked(_ sender: Any) {
        isMute = !isMute
        setupMuteUI()
        UserDefaults.standard.set(!isMute, forKey: "isMicOn")
    }

    func setupMuteUI() {
        
        if isMute {
            muteButton.setImage(#imageLiteral(resourceName: "storyMute"), for: UIControlState.normal)
            muteLabel.text = "Mic Off"
        } else {
            muteButton.setImage(#imageLiteral(resourceName: "unmute"), for: UIControlState.normal)
            muteLabel.text = "Mic On"
        }

    }
    
    @IBAction func outTakeButtonClicked(_ sender: Any) {
        
        let photoPickerVC = PhotosPickerViewController.init()
        photoPickerVC.currentCamaraMode = currentCamaraMode
        photoPickerVC.delegate = self
//        photoPickerVC.dismissCompletion = {
//
//        }
        self.navigationController?.present(photoPickerVC, animated: true, completion: nil)
    }

    @IBAction func flipButtonClicked(_ sender: Any) {

        flipButton.isSelected = !flipButton.isSelected
        self.changeCamaraMode()
    }
    
    func resetCountDown() {
        selectedTimerValue.value = "-"
        selectedTimerValue.selectedRow = 0
        timerSelectedLabel.text = selectedTimerValue.value
        self.selectedTimerValue.saveWithKey(key: "selectedTimerValue")
    }
    
    func resetPauseCountDown() {
        selectedPauseTimerValue.value = "-"
        selectedPauseTimerValue.selectedRow = 0
        pauseTimerSelectedLabel.text = selectedPauseTimerValue.value
        self.selectedPauseTimerValue.saveWithKey(key: "selectedPauseTimerValue")
    }
    
    func resetPhotoCountDown() {
        selectedPhotoTimerValue.value = "-"
        selectedPhotoTimerValue.selectedRow = 0
        photoTimerSelectedLabel.text = selectedPhotoTimerValue.value
        self.selectedPhotoTimerValue.saveWithKey(key: "selectedPhotoTimerValue")
    }


    @IBAction func handsfreebuttonClicked(_ sender: Any) {
        if timerValue > 0 {
            timerValue = 0
            resetCountDown()
            //            timerButton.tintColor = UIColor.white
            selectedTimerLabel.text = ""
            timerButton.setImage(#imageLiteral(resourceName: "timer"), for: UIControlState.normal)
            
        }
        if photoTimerValue > 0 {
            photoTimerValue = 0
            resetPhotoCountDown()
            //            timerButton.tintColor = UIColor.white
            selectedTimerLabel.text = ""
            timerButton.setImage(#imageLiteral(resourceName: "timer"), for: UIControlState.normal)
            
        }
        handsfreeButton.isSelected = !handsfreeButton.isSelected
        self.setHandsfreeUI()
        UserDefaults.standard.set(handsfreeButton.isSelected, forKey: "isHandsfreeRecord")
        
    }
    
    func setHandsfreeUI() {
        
        if handsfreeButton.isSelected {
            
            if currentCamaraMode == .margeAllVideo || currentCamaraMode == .boom {
                self.selectedSegmentLengthValue = SelectedTimer(value: "240", selectedRow: (segmentLengthOptions.count - 1))
                self.selectedSegmentLengthValue.saveWithKey(key: "selectedSegmentLengthValue")
                videoSegmentSeconds = CGFloat((Int(self.selectedSegmentLengthValue.value) ?? 240) * 2)
                segmentLengthSelectedLabel.text = selectedSegmentLengthValue.value
            }
            
            currentCamaraMode = .handFree
            if isRecordStart {
                isRecordStart = true
            }
        } else {
            
            if currentCamaraMode == .margeAllVideo || currentCamaraMode == .boom {
               
            } else {
                 currentCamaraMode = .video
            }
        }
    }

    @IBAction func boomerangEnable(_ sender: Any) {
        recordButton!.tag = recordButton!.tag == 1 ? 0 : 1
        if recordButton!.tag == 1 {
            currentCamaraMode = .boom
            recordButton?.trackTintColor = UIColor.black
        }
        else
        {
            currentCamaraMode = .image
            recordButton?.trackTintColor = UIColor.white
        }
    }

    @IBAction func flashButtonClicked(_ sender: Any) {
        switch flashMode {
        case .on:
            flashMode = .off
        case .off:
            flashMode = .auto
        case .auto:
            flashMode = .on
        }
        self.setupFlashUI()
        UserDefaults.standard.set(flashMode.rawValue, forKey: "flashMode")

        if isWriteData {
            camera?.setTorchOn(flashMode)
        }

    }
    
    func setupFlashUI() {
        switch flashMode {
        case .off:
            flashButton.setImage(#imageLiteral(resourceName: "flashOff"), for: UIControlState.normal)
            flashLabel.text = "No Flash"
        case .auto:
            flashButton.setImage(#imageLiteral(resourceName: "flashAuto"), for: UIControlState.normal)
            flashLabel.text = "Auto Flash"
        case .on:
            flashButton.setImage(#imageLiteral(resourceName: "flashOn"), for: UIControlState.normal)
            flashLabel.text = "Flash"
        }
    }

    @IBAction func onNextClick(_ sender: Any) {
        if currentCamaraMode == .slideShow && takenSlideShowImages.count > 0 {
            if takenSlideShowImages.count < 6 {
                self.showAlert(alertMessage: "Minimum six images required to make slideshow video.")
                return
            }
            let storyboard = UIStoryboard.init(name: "PhotoEditor", bundle: nil)
            let photoEditor = storyboard.instantiateViewController(withIdentifier: "PhotoEditorViewController") as! PhotoEditorViewController
            
            photoEditor.storyCamDelegate = self.storyCamDelegate
            photoEditor.outtakesDelegate = self
            photoEditor.videoUrls = self.takenSlideShowImages
            photoEditor.currentCamaraMode = currentCamaraMode
            for i in 0...31 {
                photoEditor.stickers.append(UIImage(named: "storySticker_\(i)")!)
            }
            
            photoEditor.hiddenControls = [.crop, .share]
            
            self.navigationController?.pushViewController(photoEditor, animated: true)
            
            self.takenSlideShowImages.removeAll()
            self.takenImages.removeAll()
            self.takenVideoUrls.removeAll()
            self.stopMotionCollectionView.reloadData()

        }
        else if self.currentCamaraMode == .margeAllVideo && takenVideoUrls.count > 0
        {
            self.totalDurationOfOneSegment = 0.0
            self.mergeVideoArray()
        }
        else {
            if let pageVC = self.navigationController?.parentPageViewController {
                pageVC.scrollToPage(PageboyViewController.Page.next, animated: true)
            }
        }
    }

    @IBAction func onPreviousClick(_ sender: Any) {
        if let pageVC = self.navigationController?.parentPageViewController {
            pageVC.scrollToPage(PageboyViewController.Page.previous, animated: true)
        }
    }

    // MARK: EffectHandler
    func onEffectClick(_ path: String, styleImage: UIImage? = nil) {
        if handler == nil {
            handler = AYEffectHandler()
            handler?.verticalFlip = true
        }
        if let styleImg = styleImage {
            handler?.style = styleImg
            handler?.intensityOfStyle = 1.0
        } else {
            handler?.effectPlayCount = 0
            handler?.effectPath = path
            handler?.style = styleImage
            handler?.intensityOfStyle = 0.0
        }
    }

    @IBAction func enableCameraButtonClicked(_ sender: Any) {
        let authStatusCamera = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
        switch authStatusCamera {
        case .notDetermined:
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { [weak self] (result) in
                guard let strongSelf = self else { return }
                DispatchQueue.main.async {
                    if result && AppSettings.isMicrophoneEnabled {
                        strongSelf.initCamera()
                        strongSelf.camera?.startCapture()
                        strongSelf.blurView.isHidden = true
                        strongSelf.enableAccessView.isHidden = true
                    }
                    strongSelf.changePermissionButtonColor()
                }

            })
        case .denied, .restricted:
            print("denied")
            AppSettings.openAppSettingsUrl()
        default: break
        }

    }

    @IBAction func enableMicrophoneButtonClicked(_ sender: Any) {
        let authStatusCamera = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeAudio)
        switch authStatusCamera {
        case .notDetermined:
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeAudio, completionHandler: { (result) in
                DispatchQueue.main.async {
                    if result && AppSettings.isCameraEnabled {
                        self.initCamera()
                        self.camera?.startCapture()
                        self.blurView.isHidden = true
                        self.enableAccessView.isHidden = true
                    }
                    self.changePermissionButtonColor()
                }
            })
        case .denied, .restricted:
            print("denied")
            AppSettings.openAppSettingsUrl()
        default: break
        }
    }


}
// MARK: Setup Camera
extension StoryCameraVC {

    func initAiyaLicense() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.licenseMessage(_:)), name: NSNotification.Name.AiyaLicense, object: nil)
//        AYLicenseManager.initLicense("44c3a67691d5c980bac13c82091ca556")
        AYLicenseManager.initLicense("067ea67564164944b93e5e8825734781")
    }

    func initMediaWriter() {
        writer = MediaWriter()
    }

    func initHorizonalBottomSlider() {
        horizontalScrollView?.stringArray = stringArray
        horizontalScrollView?.CollectionViewClosuer()
        horizontalScrollView?.configureCollectionView()
        horizontalScrollView?.currentCell = { [weak self] (index) in
            guard let strongSelf = self else { return }
            if index == 4 {
                strongSelf.stopMotionCollectionView.isHidden = false
                strongSelf.view.addSubview(strongSelf.btnOk)
                strongSelf.recordButton?.tag = 4
                strongSelf.currentCamaraMode = .stopMotion
                strongSelf.recordButton?.trackTintColor = UIColor.white
            }
            else if index == 3 {
                strongSelf.stopMotionCollectionView.isHidden = true
                strongSelf.btnOk.removeFromSuperview()
                strongSelf.recordButton!.tag = 3
                strongSelf.currentCamaraMode = .handFree
                strongSelf.recordButton?.trackTintColor = UIColor.black
            }
            else if index == 2 {
                strongSelf.stopMotionCollectionView.isHidden = true
                strongSelf.btnOk.removeFromSuperview()
                strongSelf.recordButton!.tag = 2
                strongSelf.currentCamaraMode = .rewind
                strongSelf.recordButton?.trackTintColor = UIColor.white
            }
            else if index == 1 {
                strongSelf.stopMotionCollectionView.isHidden = true
                strongSelf.btnOk.removeFromSuperview()
                strongSelf.recordButton!.tag = 1
                strongSelf.currentCamaraMode = .boom
                strongSelf.recordButton?.trackTintColor = UIColor.black
                strongSelf.speedSlider.isHidden = true
                strongSelf.speedSliderLabels.isHidden = true
            } else if index == 0 {
                strongSelf.stopMotionCollectionView.isHidden = true
                strongSelf.btnOk.removeFromSuperview()
                strongSelf.recordButton!.tag = 0
                strongSelf.currentCamaraMode = .image
                strongSelf.recordButton?.trackTintColor = UIColor.white
                strongSelf.speedSlider.isHidden = false
                strongSelf.speedSliderLabels.isHidden = false
            }
        }
    }

    func initPreviewScreen() {
        var width = view.frame.width
        let height = view.frame.height

        if height == 812.0 {
            width = 457.0
            preview = Preview(frame: CGRect(x: -41.0, y: 0.0, width: width, height: height))
        } else {
            preview = Preview(frame: CGRect(x: 0.0, y: 0.0, width: width, height: height))
        }
        
        view.addSubview(preview ?? UIView())
    }

    func initCameraDataProcess() {
        dataProcess = CameraDataProcess()
        switch currentCameraPosition {
        case .back:
            dataProcess?.mirror = false
        case .front:
            dataProcess?.mirror = true
        default:
            break
        }
        dataProcess?.delegate = self

        saveDataProcess = CameraDataProcess()
        switch currentCameraPosition {
        case .back:
            saveDataProcess?.mirror = false
        case .front:
            saveDataProcess?.mirror = false
        default:
            break
        }
        saveDataProcess?.delegate = self

    }

    func initCamera() {
        camera = Camera(captureDevicePosition: currentCameraPosition)
        camera?.delegate = self
        camera?.setRate(60)
        camera?.zoomFactor = 1.0
        let maxZoom = min(10, self.camera?.getMaxZoomFactor() ?? 10)
        maximumZoom = CGFloat(maxZoom)
        zoomSlider.maximumValue = Float(maxZoom)
        zoomSlider.value = 1.0
    }

    func initResourceData() {
        let effectpath = URL(fileURLWithPath: Bundle.main.bundlePath).appendingPathComponent("EffectResources", isDirectory: true)
        var effectDirNameArr: [String] = []
        do {
            let directoryContents = try FileManager.default.contentsOfDirectory(at: effectpath, includingPropertiesForKeys: nil, options: [])
            for item in directoryContents {
                effectDirNameArr.append(item.lastPathComponent)
                print("Found \(item)")
            }
        } catch {
            print(error.localizedDescription)
        }

        let effectRootDirPath = URL(fileURLWithPath: Bundle.main.bundlePath).appendingPathComponent("EffectResources").path

        effectData.append(EffectData(name: "original", image: UIImage(named: "no_eff")!, path: ""))

        for effectDirName: String in effectDirNameArr {
            let path: String = URL(fileURLWithPath: effectRootDirPath).appendingPathComponent(URL(fileURLWithPath: effectDirName).appendingPathComponent("meta.json").path).path
            if !FileManager.default.fileExists(atPath: path) {
                continue
            }

            do {
                let jsonData: Data = try! NSData(contentsOfFile: path) as Data
                print("jsonData \(jsonData)")
                let dic = try JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers) as! [AnyHashable: Any]
                var effectImage = UIImage(named: "effect")!
                if let image: String = dic["image"] as? String {
                    if image != "" {
                        effectImage = UIImage(named: image)!
                    }
                }
                effectData.append(EffectData.init(name: dic["name"] as? String ?? "", image: effectImage, path: path))
            }
            catch {

            }

        }
        
        let styleRootDirPath = URL(fileURLWithPath: Bundle.main.bundlePath).appendingPathComponent("FilterResources/filter").path
        let styleIconRootDirPath = URL(fileURLWithPath: Bundle.main.bundlePath).appendingPathComponent("FilterResources/icon").path
        let styleFileNameArr = try? FileManager.default.contentsOfDirectory(atPath: styleRootDirPath)
        
        styleData.append(StyleData(name: "original", image: UIImage(named: "no_eff")!, styleImage: nil))
        
        for styleFileName in styleFileNameArr! {
            let stylePath = URL(fileURLWithPath: styleRootDirPath).appendingPathComponent(styleFileName).path
            let styleIconPath = URL(fileURLWithPath: styleIconRootDirPath).appendingPathComponent(URL(fileURLWithPath: URL(fileURLWithPath: styleFileName).deletingPathExtension().path).appendingPathExtension("png").path).path
            if !FileManager.default.fileExists(atPath: stylePath) || !FileManager.default.fileExists(atPath: styleIconPath) {
                continue
            }
            if let styleIcon = UIImage(contentsOfFile: styleIconPath),
                let styleImage = UIImage(contentsOfFile: stylePath) {
                
                let path = URL(fileURLWithPath: styleFileName).deletingPathExtension().path.substring(from: String.Index(encodedOffset: 3))
                print("\(path)")
                styleData.append(StyleData(name: path, image: styleIcon, styleImage: styleImage))
            }
            
        }
    }

    func registerCells() {
        collectionView?.register(UINib(nibName: "FilterCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: "FilterCollectionViewCell")
    }

    @objc func licenseMessage(_ notifi: Notification) {
        let result = notifi.userInfo![AiyaLicenseNotificationUserInfoKey] as! UInt
        switch AiyaLicenseResult(rawValue: result)! {
        case AiyaLicenseResult.success:
            print("License success")
        case AiyaLicenseResult.fail:
            print("License fail")
        }
    }
}
// MARK: Manage Application State
extension StoryCameraVC {

    func observeState() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.enterBackground), name: .UIApplicationDidEnterBackground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.enterForeground), name: .UIApplicationDidBecomeActive, object: nil)
    }

    func removeObserveState() {
        NotificationCenter.default.removeObserver(self, name: .UIApplicationDidEnterBackground, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIApplicationDidBecomeActive, object: nil)
    }

    func enterBackground(_ notifi: Notification) {
        if isViewAppear {
            camera?.stopCapture()
            self.isStopConnVideo = true
            self.stopRecording()
        }
    }

    func enterForeground(_ notifi: Notification) {

        if isViewAppear {
            camera?.startCapture()
        }

    }

}
// MARK: UICollectionViewDataSource
extension StoryCameraVC: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.stopMotionCollectionView {
            if currentCamaraMode == .slideShow {
                return takenSlideShowImages.count
            }
            if currentCamaraMode != .margeAllVideo {
                return takenVideoUrls.count
            }
            return 0
        }
        else {
            switch collectionMode {
            case .effect:
                return effectData.count
            case .style:
                return styleData.count
            }
            
        }
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.stopMotionCollectionView {
         
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageCollectionViewCell.identifier, for: indexPath) as! ImageCollectionViewCell
           
            let borderColor: CGColor! = UIColor.white.cgColor
            let borderWidth: CGFloat = 3
            
            cell.imagesStackView.tag = indexPath.row
            
            var images = [SegmentVideos]()

            if currentCamaraMode == .slideShow {
                images = [takenSlideShowImages[indexPath.row]]
            } else {
                images = takenVideoUrls[(indexPath as NSIndexPath).row].videos
            }

            
            let views = cell.imagesStackView.subviews
            for view in views {
                cell.imagesStackView.removeArrangedSubview(view)
            }
        
            cell.lblSegmentCount.text = String(indexPath.row + 1)
            
            for imageName in images {
                let mainView = UIView.init(frame: CGRect(x: 0, y: 3, width: 41, height: 52))
                
                let imageView = UIImageView.init(frame: CGRect(x: 0, y: 0, width: 41, height: 52))
                imageView.image = imageName.image
                imageView.contentMode = .scaleToFill
                imageView.clipsToBounds = true
                mainView.addSubview(imageView)
                cell.imagesStackView.addArrangedSubview(mainView)
            }
            
            cell.imagesView.layer.cornerRadius = 5
            cell.imagesView.layer.borderWidth = borderWidth
            cell.imagesView.layer.borderColor = borderColor
            
            return cell
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterCollectionViewCell", for: indexPath) as! FilterCollectionViewCell
            switch collectionMode {
            case .effect:
                cell.myCustomImage = effectData[indexPath.row].image
                cell.name = effectData[indexPath.row].name
            case .style:
                cell.myCustomImage = styleData[indexPath.row].image
                cell.name = styleData[indexPath.row].name
            }
            
            return cell
        }
    }


    // MARK: - UIScrollViewDelegate

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let layout = self.stopMotionCollectionView.collectionViewLayout as! UPCarouselFlowLayout
        let pageSide = (layout.scrollDirection == .horizontal) ? self.pageSize.width : self.pageSize.height
        let offset = (layout.scrollDirection == .horizontal) ? scrollView.contentOffset.x : scrollView.contentOffset.y
        currentPage = Int(floor((offset - pageSide / 2) / pageSide) + 1)
    }

}

extension StoryCameraVC: PhotosPickerViewControllerDelegate {
    func selectAlbum(collection: PHAssetCollection) {

    }

    func albumViewCameraRollUnauthorized() {

    }

    func albumViewCameraRollAuthorized() {

    }

    func dismissPhotoPicker(withTLPHAssets: [Image])
    {
        if withTLPHAssets.count != 0 {
            if withTLPHAssets[0].assetType == .image {
                if currentCamaraMode == .slideShow {
                    for image in withTLPHAssets {
                        self.takenSlideShowImages.append(SegmentVideos(urlStr: URL(string: "www.google.com")!, thumbimage: image.fullResolutionImage!, latitued: nil, longitued: nil, placeAddress: nil, numberOfSegement: String(self.takenSlideShowImages.count + 1), videoduration: nil, combineOneVideo: false))
                    }
                    DispatchQueue.main.async {
                        self.stopMotionCollectionView.isHidden = false
                        self.stopMotionCollectionView.reloadData()
                    }
                    if self.takenSlideShowImages.count == 20 {
                        self.onNextClick(UIButton())
                    }

                } else {
                    let storyboard = UIStoryboard.init(name: "PhotoEditor", bundle: nil)
                    let photoEditor = storyboard.instantiateViewController(withIdentifier: "PhotoEditorViewController") as! PhotoEditorViewController
                    photoEditor.image = withTLPHAssets[0].fullResolutionImage!
                    photoEditor.outtakesDelegate = self
                    photoEditor.storyCamDelegate = self.storyCamDelegate
                    for i in 0...31 {
                        photoEditor.stickers.append(UIImage(named: "storySticker_\(i)")!)
                    }
                    
                    photoEditor.hiddenControls = [.crop, .share]
                    
                    self.navigationController?.pushViewController(photoEditor, animated: false)
                    
                    self.takenImages.removeAll()
                    self.takenVideoUrls.removeAll()
                    self.stopMotionCollectionView.reloadData()

                }
            }
            else if withTLPHAssets[0].assetType == .video {

                let storyboard = UIStoryboard.init(name: "PhotoEditor", bundle: nil)
                let photoEditor = storyboard.instantiateViewController(withIdentifier: "PhotoEditorViewController") as! PhotoEditorViewController
                self.takenVideoUrls = [SegmentVideos.init(urlStr: withTLPHAssets[0].videoUrl!, thumbimage: withTLPHAssets[0].thumbImage, latitued: nil, longitued: nil, placeAddress: nil, numberOfSegement: "\(self.takenVideoUrls.count + 1)", videoduration: nil)]
                self.takenImages = [withTLPHAssets[0].thumbImage]
                photoEditor.storyCamDelegate = self.storyCamDelegate
                photoEditor.videoUrls = self.takenVideoUrls
                photoEditor.outtakesDelegate = self
                let recordSession = SCRecordSession()
                for url in self.takenVideoUrls {
                    let segment = SCRecordSessionSegment(url: url.url!, info: nil)
                    recordSession.addSegment(segment)
                }

                photoEditor.recordSession = recordSession

                for i in 0...31 {
                    photoEditor.stickers.append(UIImage(named: "storySticker_\(i)")!)
                }

                photoEditor.hiddenControls = [.crop, .share]

                self.navigationController?.pushViewController(photoEditor, animated: true)
                self.takenImages.removeAll()
                self.takenVideoUrls.removeAll()
                self.stopMotionCollectionView.reloadData()
            }

        }
    }
}

// MARK: UICollectionViewDelegate
extension StoryCameraVC: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.stopMotionCollectionView {
            let character = takenImages[(indexPath as NSIndexPath).row]
            print(character)
        }
        else {
            switch collectionMode {
            case .effect:
                self.onEffectClick(effectData[indexPath.row].path)
            case .style:
                self.onEffectClick("", styleImage: styleData[indexPath.row].styleImage)
            }
        }

    }
}
// MARK: Setup Focus View
extension StoryCameraVC {

    func setupRecordButton() {
        self.photoTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handlePhotoTapGestureRecognizer(_:)))

        self.longPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPressGestureRecognizer(_:)))

        self.longPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPressGestureRecognizer(_:)))

        if let recordButton = self.recordButton,
            let longPressGestureRecognizer = self.longPressGestureRecognizer,
            let photoTapGestureRecognizer = self.photoTapGestureRecognizer {
            recordButton.isUserInteractionEnabled = true
            recordButton.sizeToFit()

            longPressGestureRecognizer.delegate = self
            longPressGestureRecognizer.minimumPressDuration = 0.25
            longPressGestureRecognizer.allowableMovement = 10.0
            recordButton.addGestureRecognizer(longPressGestureRecognizer)
            recordButton.addGestureRecognizer(photoTapGestureRecognizer)
        }
    }

    func setUpPinchToZoom() {
        if let gestureView = self.baseView {
            gestureView.autoresizingMask = [.flexibleWidth, .flexibleBottomMargin]
            gestureView.backgroundColor = .clear
            self.pinchGestureRecognizer = UIPinchGestureRecognizer(target: self, action: #selector(pinch(_:)))
            if let focusTapGestureRecognizer = self.pinchGestureRecognizer {
                focusTapGestureRecognizer.delegate = self
                gestureView.addGestureRecognizer(focusTapGestureRecognizer)
            }
        }
    }

    func setUpSwapUP() {
        if let gestureView = self.baseView {
            gestureView.autoresizingMask = [.flexibleWidth, .flexibleBottomMargin]
            gestureView.backgroundColor = .clear

            self.changeCamaraModeSwapUPGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(handleSwapUPGestureRecognizer(_:)))

            if let focusTapGestureRecognizer = self.changeCamaraModeSwapUPGestureRecognizer {
                focusTapGestureRecognizer.direction = UISwipeGestureRecognizerDirection.up
                gestureView.addGestureRecognizer(focusTapGestureRecognizer)
                focusTapGestureRecognizer.delegate = self
                self.focusTapGestureRecognizer?.require(toFail: focusTapGestureRecognizer)
            }
        }
        if let timerbutton = self.timerButton {
            timerbutton.autoresizingMask = [.flexibleWidth, .flexibleBottomMargin]

            self.longPressTimerGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPressTimerGestureRecognizer(_:)))
            if let longPressGestureRecognizer = self.longPressTimerGestureRecognizer
                {
                timerbutton.isUserInteractionEnabled = true
                timerbutton.sizeToFit()

                longPressGestureRecognizer.delegate = self
                longPressGestureRecognizer.minimumPressDuration = 0.25
                longPressGestureRecognizer.allowableMovement = 10.0
                timerbutton.addGestureRecognizer(longPressGestureRecognizer)
            }
        }

    }

    func minMaxZoom(_ factor: CGFloat) -> CGFloat {
        return min(min(max(factor, minimumZoom), maximumZoom), CGFloat(camera?.getMaxZoomFactor() ?? 1.0))
    }

    func pinch(_ pinch: UIPinchGestureRecognizer) {
//        guard !isWriteData else {
//            return
//        }
        let newScaleFactor = minMaxZoom(pinch.scale * lastZoomFactor)
        zoomSlider.value = Float(newScaleFactor)
        switch pinch.state {
        case .began: fallthrough
        case .changed: camera?.zoomFactor = newScaleFactor
        case .ended:
            lastZoomFactor = minMaxZoom(newScaleFactor)
            camera?.zoomFactor = lastZoomFactor
        default: break
        }



    }

    func setUpFocusView() {
        self.focusView = FocusIndicatorView(frame: .zero)
        self.gestureView = UIView(frame: view.bounds)
        if let gestureView = self.baseView {
            gestureView.autoresizingMask = [.flexibleWidth, .flexibleBottomMargin]
            gestureView.backgroundColor = .clear
            self.focusTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleFocusTapGestureRecognizer(_:)))
            if let focusTapGestureRecognizer = self.focusTapGestureRecognizer {
                focusTapGestureRecognizer.delegate = self
                focusTapGestureRecognizer.numberOfTapsRequired = 1
                gestureView.addGestureRecognizer(focusTapGestureRecognizer)
            }
        }
    }

    @objc internal func handleFocusTapGestureRecognizer(_ gestureRecognizer: UIGestureRecognizer) {
        let tapPoint = gestureRecognizer.location(in: self.preview)

        if let focusView = self.focusView {
            var focusFrame = focusView.frame
            focusFrame.origin.x = CGFloat((tapPoint.x - (focusFrame.size.width * 0.5)).rounded())
            focusFrame.origin.y = CGFloat((tapPoint.y - (focusFrame.size.height * 0.5)).rounded())
            focusView.frame = focusFrame

            self.preview?.addSubview(focusView)
            self.view.bringSubview(toFront: focusView)
            focusView.startAnimation()
        }
//        camera?.focus(at: tapPoint)
        _ = Timer.schedule(delay: 1.0, handler: { (timer) in
            self.focusView?.stopAnimation()
        })
    }
}

// MARK: Setup Recording
var count = 30
var countDownTimer: Timer?
extension StoryCameraVC {

    func startVideoRecoredDesignChanges() {
        UIView.animate(withDuration: 0.50, delay: 0, options: .curveEaseInOut, animations: {
            self.recordButton?.transform = CGAffineTransform(scaleX: 2, y: 2)
            self.recordBlurView.transform = CGAffineTransform(scaleX: 2, y: 2)
        }) { (completed: Bool) in
        }
        self.redRecordingView.backgroundColor = UIColor.red
        UIView.animate(withDuration: 1.5, delay: 0, options: .curveEaseInOut, animations: {
            self.redRecordingView?.transform = CGAffineTransform(scaleX: 50, y: 50)
        }) { (completed: Bool) in
        }
        let mp4FileName = UUID().uuidString.replacingOccurrences(of: "-", with: "") + (".mp4")
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0] as String
        let filePath: String = "\(documentsDirectory)/\(mp4FileName)"
        writer?.setOutputURL(URL(fileURLWithPath: filePath))
        self.camera?.setTorchOn(flashMode)
        isWriteData = true
        speedSlider.isUserInteractionEnabled = (currentCamaraMode == .handFree || currentCamaraMode == .timer)
        hideControls()
    }

    func hideControls() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.5, animations: {
                self.nextButton.alpha = 0
                self.settingsView.alpha = 0
                self.goLiveView.alpha = 0
                self.slideShowView.alpha = 0
                self.timerValueView.alpha = 0
                self.friendsView.alpha = 0
                self.faceFiltersView.alpha = 0
                self.sceneFilterView.alpha = 0
            })
        }
    }
    
    func showControls() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.5, animations: {
                self.nextButton.alpha = 1
                self.settingsView.alpha = 1
                self.goLiveView.alpha = 1
                self.slideShowView.alpha = 1
                self.timerValueView.alpha = 1
                self.friendsView.alpha = 1
                self.faceFiltersView.alpha = 1
                self.sceneFilterView.alpha = 1
            })
        }

    }
    
    func startRecording() {
        self.isFirstImage = true
        var progress: CGFloat = 0.2
        if currentCamaraMode == .margeAllVideo {
            progress = (self.recordButton?.progress)! * 480.0
        }

        slow2xBar.isHidden = false
        fast2xBar.isHidden = false
        slowVerticalBar.isHidden = false
        fastVerticalBar.isHidden = false

        timer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true, block: { [weak self] (timer) in
            guard let strongSelf = self else { return }
            DispatchQueue.main.async {
                if let progress = strongSelf.recordButton?.progress,
                    Int(progress) == 1 {
                    DispatchQueue.main.async {
                        timer.invalidate()
                        strongSelf.timer?.invalidate()
                        strongSelf.timer = nil
                        strongSelf.recordButton?.updateProgress(0.0)
                        strongSelf.isStopConnVideo = false
                        strongSelf.stopRecording()

                    }
                } else {
                    DispatchQueue.main.async {
                        if strongSelf.isWriteData {
                            var updatedProgress: CGFloat = 0.0
                            if strongSelf.currentCamaraMode == .margeAllVideo {
                                if strongSelf.takenVideoUrls.count >= 1 {
//                                    strongSelf.btnDone.isHidden = false
                                }
                                updatedProgress = progress / 480.0
                            }
                            else {
                                updatedProgress = progress / strongSelf.videoSegmentSeconds
                            }
                            
                            strongSelf.recordButton?.updateProgress(updatedProgress)
                            progress = progress + 0.2
//                            switch strongSelf.videoSpeedType {
//                            case .normal:
//                                strongSelf.recordButton?.updateProgress(updatedProgress)
//                                progress = progress + 0.2
//                            case .fast(let scaleFactor):
//                                strongSelf.recordButton?.updateProgress(updatedProgress/CGFloat(scaleFactor))
//                                progress = progress + 0.2
//                            case .slow(let scaleFactor):
//                                strongSelf.recordButton?.updateProgress(updatedProgress*CGFloat(scaleFactor))
//                                progress = progress + 0.2
//                            }
                            
                        } else {
                            strongSelf.recordButton?.updateProgress(0.0)
                            progress = 0.2
                        }
                    }
                }
            }
        })

        self.startVideoRecoredDesignChanges()

    }

    func stopRecording() {
        DispatchQueue.main.async {
            self.timer?.invalidate()
            self.timer = nil
            if self.currentCamaraMode == .margeAllVideo {
                UIView.animate(withDuration: 0.1, delay: 0, options: .curveEaseOut, animations: {
                    self.recordButton?.transform = .identity
                    self.recordBlurView.transform = .identity
                }) { (completed: Bool) in
                }
                self.takenVideoUrlsSnapshoot = true
                self.writer?.finishWriting(completionHandler: { [weak self] () -> Void in
                    guard let strongSelf = self else { return }
                    print("Recording completed \(String(describing: strongSelf.writer?.outputURL?.path))")
                    //                    strongSelf.camera?.setTorchOn(.off)
                    strongSelf.takenVideoUrl = SegmentVideos(urlStr: (strongSelf.writer?.outputURL)!, thumbimage: strongSelf.takenImages.last, latitued: nil, longitued: nil, placeAddress: nil, numberOfSegement: "\(strongSelf.takenVideoUrls.count + 1)", videoduration: nil, combineOneVideo: true)
                    
                    strongSelf.takenVideoUrls.append(SegmentVideos(urlStr: (strongSelf.writer?.outputURL)!, thumbimage: strongSelf.takenImages.last, latitued: nil, longitued: nil, placeAddress: nil, numberOfSegement: "\(strongSelf.takenVideoUrls.count + 1)", videoduration: nil, combineOneVideo: true))
                    
                    let asst = AVAsset(url: (strongSelf.writer?.outputURL)!)
                    strongSelf.videoArray.append(asst)
                    
                    DispatchQueue.main.async {
                        strongSelf.setupForPreviewScreen()
                    }
                    
                })
                return
            }
            
            self.recordButton?.updateProgress(0.0, animated: false) {

//                self.videoSpeedType = .normal
//                self.speedSliderLabels.value = 2
//                self.speedSlider.value = 2
//                self.isSpeedChanged = false
//                UIView.animate(withDuration: 0.1, delay: 0, options: .curveEaseOut, animations: {
//                    self.redRecordingView?.transform = .identity
//                }) { (completed: Bool) in
//                    self.redRecordingView.backgroundColor = UIColor.clear
//                }

                UIView.animate(withDuration: 0.1, delay: 0, options: .curveEaseOut, animations: {
                    self.recordButton?.transform = .identity
                    self.recordBlurView.transform = .identity
                }) { (completed: Bool) in
                }
                self.takenVideoUrlsSnapshoot = true
                self.writer?.finishWriting(completionHandler: { [weak self] () -> Void in
                    guard let strongSelf = self else { return }
                    print("Recording completed \(String(describing: strongSelf.writer?.outputURL?.path))")
//                    strongSelf.camera?.setTorchOn(.off)
                    strongSelf.takenVideoUrl = SegmentVideos(urlStr: (strongSelf.writer?.outputURL)!, thumbimage: strongSelf.takenImages.last, latitued: nil, longitued: nil, placeAddress: nil, numberOfSegement: "\(strongSelf.takenVideoUrls.count + 1)", videoduration: nil)
                 
                    strongSelf.takenVideoUrls.append(SegmentVideos(urlStr: (strongSelf.writer?.outputURL)!, thumbimage: strongSelf.takenImages.last, latitued: nil, longitued: nil, placeAddress: nil, numberOfSegement: "\(strongSelf.takenVideoUrls.count + 1)", videoduration: nil))
                   
                    
                    DispatchQueue.main.async {
                        strongSelf.setupForPreviewScreen()
                    }

                })
            }
        }
    }
    
    func getDurationOf(videoPath: URL) -> Double {
        return AVAsset.init(url: videoPath).duration.seconds
    }
    
    func setupForPreviewScreen() {
        self.isWriteData = false
        self.stopMotionCollectionView.reloadData()
        let layout = self.stopMotionCollectionView.collectionViewLayout as! UPCarouselFlowLayout
        let pageSide = (layout.scrollDirection == .horizontal) ? self.pageSize.width : self.pageSize.height
        self.stopMotionCollectionView?.contentOffset.x = (self.stopMotionCollectionView?.contentSize.width)! + pageSide
        
//        strongSelf.speedSlider.isUserInteractionEnabled = true
        
//        strongSelf.zoomSliderView.isHidden = true
        
        if self.currentCamaraMode == .margeAllVideo {
            self.showControls()
            totalDurationOfOneSegment = totalDurationOfOneSegment + self.getDurationOf(videoPath: (self.takenVideoUrls.last?.url)!)
            self.isRecordStart = false
            if totalDurationOfOneSegment > 240.0 {
                totalDurationOfOneSegment = 0.0
                self.mergeVideoArray()
            }
            
        }
        else if self.currentCamaraMode == .boom {
            self.showControls()
//            btnDone.isHidden = true
            self.recordButton?.updateProgress(0.0)
            
            self.isRecordStart = false
            self.videoSpeedType = .normal
            self.speedSliderLabels.value = 2
            self.speedSlider.value = 2
            self.isSpeedChanged = false
            if (self.currentCamaraMode == .timer && !handsfreeButton.isSelected) || (self.currentCamaraMode == .photoTimer && !handsfreeButton.isSelected) {
                self.currentCamaraMode = .video
            }
            
            let storyboard = UIStoryboard.init(name: "PhotoEditor", bundle: nil)
            let photoEditor = storyboard.instantiateViewController(withIdentifier: "PhotoEditorViewController") as! PhotoEditorViewController
            
            photoEditor.storyCamDelegate = self.storyCamDelegate
            photoEditor.videoUrls = self.takenVideoUrls
            photoEditor.currentCamaraMode = currentCamaraMode
            photoEditor.outtakesDelegate = self
            let recordSession = SCRecordSession()
            for url in self.takenVideoUrls {
                let segment = SCRecordSessionSegment(url: url.url!, info: nil)
                recordSession.addSegment(segment)
            }
            
            photoEditor.recordSession = recordSession
            
            for i in 0...31 {
                photoEditor.stickers.append(UIImage(named: "storySticker_\(i)")!)
            }
            
            photoEditor.hiddenControls = [.crop, .share]
            
            self.navigationController?.pushViewController(photoEditor, animated: true)
            
            self.takenImages.removeAll()
            self.takenVideoUrls.removeAll()
            self.stopMotionCollectionView.reloadData()
        }
        else {
//            btnDone.isHidden = true
            self.recordButton?.updateProgress(0.0)
            
            let currentVideoSeconds = self.videoSegmentSeconds/2
            if !self.isStopConnVideo && currentVideoSeconds*CGFloat(self.takenVideoUrls.count) < 240 {
                
                if (self.currentCamaraMode == .handFree || self.currentCamaraMode == .timer) && self.pauseTimerValue > 0 && !self.isWriteData {
                    self.isCountDownStarted = true
                    if self.sfCountdownView == nil {
                        self.setupCountDownView()
                    }
                    self.view?.bringSubview(toFront: self.sfCountdownView)
                    self.sfCountdownView.isHidden = false
                    self.sfCountdownView.countdownFrom = self.pauseTimerValue
                    self.sfCountdownView.start()
                    self.view?.bringSubview(toFront: self.baseView)
                    
                } else {
                    self.isRecordStart = true
                    self.startRecording()
                }
                
            } else {
                self.showControls()
                self.isRecordStart = false
                self.videoSpeedType = .normal
                self.speedSliderLabels.value = 2
                self.speedSlider.value = 2
                self.isSpeedChanged = false
                if (self.currentCamaraMode == .timer && !handsfreeButton.isSelected) || (self.currentCamaraMode == .photoTimer && !handsfreeButton.isSelected) {
                    self.currentCamaraMode = .video
                }
                
                let storyboard = UIStoryboard.init(name: "PhotoEditor", bundle: nil)
                let photoEditor = storyboard.instantiateViewController(withIdentifier: "PhotoEditorViewController") as! PhotoEditorViewController
                
                photoEditor.storyCamDelegate = self.storyCamDelegate
                photoEditor.videoUrls = self.takenVideoUrls
                photoEditor.outtakesDelegate = self
                let recordSession = SCRecordSession()
                for url in self.takenVideoUrls {
                    let segment = SCRecordSessionSegment(url: url.url!, info: nil)
                    recordSession.addSegment(segment)
                }
                
                photoEditor.recordSession = recordSession
                
                for i in 0...31 {
                    photoEditor.stickers.append(UIImage(named: "storySticker_\(i)")!)
                }
                
                photoEditor.hiddenControls = [.crop, .share]
                
                self.navigationController?.pushViewController(photoEditor, animated: true)
                
                self.takenImages.removeAll()
                self.takenVideoUrls.removeAll()
                self.stopMotionCollectionView.reloadData()
            }
        }
    }

    func startBoomerangRecording() {
        self.isFirstImage = true
        
        slow2xBar.isHidden = false
        fast2xBar.isHidden = false
        slowVerticalBar.isHidden = false
        fastVerticalBar.isHidden = false

        var progress: CGFloat = 1 / 200
        timer = Timer.schedule(repeatInterval: 0.01, handler: { [weak self] (timer) in
            guard let strongSelf = self else { return }
            if let progress = strongSelf.recordButton?.progress,
                Int(progress) == 1 {
                timer?.invalidate()
                strongSelf.recordButton?.updateProgress(0.0)
                strongSelf.stopBoomerangRecording()
            } else {
                if strongSelf.isWriteData {
                    strongSelf.recordButton?.updateProgress(progress)
                    progress = progress + (1 / 200)
                } else {
                    strongSelf.recordButton?.updateProgress(0.0)
                    progress = 1 / 200
                }
            }
            if strongSelf.blinkStatus == false {
                strongSelf.view.bringSubview(toFront: strongSelf.blinkView)
                strongSelf.blinkView.backgroundColor = UIColor.white.withAlphaComponent(0.6)
                strongSelf.blinkStatus = true
            } else {
                strongSelf.view.sendSubview(toBack: strongSelf.blinkView)
                strongSelf.blinkView.backgroundColor = UIColor.clear
                strongSelf.blinkStatus = false
            }
        })

        self.startVideoRecoredDesignChanges()

    }

    func stopBoomerangRecording() {
        showControls()
        self.horizontalScrollView?.isHidden = false
        self.timer?.invalidate()
        self.timer = nil
      
        let button = recordButton
        let redRecView = redRecordingView
        let blurRecordView = recordBlurView
        button?.center = recoredButtonCenterPoint
        redRecView?.center = recoredViewCenterPoint
        blurRecordView?.center = recoredBlurCenterPoint
        
        self.recordButton?.updateProgress(0.0, animated: false) {
            
            UIView.animate(withDuration: 0.1, delay: 0, options: .curveEaseOut, animations: {
                self.recordButton?.transform = .identity
                self.recordBlurView.transform = .identity
                DispatchQueue.main.async {
                    self.recordButton?.center = self.recoredButtonCenterPoint
                    self.redRecordingView?.center = self.recoredViewCenterPoint
                    self.recordBlurView?.center = self.recoredBlurCenterPoint
                }
             
            }) { (completed: Bool) in
            }
           
            self.writer?.finishWriting(completionHandler: { [weak self] () -> Void in
                guard let strongSelf = self else { return }
                print("Recording completed \(String(describing: strongSelf.writer?.outputURL?.path))")
                strongSelf.camera?.setTorchOn(.off)
                strongSelf.isWriteData = false
                
                strongSelf.takenVideoUrl = SegmentVideos(urlStr: (strongSelf.writer?.outputURL)!, thumbimage: strongSelf.takenImages.last, latitued: nil, longitued: nil, placeAddress: nil, numberOfSegement: "\(strongSelf.takenVideoUrls.count + 1)", videoduration: nil)
                
                strongSelf.takenVideoUrls.append(SegmentVideos(urlStr: (strongSelf.writer?.outputURL)!, thumbimage: strongSelf.takenImages.last, latitued: nil, longitued: nil, placeAddress: nil, numberOfSegement: "\(strongSelf.takenVideoUrls.count + 1)", videoduration: nil))
                
                
                DispatchQueue.main.async {
                    strongSelf.setupForPreviewScreen()
                }
            })
        }
    }

    func startRewindRecording() {
        self.isFirstImage = true
        var progress: CGFloat = 1 / 1000
        timer = Timer.schedule(repeatInterval: 0.01, handler: { [weak self] (timer) in
            guard let strongSelf = self else { return }
            DispatchQueue.main.async {
                if let progress = strongSelf.recordButton?.progress,
                    Int(progress) == 1 {
                    DispatchQueue.main.async {
                        timer?.invalidate()
                        strongSelf.recordButton?.updateProgress(0.0)
                        strongSelf.stopRewindRecording()
                    }
                } else {
                    DispatchQueue.main.async {
                        if strongSelf.isWriteData {
                            strongSelf.recordButton?.updateProgress(progress)
                            progress = progress + 1 / 1000
                        } else {
                            strongSelf.recordButton?.updateProgress(0.0)
                            progress = 1 / 1000
                        }
                    }
                }
            }
        })

        self.startVideoRecoredDesignChanges()

    }

    func stopRewindRecording() {
        DispatchQueue.main.async {
            self.timer?.invalidate()
            self.timer = nil
            self.view.sendSubview(toBack: self.blinkView)
            self.blinkView.backgroundColor = UIColor.clear
            self.blinkStatus = false
            
            self.recordButton?.updateProgress(0.0, animated: false) { [weak self] in
                guard let strongSelf = self else { return }
                strongSelf.videoSpeedType = .normal
                UIView.animate(withDuration: 0.1, delay: 0, options: .curveEaseOut, animations: {
                    strongSelf.redRecordingView?.transform = .identity
                }) { (completed: Bool) in
                    strongSelf.redRecordingView.backgroundColor = UIColor.clear
                }

                UIView.animate(withDuration: 0.1, delay: 0, options: .curveEaseOut, animations: {
                    strongSelf.recordButton?.transform = .identity
                    strongSelf.recordBlurView.transform = .identity
                }) { (completed: Bool) in
                }
                strongSelf.takenVideoUrlsSnapshoot = true

                strongSelf.writer?.finishWriting(completionHandler: { [weak self] () -> Void in
                    guard let strongSelf = self else { return }
                    print("Recording completed \(String(describing: strongSelf.writer?.outputURL?.path))")
                    strongSelf.camera?.setTorchOn(.off)
                    strongSelf.isWriteData = false
                   // strongSelf.takenVideoUrl = strongSelf.writer?.outputURL

                    DispatchQueue.main.async {
                        strongSelf.recordButton?.updateProgress(0.0)
                        let previewVC = StoryPreviewViewController(nibName: "StoryPreviewViewController", bundle: nil)
                        previewVC.storyCamDelegate = strongSelf.storyCamDelegate
                       // previewVC.boomerangEffectVideoUrl = strongSelf.takenVideoUrl
                        previewVC.currentCamaraMode = strongSelf.currentCamaraMode
                        previewVC.image = strongSelf.takenImage
                        strongSelf.navigationController?.pushViewController(previewVC, animated: false)
                    }
                })
            }
        }
    }

    func startHandFreeRecording() {
        self.isFirstImage = true
        var progress: CGFloat = 1 / 1000
        timer = Timer.schedule(repeatInterval: 0.01, handler: { [weak self] (timer) in
            guard let strongSelf = self else { return }
            DispatchQueue.main.async {
                if let progress = strongSelf.recordButton?.progress,
                    Int(progress) == 1 {
                    DispatchQueue.main.async {
                        timer?.invalidate()
                        strongSelf.recordButton?.updateProgress(0.0)
                        strongSelf.stopRecording()
                    }
                } else {
                    DispatchQueue.main.async {
                        if strongSelf.isWriteData {
                            strongSelf.recordButton?.updateProgress(progress)
                            progress = progress + 1 / 1000
                        } else {
                            strongSelf.recordButton?.updateProgress(0.0)
                            progress = 1 / 1000
                        }
                    }
                }
            }
        })

        self.startVideoRecoredDesignChanges()
    }

    func changeCamaraMode() {
        let blurView = UIVisualEffectView(frame: (preview?.bounds)!)
        blurView.effect = UIBlurEffect.init(style: .light)
        preview?.addSubview(blurView)

        let newScaleFactor = lastZoomFactor

        UIView.transition(with: preview!, duration: 0.4, options: .transitionFlipFromBottom, animations: {
            switch self.currentCameraPosition {
            case .back:
                self.currentCameraPosition = .front
            case .front:
                self.currentCameraPosition = .back
            default:
                break
            }
            self.setCameraPositionUI()
            UserDefaults.standard.set(self.currentCameraPosition.rawValue, forKey: "cameraPosition")

        }) { (finished) in
            self.zoomSlider.value = Float(newScaleFactor)
            self.camera?.zoomFactor = newScaleFactor
            blurView.removeFromSuperview()
        }
    }
    
    func setCameraPositionUI() {
        switch self.currentCameraPosition {
        case .front:
            self.camera?.setCameraPosition(self.currentCameraPosition)
            self.flipLabel.text = "Selfie"
            self.flipButton.setImage(#imageLiteral(resourceName: "cameraFlip"), for: UIControlState.normal)
            self.dataProcess?.mirror = true
            self.saveDataProcess?.mirror = false
        case .back:
            self.camera?.setCameraPosition(self.currentCameraPosition)
            self.flipLabel.text = "Rear"
            self.flipButton.setImage(#imageLiteral(resourceName: "cameraFlipBack"), for: UIControlState.normal)
            self.dataProcess?.mirror = false
            self.saveDataProcess?.mirror = false
        default:
            break
        }
        UserDefaults.standard.set(self.currentCameraPosition.rawValue, forKey: "cameraPosition")

    }

}

// MARK: CameraDelegate
extension StoryCameraVC: CameraDelegate {

    func cameraVideoOutput(_ sampleBuffer: CMSampleBuffer?) {
        // Data processing
        var bgraSampleBuffer = dataProcess?.process(sampleBuffer)
        guard let sampleBuf = bgraSampleBuffer?.takeRetainedValue() else {
            return
        }

        var bgraSample = saveDataProcess?.process(sampleBuffer)
        guard let saveBuf = bgraSample?.takeRetainedValue() else {
            return
        }

        // Write data to Mp4 file
        if isWriteData {
//            print("video - \(CMSampleBufferGetPresentationTimeStamp(sampleBuf))")
            writer?.writeVideoPixelBuffer(false, CMSampleBufferGetImageBuffer(saveBuf)!, saveBuf, time: CMSampleBufferGetPresentationTimeStamp(saveBuf), speedType: self.videoSpeedType)
        }

        if isSnapshoot {
            isSnapshoot = false
            let pixelBuffer: CVPixelBuffer = CMSampleBufferGetImageBuffer(saveBuf)!
            let image = pixelBuffer.getImage()

            let when = DispatchTime.now() + 0.5
            AudioServicesPlaySystemSound(1108)
            DispatchQueue.main.asyncAfter(deadline: when, execute: {
                self.camera?.setTorchOn(.off)
            })
            self.takenImage = image
            
            if self.currentCamaraMode == .slideShow {
                
                self.takenSlideShowImages.append(SegmentVideos(urlStr: URL(string: "www.google.com")!, thumbimage: image, latitued: nil, longitued: nil, placeAddress: nil, numberOfSegement: String(self.takenSlideShowImages.count + 1), videoduration: nil, combineOneVideo: false))
                if takenSlideShowImages.count == 1 {
                    self.currentCamaraMode = .slideShow
                }
                
                DispatchQueue.main.async {
                    self.stopMotionCollectionView.isHidden = false
                    self.stopMotionCollectionView.reloadData()
                    if self.takenSlideShowImages.count == 20 {
                        self.onNextClick(UIButton())
                    }
                }
                
            } else if self.currentCamaraMode != .stopMotion {

                DispatchQueue.main.async {
//                    let previewVC = StoryPreviewViewController(nibName: "StoryPreviewViewController", bundle: nil)
//                    previewVC.storyCamDelegate = self.storyCamDelegate
//                    previewVC.image = image
//                    previewVC.outtakesDelegate = self
//                    self.navigationController?.pushViewController(previewVC, animated: false)
                    let storyboard = UIStoryboard.init(name: "PhotoEditor", bundle: nil)
                    let photoEditor = storyboard.instantiateViewController(withIdentifier: "PhotoEditorViewController") as! PhotoEditorViewController
                    photoEditor.image = image
                    photoEditor.outtakesDelegate = self
                    photoEditor.storyCamDelegate = self.storyCamDelegate
                    for i in 0...31 {
                        photoEditor.stickers.append(UIImage(named: "storySticker_\(i)")!)
                    }
                    photoEditor.hiddenControls = [.crop, .share]

                    self.navigationController?.pushViewController(photoEditor, animated: false)

                    self.takenImages.removeAll()
                    self.takenVideoUrls.removeAll()
                    self.stopMotionCollectionView.reloadData()
                }
            }
        }

        if isFirstImage {
            isFirstImage = false
            let pixelBuffer: CVPixelBuffer = CMSampleBufferGetImageBuffer(saveBuf)!
            let image = pixelBuffer.getImage()
            self.takenImage = image
            self.takenImages.append(image!)
        }

        if takenVideoUrlsSnapshoot {
            takenVideoUrlsSnapshoot = false
            
        }

        // Render in Preview
        preview?.render(CMSampleBufferGetImageBuffer(sampleBuf))
        // Release resources
        CMSampleBufferInvalidate(saveBuf)
        CMSampleBufferInvalidate(sampleBuf)
        bgraSampleBuffer = nil
        bgraSample = nil

    }

    func fixOrientation(img: UIImage) -> UIImage {
        if img.imageOrientation == .up {
            return img
        }

        UIGraphicsBeginImageContextWithOptions(img.size, false, img.scale)
        let rect = CGRect(x: 0, y: 0, width: img.size.width, height: img.size.height)
        img.draw(in: rect)

        let normalizedImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()

        return normalizedImage
    }

    func cameraAudioOutput(_ sampleBuffer: CMSampleBuffer?) {
        // Write data to Mp4 file
        if isWriteData {
            //            print("audio - \(CMSampleBufferGetPresentationTimeStamp(sampleBuffer!))")
            if currentCamaraMode != .boom {
                writer?.writeAudioSampleBuffer(sampleBuffer!, speedType: self.videoSpeedType, needToWrite: !isMute)
            }
            else {
                writer?.writeAudioSampleBuffer(sampleBuffer!, speedType: self.videoSpeedType, needToWrite: false)
            }
        }
    }
}

// MARK: CameraDataProcessDelegate
extension StoryCameraVC: CameraDataProcessDelegate {

    func cameraDataProcess(withTexture texture: GLuint, width: GLuint, height: GLuint) -> GLuint {
        handler?.process(withTexture: texture, width: GLint(width), height: GLint(height))
        return texture
    }
}

extension StoryCameraVC: CountdownViewDelegate
{
    func countdownFinished(_ view: CountdownView?) {
        isCountDownStarted = false
        sfCountdownView.stop()
        sfCountdownView.isHidden = true
        if photoTimerValue > 0 {
            currentCamaraMode = .photoTimer
            camera?.setTorchOn(flashMode)
            let when = DispatchTime.now() + 0.5
            DispatchQueue.main.asyncAfter(deadline: when, execute: {
                self.isSnapshoot = true
            })
        } else {
            isRecordStart = true
            currentCamaraMode = .timer
            self.startRecording()
        }
//        timerButton.tintColor = UIColor.white
//        timerValue = 0
//        selectedTimerLabel.text = ""
//        timerButton.setImage(#imageLiteral(resourceName: "timer"), for: UIControlState.normal)
    }
}

// MARK: Record Button Gestures
extension StoryCameraVC: UIGestureRecognizerDelegate {
    @objc internal func handleLongPressTimerGestureRecognizer(_ gestureRecognizer: UIGestureRecognizer) {
        return
        switch gestureRecognizer.state {
        case .began:
            guard !isWriteData else {
                return
            }

            setupTimeSegmentCustomPopover(for: timerLabel)
            break
        case .changed:

            break
        case .ended:

            fallthrough
        case .cancelled:
            fallthrough
        case .failed:
            fallthrough
        default:
            break
        }
    }

    @objc internal func handleLongPressGestureRecognizer(_ gestureRecognizer: UIGestureRecognizer) {
        if self.currentCamaraMode == .slideShow {
            return
        }

        if currentCamaraMode != .boom && currentCamaraMode != .margeAllVideo {
            
            if isCountDownStarted {
                isCountDownStarted = false
                sfCountdownView.stop()
                sfCountdownView.isHidden = true
                currentCamaraMode = .video
                timerValue = 0
                resetCountDown()
                selectedTimerLabel.text = ""
                timerButton.setImage(#imageLiteral(resourceName: "timer"), for: UIControlState.normal)
            }
            if timerValue > 0 {
                return
            }
            if currentCamaraMode == .handFree || currentCamaraMode == .timer {
                return
            }
            if !sfCountdownView.isHidden {
                return
            }
            
            if timerValue > 0 && !isWriteData {
                isCountDownStarted = true
                view?.bringSubview(toFront: sfCountdownView)
                sfCountdownView.isHidden = false
                sfCountdownView.countdownFrom = timerValue
                sfCountdownView.start()
                view?.bringSubview(toFront: baseView)
                return
            }

        }

        //recordButton?.frame = gestureRecognizer.location(in: self.view)
        let button = recordButton
        let redRecView = redRecordingView
        let blurRecordView = recordBlurView

        switch gestureRecognizer.state {
        case .began:

            recordButton?.layoutIfNeeded()

            redRecordingView?.layoutIfNeeded()

            recoredButtonCenterPoint = (recordButton?.center)!

            recoredViewCenterPoint = (redRecordingView?.center)!

            recoredBlurCenterPoint = recordBlurView.center

            horizontalScrollView?.isHidden = true
            if currentCamaraMode == .boom {
                slow2xBar.isHidden = false
                fast2xBar.isHidden = false
                slowVerticalBar.isHidden = false
                fastVerticalBar.isHidden = false
                isRecordStart = true
                startBoomerangRecording()
            }
            else if currentCamaraMode == .rewind {
                isRecordStart = true
                startRewindRecording()
            }
            else
            {
                slow2xBar.isHidden = false
                fast2xBar.isHidden = false
                slowVerticalBar.isHidden = false
                fastVerticalBar.isHidden = false
                isRecordStart = true
                startRecording()
            }

            self._panStartPoint = gestureRecognizer.location(in: self.view)
            self._panStartZoom = camera?.zoomFactor ?? 1.0
            break
        case .changed:
            let normalPart = (UIScreen.main.bounds.width * CGFloat(142.5)) / 375
            let screenPart = Int((UIScreen.main.bounds.width - normalPart) / 4)

            let translation: CGPoint = gestureRecognizer.location(in: button)
            let translationRedRecView: CGPoint = gestureRecognizer.location(in: redRecView)
            let translationBlurView: CGPoint = gestureRecognizer.location(in: blurRecordView)

            button?.center = CGPoint(x: (button?.center.x ?? 0.0) + translation.x - 31, y: (button?.center.y ?? 0.0) + translation.y - 31)
            blurRecordView?.center = CGPoint(x: (blurRecordView?.center.x ?? 0.0) + translationBlurView.x - 31, y: (blurRecordView?.center.y ?? 0.0) + translationBlurView.y - 31)
            redRecView?.center = CGPoint(x: (button?.center.x ?? 0.0) + translationRedRecView.x, y: (button?.center.y ?? 0.0) + translationRedRecView.y)
            gestureRecognizer.location(ofTouch: 0, in: button)
            gestureRecognizer.location(ofTouch: 0, in: redRecView)

//            let screenPartFast = Int((Int((UIScreen.main.bounds.width - 100) / 2))/3)
            let newPoint = gestureRecognizer.location(in: self.view)
            print("\(Int(newPoint.x))")
            print("difference = \(Int(self._panStartPoint.x) - Int(newPoint.x))")

            let zoomEndPoint: CGFloat = self.view.center.y - 50.0


            let maxZoom = min(10.0, self.camera?.getMaxZoomFactor() ?? 10)


            var newZoom = CGFloat(maxZoom) - ((zoomEndPoint - newPoint.y) / (zoomEndPoint - self._panStartPoint.y) * CGFloat(maxZoom))
            newZoom = newZoom + lastZoomFactor
            if newZoom <= 1.0 {
                self.camera?.zoomFactor = 1.0
//                self.zoomSliderView.isHidden = true
                self.zoomSlider.value = 1.0
            } else {
                self.camera?.zoomFactor = newZoom
//                self.zoomSliderView.isHidden = false
                self.zoomSlider.value = Float(newZoom)
            }

            if currentCamaraMode != .boom || currentCamaraMode != .stopMotion {
                if abs((Int(self._panStartPoint.x) - Int(newPoint.x))) > 50 {
                    let difference = abs((Int(self._panStartPoint.x) - Int(newPoint.x))) - 50
                    if (Int(self._panStartPoint.x) - Int(newPoint.x)) > 0 {
                        print("swipe left")
//                        if difference > screenPart * 2 {
//                            if videoSpeedType != VideoSpeedType.slow(scaleFactor: 4.0) {
//                                DispatchQueue.main.async {
//                                    self.videoSpeedType = .slow(scaleFactor: 4.0)
//                                    self.speedSliderLabels.value = 0
//                                    self.speedSlider.value = 0
//                                    self.isSpeedChanged = true
//                                }
//                            }
//                        } else
                        if difference > screenPart {
                            print("Slow 3x")
                            if videoSpeedType != VideoSpeedType.slow(scaleFactor: 3.0) {
                                DispatchQueue.main.async {
                                    self.videoSpeedType = .slow(scaleFactor: 3.0)
                                    self.speedSliderLabels.value = 0
                                    self.speedSlider.value = 0
                                    self.isSpeedChanged = true
                                    self.speedLabel.text = "Slow 3x"
                                    self.speedLabel.startBlink()
                                }
                            }
                        } else {
                            print("Slow 2x")
                            if videoSpeedType != VideoSpeedType.slow(scaleFactor: 2.0) {
                                DispatchQueue.main.async {
                                    self.videoSpeedType = .slow(scaleFactor: 2.0)
                                    self.speedSliderLabels.value = 1
                                    self.speedSlider.value = 1
                                    self.isSpeedChanged = true
                                    self.speedLabel.text = "Slow 2x"
                                    self.speedLabel.startBlink()
                                }
                            }
                        }

                    } else {
                        print("swipe right")
//                        if difference > screenPartFast * 2 {
//                            if videoSpeedType != VideoSpeedType.fast(scaleFactor: 4.0) {
//                                DispatchQueue.main.async {
//                                    self.videoSpeedType = .fast(scaleFactor: 4.0)
//                                    self.speedSliderLabels.value = 5
//                                    self.speedSlider.value = 5
//                                    self.isSpeedChanged = true
//                                }
//                            }
//                        } else
                        if difference > screenPart {
                            print("Fast 3x")
                            if videoSpeedType != VideoSpeedType.fast(scaleFactor: 3.0) {
                                DispatchQueue.main.async {
                                    self.videoSpeedType = .fast(scaleFactor: 3.0)
                                    self.speedSliderLabels.value = 4
                                    self.speedSlider.value = 4
                                    self.isSpeedChanged = true
                                    self.speedLabel.text = "Fast 3x"
                                    self.speedLabel.startBlink()
                                }
                            }
                        } else {
                            print("Fast 2x")
                            if videoSpeedType != VideoSpeedType.fast(scaleFactor: 2.0) {
                                DispatchQueue.main.async {
                                    self.videoSpeedType = .fast(scaleFactor: 2.0)
                                    self.speedSliderLabels.value = 3
                                    self.speedSlider.value = 3
                                    self.isSpeedChanged = true
                                    self.speedLabel.text = "Fast 2x"
                                    self.speedLabel.startBlink()
                                }
                            }
                        }
                    }
                }
                else {
                    if !isSpeedChanged {
                        if speedSlider.speedType == .normal {
                            if videoSpeedType != VideoSpeedType.normal {

                                DispatchQueue.main.async {
                                    self.videoSpeedType = .normal
                                    self.speedSliderLabels.value = 2
                                    self.speedSlider.value = 2
                                    self.speedLabel.text = ""
                                    self.speedLabel.stopBlink()
                                }
                            }
                        }
                    } else {
                        if videoSpeedType != VideoSpeedType.normal {

                            DispatchQueue.main.async {
                                self.videoSpeedType = .normal
                                self.speedSliderLabels.value = 2
                                self.speedSlider.value = 2
                                self.speedLabel.text = ""
                                self.speedLabel.stopBlink()
                            }
                        }
                    }
                }
            }
            break
        case .ended:
            let button = recordButton
            let redRecView = redRecordingView
            let blurRecordView = recordBlurView
            button?.center = recoredButtonCenterPoint
            redRecView?.center = recoredViewCenterPoint
            blurRecordView?.center = recoredBlurCenterPoint
            gestureRecognizer.location(ofTouch: 0, in: button)
            gestureRecognizer.location(ofTouch: 0, in: redRecView)
            gestureRecognizer.location(ofTouch: 0, in: blurRecordView)
            DispatchQueue.main.async {
                self.speedLabel.text = ""
                self.speedLabel.stopBlink()
                self.horizontalScrollView?.isHidden = false
                if self.currentCamaraMode == .boom {
                    self.isStopConnVideo = true
                    self.stopBoomerangRecording()
                }
                else if self.currentCamaraMode == .rewind {
                    self.stopRewindRecording()
                }
                else if self.currentCamaraMode != .handFree || self.currentCamaraMode == .timer {
                    self.isStopConnVideo = true
                    self.stopRecording()
                }

                if self.currentCamaraMode != .handFree {
//                    self.camera?.zoomFactor = 1.0
//                    self.zoomSlider.value = 1.0
                }
            }
            fallthrough
        case .cancelled:
            fallthrough
        case .failed:
            fallthrough
        default:
            break
        }
    }

    @objc internal func handlePhotoTapGestureRecognizer(_ gestureRecognizer: UIGestureRecognizer) {
       
        if self.currentCamaraMode == .slideShow {
            camera?.setTorchOn(flashMode)
            let when = DispatchTime.now() + 0.5
            DispatchQueue.main.asyncAfter(deadline: when, execute: {
                self.isSnapshoot = true
            })

            return
        }
        
        if self.currentCamaraMode == .margeAllVideo {
            if takenVideoUrls.count != 0 {
                let alert = UIAlertController(title: "Delete", message: "you can last video delete?", preferredStyle: UIAlertControllerStyle.alert)
                let notNow = UIAlertAction(title: "Not Now", style: .default, handler: { (a) in
                    
                })
                let manage = UIAlertAction(title: "Yes", style: .default, handler: { (a) in
                    if self.takenVideoUrls.count != 0 {
                        let secondes = self.getDurationOf(videoPath: self.takenVideoUrls.last!.url!)
                        let progress: CGFloat = (self.recordButton?.progress)!
                        let removeProgress: CGFloat = CGFloat(secondes / 240.0)
                        
                        var setNewProgress = progress - removeProgress
                        if setNewProgress < 0.0 {
                            setNewProgress = 0.0
                        }
                        self.recordButton?.updateProgress(setNewProgress)
                        
                        self.takenVideoUrls.removeLast()
                        self.stopMotionCollectionView.reloadData()
                    }
                })
                alert.addAction(notNow)
                alert.addAction(manage)
                
                self.present(alert, animated: true, completion: nil)
            }
            return
        }
        
        if isCountDownStarted {
            isCountDownStarted = false
            sfCountdownView.stop()
            sfCountdownView.isHidden = true
            if pauseTimerValue > 0 && isRecordStart && (currentCamaraMode == .handFree || currentCamaraMode == .timer) {
                DispatchQueue.main.async {
                    self.isStopConnVideo = true
                    self.setupForPreviewScreen()
                }
                return
            } else if timerValue > 0 {
                currentCamaraMode = .video
                timerValue = 0
                resetCountDown()
                selectedTimerLabel.text = ""
                timerButton.setImage(#imageLiteral(resourceName: "timer"), for: UIControlState.normal)
            } else if photoTimerValue > 0 {
                currentCamaraMode = .video
                photoTimerValue = 0
                resetPhotoCountDown()
                selectedTimerLabel.text = ""
                timerButton.setImage(#imageLiteral(resourceName: "timer"), for: UIControlState.normal)
            }
        }
        
        if timerValue > 0 && !isWriteData {
            isCountDownStarted = true
            view?.bringSubview(toFront: sfCountdownView)
            sfCountdownView.isHidden = false
            sfCountdownView.countdownFrom = timerValue
            sfCountdownView.start()
            view?.bringSubview(toFront: baseView)
            return
        }
        
        if photoTimerValue > 0 && !isWriteData {
            isCountDownStarted = true
            view?.bringSubview(toFront: sfCountdownView)
            sfCountdownView.isHidden = false
            sfCountdownView.countdownFrom = photoTimerValue
            sfCountdownView.start()
            view?.bringSubview(toFront: baseView)
            return
        }
        
        if currentCamaraMode != .boom && currentCamaraMode != .rewind && currentCamaraMode != .handFree && currentCamaraMode != .timer {
            camera?.setTorchOn(flashMode)
            let when = DispatchTime.now() + 0.5
            DispatchQueue.main.asyncAfter(deadline: when, execute: {
                self.isSnapshoot = true
            })
        }
        else if currentCamaraMode == .handFree || currentCamaraMode == .timer {
            if !isRecordStart {
                isRecordStart = true
                startRecording()
            }
            else
            {
                self.isStopConnVideo = true
                stopRecording()
            }
        }
        else if currentCamaraMode == .boom {
            if !isRecordStart {
                isRecordStart = true
                startBoomerangRecording()
            }
            else
            {
                isRecordStart = false
                self.isStopConnVideo = true
                stopBoomerangRecording()
            }
        }
    }

    @objc internal func handleSwapUPGestureRecognizer(_ gestureRecognizer: UIGestureRecognizer) {
        self.changeCamaraMode()
    }

}

extension StoryCameraVC: PickerViewDataSource {
    
    // MARK: - PickerViewDataSource
    
    public func pickerViewNumberOfRows(_ pickerView: PickerView) -> Int {
        
        switch timerType {
        case .timer:
            return timerOptions.count
        case .pauseTimer:
            return pauseTimerOptions.count
        case .segmentLength:
            return segmentLengthOptions.count
        case .photoTimer:
            return photoTimerOptions.count
        }
        
    }
    
    public func pickerView(_ pickerView: PickerView, titleForRow row: Int, index: Int) -> String {
        
        switch timerType {
        case .timer:
            return timerOptions[index]
        case .pauseTimer:
            return pauseTimerOptions[index]
        case .segmentLength:
            return segmentLengthOptions[index]
        case .photoTimer:
            return photoTimerOptions[index]
        }
        
    }
    
}

extension StoryCameraVC: PickerViewDelegate {
    
    // MARK: - PickerViewDelegate
    
    public func pickerViewHeightForRows(_ pickerView: PickerView) -> CGFloat {
        return 134.0
    }
    
    public func pickerView(_ pickerView: PickerView, didSelectRow row: Int, index: Int) {
        print(index)
    }
    
    public func pickerView(_ pickerView: PickerView, styleForLabel label: UILabel, highlighted: Bool) {
        if timerType == .segmentLength {
            if pickerView.currentSelectedRow == 0 || pickerView.currentSelectedRow == 1 {
                pickerView.selectionTitle.text = ""
                label.font = UIFont(name: "Avenir-Heavy", size: 35 * UIScreen.main.bounds.width/414)
            } else {
                label.font = UIFont(name: "Avenir-Heavy", size: 35 * UIScreen.main.bounds.width/414)
                pickerView.selectionTitle.text = "seconds"
            }
        } else {
            label.font = UIFont(name: "Avenir-Heavy", size: 35 * UIScreen.main.bounds.width/414)
            if pickerView.currentSelectedRow == 0 {
                pickerView.selectionTitle.text = ""
            } else {
                pickerView.selectionTitle.text = "seconds"
            }
        }
        
        if highlighted {
            label.textColor = UIColor.white
        } else {
            label.textColor = UIColor.white.withAlphaComponent(0.75)
        }
        
    }
    
    public func pickerView(_ pickerView: PickerView, viewForRow row: Int, index: Int, highlighted: Bool, reusingView view: UIView?) -> UIView? {
        return nil
    }
    
}

extension StoryCameraVC: StoryUploadDelegate {
    
    func didCompletedStory() {
        
    }
    
    func didUpdateBytes(_ progress: Double) {
        firstUploadCompletedSize = progress
    }
    
    func didChangeThumbImage(_ image: UIImage) {
        DispatchQueue.main.async {
            self.storyUploadImageView?.image = image
        }
    }
    
    func didUpdateProgress(_ progress: Double) {
        DispatchQueue.main.async {
            var percentage = progress
            if percentage > 100.0 {
                percentage = 100.0
            }
            self.firstPersiontage = percentage
            self.lblStoryPercentage.text = "\(Int(percentage))%"
        }
    }
    
    func didChangeStoryCount(_ storyCount: String) {
        DispatchQueue.main.async {
            if storyCount == "" {
                self.storyUploadView.isHidden = true
            } else {
                self.lblStoryCount.text = storyCount
                self.storyUploadView.isHidden = false
            }
        }
    }
    
}

extension StoryCameraVC {
    // EDIT 1: I FORGOT THIS AT FIRST
    
    func tempURL() -> URL? {
        let directory = NSTemporaryDirectory() as NSString
        
        if directory != "" {
            let path = directory.appendingPathComponent("\(partitionCountNumber)" + ".mov")
            return URL(fileURLWithPath: path)
        }
        return nil
    }
    
    func currentVideoOrientation() -> AVCaptureVideoOrientation {
        var orientation: AVCaptureVideoOrientation
        
        switch UIDevice.current.orientation {
        case .portrait:
            orientation = AVCaptureVideoOrientation.portrait
        case .landscapeRight:
            orientation = AVCaptureVideoOrientation.landscapeLeft
        case .portraitUpsideDown:
            orientation = AVCaptureVideoOrientation.portraitUpsideDown
        default:
            orientation = AVCaptureVideoOrientation.landscapeRight
        }
        
        return orientation
    }
    
    func mergeVideoArray() { 
    
        let mergeSession = SCRecordSession.init()
        for segementModel in self.takenVideoUrls {
            let segment = SCRecordSessionSegment(url: segementModel.url!, info: nil)
            mergeSession.addSegment(segment)
        }
        
        let storyboard = UIStoryboard.init(name: "PhotoEditor", bundle: nil)
        let photoEditor = storyboard.instantiateViewController(withIdentifier: "PhotoEditorViewController") as! PhotoEditorViewController
        
        photoEditor.storyCamDelegate = self.storyCamDelegate
        let image = self.takenVideoUrls[0].image
//        self.takenVideoUrls[0].currentAsset = SegmentVideos.getRecordSession(videoModel: self.takenVideoUrls)
//        self.takenVideoUrls[0].videos = self.takenVideoUrls
//        self.takenVideoUrls[0].isCombineOneVideo = true
//        photoEditor.videoUrls = [self.takenVideoUrls[0]]
        photoEditor.videoUrls = self.takenVideoUrls
        photoEditor.outtakesDelegate = self
        photoEditor.isCombineSegmentShow = true
        photoEditor.recordSession = mergeSession
        photoEditor.currentCamaraMode = currentCamaraMode
        for i in 0...31 {
            photoEditor.stickers.append(UIImage(named: "storySticker_\(i)")!)
        }
        
        photoEditor.hiddenControls = [.crop, .share]
        
        self.navigationController?.pushViewController(photoEditor, animated: true)
        
        self.takenImages.removeAll()
        self.takenVideoUrls.removeAll()
        self.stopMotionCollectionView.reloadData()
    }
    
}

extension CVPixelBuffer {

    func getImage() -> UIImage? {
        CVPixelBufferLockBaseAddress(self, CVPixelBufferLockFlags(rawValue: 0))
        // Get details of the image from CVImageBufferRef
        var base: UnsafeMutableRawPointer?
        var width: size_t
        var height: size_t
        var bytesPerRow: size_t
        base = CVPixelBufferGetBaseAddress(self)
        width = CVPixelBufferGetWidth(self)
        height = CVPixelBufferGetHeight(self)
        bytesPerRow = CVPixelBufferGetBytesPerRow(self)
        // CGIntextRef is formatted using the image detail information
        var colorSpace: CGColorSpace
        var cgContext: CGContext
        colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedFirst.rawValue)
            .union(.byteOrder32Little)
        cgContext = CGContext(data: base, width: width, height: height, bitsPerComponent: 8, bytesPerRow: bytesPerRow, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)!
        // Convert CGContextRef to CGImageRef
        var cgImage: CGImage
        cgImage = cgContext.makeImage()!
        // Convert CGImageRef to UIImage
        var image: UIImage?

        image = UIImage(cgImage: cgImage, scale: 1.0, orientation: .right)
        // Rotated 90
        CVPixelBufferUnlockBaseAddress(self, CVPixelBufferLockFlags(rawValue: 0))

        return image
    }
}

extension UIView {

    func startBlink() {
        UIView.animate(withDuration: 0.8,
                       delay: 0.0,
                       options: [.allowUserInteraction, .curveEaseInOut, .autoreverse, .repeat],
                       animations: { self.alpha = 0 },
                       completion: nil)
    }

    func stopBlink() {
        layer.removeAllAnimations()
        alpha = 1
    }
}

class VideoMerger: NSObject {
    func mergeVideos(withFileURLs videoFileURLs: [URL], completion: @escaping (_ mergedVideoURL: URL?, _ error: Error?) -> Void) {
        
        let composition = AVMutableComposition()
        guard let videoTrack: AVMutableCompositionTrack = composition.addMutableTrack(withMediaType: AVMediaTypeVideo, preferredTrackID: kCMPersistentTrackID_Invalid) else {
            completion(nil, videoTarckError())
            return
        }
        guard let audioTrack: AVMutableCompositionTrack = composition.addMutableTrack(withMediaType: AVMediaTypeAudio, preferredTrackID: kCMPersistentTrackID_Invalid) else {
            completion(nil, audioTarckError())
            return
        }
        var instructions = [AVVideoCompositionInstructionProtocol]()
        var isError = false
        var currentTime: CMTime = kCMTimeZero
        var videoSize = CGSize.zero
        var highestFrameRate = 0
        for videoFileURL in videoFileURLs {
            let options = [AVURLAssetPreferPreciseDurationAndTimingKey: true]
            let asset = AVURLAsset(url: videoFileURL, options: options)
            let videoAsset: AVAssetTrack? = asset.tracks(withMediaType: AVMediaTypeVideo).first
            if videoSize.equalTo(CGSize.zero) {
                videoSize = (videoAsset?.naturalSize)!
            }
            if videoSize.height < (videoAsset?.naturalSize.height)! {
                videoSize.height = (videoAsset?.naturalSize.height)!
            }
            if videoSize.width < (videoAsset?.naturalSize.width)! {
                videoSize.width = (videoAsset?.naturalSize.width)!
            }
        }
        
        for  videoFileURL in videoFileURLs {
            let options = [AVURLAssetPreferPreciseDurationAndTimingKey: true]
            let asset = AVURLAsset(url: videoFileURL, options: options)
            guard let videoAsset: AVAssetTrack = asset.tracks(withMediaType: AVMediaTypeVideo).first else {
                completion(nil, videoTarckError())
                return
            }
            guard let audioAsset: AVAssetTrack = asset.tracks(withMediaType: AVMediaTypeAudio).first else {
                completion(nil, audioTarckError())
                return
            }
            let currentFrameRate = Int(roundf((videoAsset.nominalFrameRate)))
            highestFrameRate = (currentFrameRate > highestFrameRate) ? currentFrameRate : highestFrameRate
            let trimmingTime: CMTime = CMTimeMake(Int64(lround(Double((videoAsset.nominalFrameRate) / (videoAsset.nominalFrameRate)))), Int32((videoAsset.nominalFrameRate)))
            let timeRange: CMTimeRange = CMTimeRangeMake(trimmingTime, CMTimeSubtract((videoAsset.timeRange.duration), trimmingTime))
            do {
                try videoTrack.insertTimeRange(timeRange, of: videoAsset, at: currentTime)
                try audioTrack.insertTimeRange(timeRange, of: audioAsset, at: currentTime)
                
                let videoCompositionInstruction = AVMutableVideoCompositionInstruction.init()
                videoCompositionInstruction.timeRange = CMTimeRangeMake(currentTime, timeRange.duration)
                let layerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: videoTrack)
                
                var tx: Int = 0
                if videoSize.width - videoAsset.naturalSize.width != 0 {
                    tx = Int((videoSize.width - videoAsset.naturalSize.width) / 2)
                }
                var ty: Int = 0
                if videoSize.height - videoAsset.naturalSize.height != 0 {
                    ty = Int((videoSize.height - videoAsset.naturalSize.height) / 2)
                }
                var Scale = CGAffineTransform(scaleX: 1, y: 1)
                if tx != 0 && ty != 0 {
                    if tx <= ty {
                        let factor = Float(videoSize.width / videoAsset.naturalSize.width)
                        Scale = CGAffineTransform(scaleX: CGFloat(factor), y: CGFloat(factor))
                        tx = 0
                        ty = Int((videoSize.height - videoAsset.naturalSize.height * CGFloat(factor)) / 2)
                    }
                    if tx > ty {
                        let factor = Float(videoSize.height / videoAsset.naturalSize.height)
                        Scale = CGAffineTransform(scaleX: CGFloat(factor), y: CGFloat(factor))
                        ty = 0
                        tx = Int((videoSize.width - videoAsset.naturalSize.width * CGFloat(factor)) / 2)
                    }
                }
                let Move = CGAffineTransform(translationX: CGFloat(tx), y: CGFloat(ty))
                layerInstruction.setTransform(videoTrack.preferredTransform.concatenating(Scale.concatenating(Move)), at: kCMTimeZero)
           

                videoCompositionInstruction.layerInstructions = [layerInstruction]
                instructions.append(videoCompositionInstruction)
                currentTime = CMTimeAdd(currentTime, timeRange.duration)
            } catch {
                print("Unable to load data: \(error)")
                isError = true
                completion(nil, error)
            }
        }
        if isError == false {
            let exportSession = AVAssetExportSession(asset: composition, presetName: AVAssetExportPresetHighestQuality)
            let strFilePath: String = generateMergedVideoFilePath()
            try? FileManager.default.removeItem(atPath: strFilePath)
            exportSession?.outputURL = URL(fileURLWithPath: strFilePath)
            exportSession?.outputFileType = AVFileTypeMPEG4
            exportSession?.shouldOptimizeForNetworkUse = true
            let mutableVideoComposition = AVMutableVideoComposition.init()
            mutableVideoComposition.instructions = instructions
            mutableVideoComposition.frameDuration = CMTimeMake(1, Int32(highestFrameRate))
            mutableVideoComposition.renderSize = videoSize
            exportSession?.videoComposition = mutableVideoComposition
            print("Composition Duration: %ld s", lround(CMTimeGetSeconds(composition.duration)))
            print("Composition Framerate: %d fps", highestFrameRate)
            let exportCompletion: (() -> Void) = {() -> Void in
                DispatchQueue.main.async(execute: {() -> Void in
                    completion(exportSession?.outputURL, exportSession?.error)
                })
            }
            if let exportSession = exportSession {
                exportSession.exportAsynchronously(completionHandler: {() -> Void in
                    switch exportSession.status {
                    case .completed:
                        print("Successfully merged: %@", strFilePath)
                        exportCompletion()
                    case .failed:
                        print("Failed")
                        exportCompletion()
                    case .cancelled:
                        print("Cancelled")
                        exportCompletion()
                    case .unknown:
                        print("Unknown")
                    case .exporting:
                        print("Exporting")
                    case .waiting:
                        print("Wating")
                    }
                    
                })
            }
        }
    }
    func videoTarckError() -> Error {
        let userInfo: [AnyHashable : Any] =
            [ NSLocalizedDescriptionKey :  NSLocalizedString("error", value: "Provide correct video file", comment: "") ,
              NSLocalizedFailureReasonErrorKey : NSLocalizedString("error", value: "No video track available", comment: "")]
        return NSError(domain: "DPVideoMerger", code: 404, userInfo: (userInfo as! [String : Any]))
    }
    func audioTarckError() -> Error {
        let userInfo: [AnyHashable : Any] =
            [ NSLocalizedDescriptionKey :  NSLocalizedString("error", value: "Video file had no Audio track", comment: "") ,
              NSLocalizedFailureReasonErrorKey : NSLocalizedString("error", value: "No Audio track available", comment: "")]
        return NSError(domain: "DPVideoMerger", code: 404, userInfo: (userInfo as! [String : Any]))
    }
    func generateMergedVideoFilePath() -> String {
        return URL(fileURLWithPath: ((FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last)?.path)!).appendingPathComponent("\(UUID().uuidString)-mergedVideo.mp4").path
    }
}
