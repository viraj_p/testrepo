//
//  ViewController.swift
//  Photo Editor
//
//  Created by Mohamed Hamed on 4/23/17.
//  Copyright © 2017 Mohamed Hamed. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import SCRecorder
import ColorSlider
import IQKeyboardManagerSwift
import CoreLocation
import Foundation
import SVProgressHUD

public final class PhotoEditorViewController: UIViewController {

    @IBOutlet weak var selectCoverView: UIView! {
        didSet {
            selectCoverView.isHidden = true
        }
    }

    @IBOutlet weak var segmentTypeMergeView: UIStackView!
    @IBOutlet weak var segmentBeforeMergeView: UIView!
    @IBOutlet weak var segmentAfterMergeView: UIView!
    
    
    @IBOutlet weak var segmentResetView: UIView!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var resetLabel: UILabel!
    
    @IBOutlet weak var segmentUndoView: UIView!
    @IBOutlet weak var undoSButton: UIButton!
    @IBOutlet weak var undoLabel: UILabel!
    
    @IBOutlet weak var postStoryButton: UIButton!
    @IBOutlet weak var segmentCombineView: UIView!
    @IBOutlet weak var combineButton: UIButton!
    @IBOutlet weak var combineLabel: UILabel!
    
    @IBOutlet weak var segmentMergeView: UIView!
    @IBOutlet weak var mergeButton: UIButton!
    @IBOutlet weak var mergeLabel: UILabel!
    
    @IBOutlet weak var segmentEditOptionView: UIStackView!
    @IBOutlet weak var feedExportLabel: UILabel!
    @IBOutlet weak var chatExportLabel: UILabel!
    @IBOutlet weak var storyExportLabel: UILabel!
    @IBOutlet weak var notesExportLabel: UILabel!
    @IBOutlet weak var outtakesExportLabel: UILabel!
    
    @IBOutlet weak var postToFeedProgress: RPCircularProgress! {
        didSet {
            postToFeedProgress.trackTintColor = UIColor.clear
            postToFeedProgress.progressTintColor = UIColor.green8215284
            postToFeedProgress.thicknessRatio = 0.15
        }
    }

    @IBOutlet weak var postToChatProgress: RPCircularProgress! {
        didSet {
            postToChatProgress.trackTintColor = UIColor.clear
            postToChatProgress.progressTintColor = UIColor.green8215284
            postToChatProgress.thicknessRatio = 0.15
        }
    }

    @IBOutlet weak var outtakesProgress: RPCircularProgress! {
        didSet {
            outtakesProgress.trackTintColor = UIColor.clear
            outtakesProgress.progressTintColor = UIColor.green8215284
            outtakesProgress.thicknessRatio = 0.15
        }
    }

    @IBOutlet weak var notesProgress: RPCircularProgress! {
        didSet {
            notesProgress.trackTintColor = UIColor.clear
            notesProgress.progressTintColor = UIColor.green8215284
            notesProgress.thicknessRatio = 0.15
        }
    }

    @IBOutlet weak var postStoryProgress: RPCircularProgress! {
        didSet {
            postStoryProgress.trackTintColor = UIColor.clear
            postStoryProgress.progressTintColor = UIColor.green8215284
            postStoryProgress.thicknessRatio = 0.15
        }
    }

    @IBOutlet weak var selectStoryTimeButton: UIView! {
        didSet {
            selectStoryTimeButton.isHidden = false
        }
    }

    @IBOutlet weak var selectCoverButton: UIButton! {
        didSet {
            selectCoverButton.isHidden = true
        }
    }
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var videoCoverView: UIView! {
        didSet {
            videoCoverView.isHidden = true
        }
    }
    @IBOutlet weak var rearrangedView: UIView! {
        didSet {
             rearrangedView.isHidden = true
        }
    }
    @IBOutlet weak var thumbHeight: NSLayoutConstraint!

    @IBOutlet weak var thumbSelectorView: ThumbSelectorView!
    @IBOutlet weak var stopMotionCollectionView: KDDragAndDropCollectionView!
    @IBOutlet weak var soundButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    var colorSlider: ColorSlider?
    @IBOutlet weak var storyTimeLabel: UILabel!
    @IBOutlet weak var storyTimeButton: UIButton!
    @IBOutlet weak var storyTimePickerView: PickerView! {
        didSet {
            storyTimePickerView.dataSource = self
            storyTimePickerView.delegate = self
            storyTimePickerView.scrollingStyle = .default
            storyTimePickerView.selectionStyle = .overlay
            storyTimePickerView.tintColor = UIColor.white
            storyTimePickerView.selectionTitle.text = "seconds"
        }
    }


    @IBOutlet weak var selectStoryTimeView: UIView!
    @IBOutlet weak var undoButton: UIButton!
    @IBOutlet weak var doneButtonView: UIView!
    //    @IBOutlet weak var sketchView: SketchView!
    var sketchView: SketchView?
    @IBOutlet weak var editOptionsToolBar: UIView!
    /** holding the 2 imageViews original image and drawing & stickers */
    var drawingImageView: UIImageView?
    @IBOutlet weak var canvasView: UIView!
    // To hold the image
    @IBOutlet var imageView: UIImageView!
    @IBOutlet weak var imageViewHeightConstraint: NSLayoutConstraint!
    // To hold the drawings and stickers
    @IBOutlet weak var canvasImageView: UIImageView!

    @IBOutlet weak var topToolbar: UIView!
    @IBOutlet weak var bottomToolbar: UIView!

    @IBOutlet weak var topGradient: UIView!
    @IBOutlet weak var bottomGradient: UIView!

    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var deleteView: UIView! {
        didSet {
            deleteView.isHidden = true
        }
    }
    @IBOutlet weak var outtakesView: UIView!
    @IBOutlet weak var notesView: UIView!
    @IBOutlet weak var chatView: UIView!
    @IBOutlet weak var feedView: UIView!
    @IBOutlet weak var storyView: UIView!

    @IBOutlet weak var colorsCollectionView: UICollectionView!
    @IBOutlet weak var colorPickerView: UIView!
    @IBOutlet weak var colorPickerViewBottomConstraint: NSLayoutConstraint!

    @IBOutlet weak var coverView: UIView!
    @IBOutlet weak var soundView: UIView!
    @IBOutlet weak var horzFlipView: UIView!
    @IBOutlet weak var vertFlipView: UIView!
    @IBOutlet weak var horizontalFlipButton: UIButton!
    @IBOutlet weak var verticalFlipButton: UIButton!
    @IBOutlet weak var resequenceButton: UIButton!
    @IBOutlet weak var resequenceLabel: UILabel!
    // Controls
    @IBOutlet weak var cropButton: UIButton!
    @IBOutlet weak var stickerButton: UIButton!
    @IBOutlet weak var drawButton: UIButton!
    @IBOutlet weak var textButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var clearButton: UIButton!
    @IBOutlet weak var filterNameLabel: UILabel!

    var storyTimeOptions = ["1",
        "2",
        "3",
        "4",
        "5",
        "6",
        "7",
        "8",
        "9",
        "10"]

    public var videoUrl: SegmentVideos?

    public var image: UIImage?

    public var isMute: Bool = false
    /**
     Array of Stickers -UIImage- that the user will choose from
     */
    public var stickers: [UIImage] = []
    /**
     Array of Colors that will show while drawing or typing
     */
    public var colors: [UIColor] = []

    public weak var photoEditorDelegate: PhotoEditorDelegate?
    var colorsCollectionViewDelegate: ColorsCollectionViewDelegate!

    // list of controls to be hidden
    public var hiddenControls: [Control] = []

    var stickersVCIsVisible = false
    var drawColor: UIColor = UIColor.black
    var textColor: UIColor = UIColor.white
    var isDrawing: Bool = false
    var lastPoint: CGPoint!
    var swiped = false
    var lastPanPoint: CGPoint?
    var lastTextViewTransform: CGAffineTransform?
    var lastTextViewTransCenter: CGPoint?
    var lastTextViewFont: UIFont?
    var activeTextView: UITextView?
    var imageViewToPan: UIImageView?
    var isTyping: Bool = false

    var filterSwitcherView: SCSwipeableFilterView?
    var recordSession: SCRecordSession?
    var scPlayer: SCPlayer?
    var draggingCell: IndexPath?
    var lastMargeCell: IndexPath?
    var isCombineSegmentShow: Bool = false
    var currentCamaraMode: CamaraMode = .video
    
    var isMovable : Bool = true {
        didSet {
            self.stopMotionCollectionView.isMovable = isMovable
        }
    }
    var selectedVideoPercentage: Int = 0
    var stickersViewController: StickersViewController!

    var fromCell: IndexPath?
    
    
    var isVideo: Bool = false {
        didSet {
            if isVideo {
                selectStoryTimeButton.isHidden = true
                soundView.isHidden = false
                coverView.isHidden = false
            } else {
                selectStoryTimeButton.isHidden = false
                soundView.isHidden = true
                coverView.isHidden = true
            }
        }
    }

    // Preview Adjust
    weak var storyCamDelegate: StoryCameraDelegate?
    weak var outtakesDelegate: OuttakesTakenDelegate?
    
    public var slideShowImages: [SegmentVideos] = []
    public var videoUrls: [SegmentVideos] = []
    public var videoResetUrls: [SegmentVideos] = []
    var videos: [FilterImage] = []
    public var currentPlayVideo: Int = -1
    public var selectedItem: Int = 0
    var isImageCellSelect: Bool = false
    var deleteInFrame: Bool = false
    fileprivate var isViewAppear = false
    var dragAndDropManager: KDDragAndDropManager?
    var deleterectFrame: CGRect?
    var outtakesFrame: CGRect?
    var notesFrame: CGRect?
    var chatFrame: CGRect?
    var feedFrame: CGRect?
    var storyFrame: CGRect?

    var currentPage: Int = 0
    let locationManager = CLLocationManager()
    var lat = ""
    var long = ""
    var address = ""
    var playerObserver: Any?
    public var selectedVideoUrlSave: SegmentVideos?
    
    var isOriginalSequence = false

    var undoMgr = MyUndoManager<Void>()
    
    // Register Custom font before we load XIB
    public override func loadView() {
        registerFont()
        super.loadView()
    }

    public override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        self.outtakesFrame = CGRect.init(x: outtakesView.frame.origin.x, y: outtakesView.frame.origin.y + bottomToolbar.frame.origin.y + 20, width: outtakesView.frame.width, height: outtakesView.frame.height)
        self.notesFrame = CGRect.init(x: notesView.frame.origin.x, y: notesView.frame.origin.y + bottomToolbar.frame.origin.y + 20, width: notesView.frame.width, height: notesView.frame.height)
        self.chatFrame = CGRect.init(x: chatView.frame.origin.x, y: chatView.frame.origin.y + bottomToolbar.frame.origin.y + 20, width: chatView.frame.width, height: chatView.frame.height)
        self.feedFrame = CGRect.init(x: feedView.frame.origin.x, y: feedView.frame.origin.y + bottomToolbar.frame.origin.y + 20, width: feedView.frame.width, height: feedView.frame.height)
        self.storyFrame = CGRect.init(x: storyView.frame.origin.x, y: storyView.frame.origin.y + bottomToolbar.frame.origin.y + 20, width: storyView.frame.width, height: storyView.frame.height)
    }

    func setupColorSliderConstraints() {
        guard let colorSlider = self.colorSlider else {
            return
        }
        let colorSliderHeight = CGFloat(175)
        colorSlider.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            colorSlider.centerXAnchor.constraint(equalTo: editOptionsToolBar.centerXAnchor),
            colorSlider.topAnchor.constraint(equalTo: drawButton.topAnchor),
            colorSlider.widthAnchor.constraint(equalToConstant: 15),
            colorSlider.heightAnchor.constraint(equalToConstant: colorSliderHeight),
        ])
    }

    func setupColorSlider() {
        colorSlider = ColorSlider(orientation: .vertical, previewSide: .left)
        if let colorSlider = colorSlider {
            colorSlider.addTarget(self, action: #selector(colorSliderValueChanged(_:)), for: UIControlEvents.valueChanged)
            self.view.addSubview(colorSlider)
            self.setupColorSliderConstraints()
            colorSlider.isHidden = true
        }
    }
    
    func setupFilter() {
        if UIScreen.main.bounds.size.height == 812.0 {
            self.filterSwitcherView = SCSwipeableFilterView(frame: CGRect(x: -41, y: 0, width: 457, height: 812))
        } else {
            self.filterSwitcherView = SCSwipeableFilterView(frame: CGRect.init(origin: CGPoint.zero, size: UIScreen.main.bounds.size))
        }
        self.filterSwitcherView?.isUserInteractionEnabled = true
        self.filterSwitcherView?.isMultipleTouchEnabled = true
        self.filterSwitcherView?.isExclusiveTouch = false
        filterSwitcherView?.contentMode = .scaleAspectFill
        self.filterSwitcherView?.backgroundColor = UIColor.clear
        for subview in (filterSwitcherView?.subviews)! {
            subview.backgroundColor = UIColor.clear
        }
        //        self.filterSwitcherView?.isMirror = false
        self.canvasImageView.addSubview(filterSwitcherView!)
        
        let emptyFilter = SCFilter.empty()
        emptyFilter.name = ""
        let fadeFilter = SCFilter(contentsOf: Bundle.main.url(forResource: "fade", withExtension: "cisf"))
        fadeFilter.name = "Fade"
        let chromeFilter = SCFilter(contentsOf: Bundle.main.url(forResource: "chrome", withExtension: "cisf"))
        chromeFilter.name = "Chrome"
        let transferFilter = SCFilter(contentsOf: Bundle.main.url(forResource: "transfer", withExtension: "cisf"))
        transferFilter.name = "Transfer"
        let instantFilter = SCFilter(contentsOf: Bundle.main.url(forResource: "instant", withExtension: "cisf"))
        instantFilter.name = "Instant"
        let monoFilter = SCFilter(contentsOf: Bundle.main.url(forResource: "mono", withExtension: "cisf"))
        monoFilter.name = "Mono"
        let noirFilter = SCFilter(contentsOf: Bundle.main.url(forResource: "noir", withExtension: "cisf"))
        noirFilter.name = "Noir"
        let processFilter = SCFilter(contentsOf: Bundle.main.url(forResource: "process", withExtension: "cisf"))
        processFilter.name = "Process"
        let tonalFilter = SCFilter(contentsOf: Bundle.main.url(forResource: "tonal", withExtension: "cisf"))
        tonalFilter.name = "Tonal"
        let structureFilter = SCFilter(ciFilter: CIFilter(name: "CISharpenLuminance", withInputParameters: ["inputSharpness": 5.0]))
        structureFilter?.name = "Structure"
        
        let blueThermalFilter = SCFilter(contentsOf: Bundle.main.url(forResource: "bluethermal", withExtension: "cisf"))
        blueThermalFilter.name = "Custom#1"
        let falseFilter = SCFilter(contentsOf: Bundle.main.url(forResource: "false_filter", withExtension: "cisf"))
        falseFilter.name = "Custom#2"
        let greenFilter = SCFilter(contentsOf: Bundle.main.url(forResource: "green_filter", withExtension: "cisf"))
        greenFilter.name = "Custom#3"
        let orangeFilter = SCFilter(contentsOf: Bundle.main.url(forResource: "orange", withExtension: "cisf"))
        orangeFilter.name = "Custom#4"
        let purpleFilter = SCFilter(contentsOf: Bundle.main.url(forResource: "purple", withExtension: "cisf"))
        purpleFilter.name = "Custom#5"
        let purplethermalFilter = SCFilter(contentsOf: Bundle.main.url(forResource: "purplethermal", withExtension: "cisf"))
        purplethermalFilter.name = "Custom#6"
        let redFilter = SCFilter(contentsOf: Bundle.main.url(forResource: "red_filter", withExtension: "cisf"))
        redFilter.name = "Custom#7"
        let unsharpFilter = SCFilter(contentsOf: Bundle.main.url(forResource: "unsharpmask", withExtension: "cisf"))
        unsharpFilter.name = "Custom#8"
        let vignetteFilter = SCFilter(contentsOf: Bundle.main.url(forResource: "vignette", withExtension: "cisf"))
        vignetteFilter.name = "Custom#9"
        let vignetteprocessFilter = SCFilter(contentsOf: Bundle.main.url(forResource: "vignetteprocess", withExtension: "cisf"))
        vignetteprocessFilter.name = "Custom#10"


        
        filterSwitcherView?.filters = [emptyFilter,
                                       structureFilter,
                                       fadeFilter,
                                       chromeFilter,
                                       transferFilter,
                                       instantFilter,
                                       monoFilter,
                                       noirFilter,
                                       processFilter,
                                       tonalFilter,
                                       blueThermalFilter,
                                       falseFilter,
                                       greenFilter,
                                       orangeFilter,
                                       purpleFilter,
                                       purplethermalFilter,
                                       redFilter,
                                       unsharpFilter,
                                       vignetteFilter,
                                       vignetteprocessFilter]

    }
    
    public override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        guard let filterSwitcherObject = object as? SCSwipeableFilterView else {
            return
        }
        if filterSwitcherObject == self.filterSwitcherView {
            filterNameLabel.isHidden = false
            self.filterNameLabel.text = self.filterSwitcherView?.selectedFilter?.name
            self.filterNameLabel.alpha = 0
            UIView.animate(withDuration: 0.3, animations: {
                self.filterNameLabel.alpha = 1
            }, completion: { (isCompleted) in
                if isCompleted {
                    UIView.animate(withDuration: 0.3, delay: 1, options: UIViewAnimationOptions.transitionCrossDissolve, animations: {
                        self.filterNameLabel.alpha = 0
                    }, completion: { (isFinished) in
                        
                    })
                }
            })
        }
    }

    override public func viewDidLoad() {
        super.viewDidLoad()
        initLocation()
        setupUI()
        setupLayout()
        setupColorSlider()
        setupFilter()

        sketchView = SketchView(frame: CGRect.init(origin: CGPoint.zero, size: UIScreen.main.bounds.size))
        sketchView?.backgroundColor = UIColor.clear
        sketchView?.isUserInteractionEnabled = false
        sketchView?.sketchViewDelegate = self
        canvasImageView.addSubview(sketchView ?? UIView())


        if let img = image {
//            let mirrorImage = UIImage(cgImage: img.cgImage!, scale: 1.0, orientation: UIImageOrientation.right)
            filterSwitcherView?.setImageBy(img)
            stopMotionCollectionView.isHidden = true
            segmentEditOptionView.isHidden = true
            isVideo = false
        } else {
            scPlayer = SCPlayer()
            scPlayer?.scImageView = filterSwitcherView
            scPlayer?.delegate = self
            scPlayer?.loopEnabled = true
            self.dragAndDropManager = KDDragAndDropManager(
                canvas: self.view,
                collectionViews: [stopMotionCollectionView]
            )

            deleterectFrame = deleteView.frame
            isVideo = true
            videoResetUrls = []
            for video in videoUrls {
                if let videoAssest = video.copy() as? SegmentVideos {
                    if video.isCombineOneVideo {
                        videoAssest.isCombineOneVideo = true
                    }
                    videoResetUrls.append(videoAssest)
                }
            }
            
//            if videoUrls.count == 1 {
//                stopMotionCollectionView.isHidden = true
//                segmentEditOptionView.isHidden = true
//            }
        }
        
        setupVideoPlayer()

//        deleteView.layer.cornerRadius = deleteView.bounds.height / 2
//        deleteView.layer.borderWidth = 2.0
//        deleteView.layer.borderColor = UIColor.white.cgColor
        deleteView.clipsToBounds = true

        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.edges = .bottom
        edgePan.delegate = self
        self.view.addGestureRecognizer(edgePan)

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow),
                                               name: .UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide),
                                               name: .UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChangeFrame(_:)), name: .UIKeyboardWillChangeFrame, object: nil)

        configureCollectionView()
        stickersViewController = StickersViewController(nibName: "StickersViewController", bundle: Bundle(for: StickersViewController.self))
        hideControls()
        
        
    }

    func initLocation() {
        self.locationManager.requestWhenInUseAuthorization()

        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }

    }

    func setupUI() {
        if videoUrl != nil {
            selectStoryTimeButton.isHidden = true
            videoCoverView.isHidden = false
//            rearrangedView.isHidden = false
            stopMotionCollectionView.isHidden = false
            selectCoverButton.isHidden = false
        }
    }

    fileprivate func setupLayout() {
//        let layout = self.stopMotionCollectionView.collectionViewLayout as? UICollectionViewFlowLayout
//        layout?.spacingMode = UPCarouselFlowLayoutSpacingMode.fixed(spacing: 0)

        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false

        self.stopMotionCollectionView.register(UINib(nibName: ImageCollectionViewCell.identifier, bundle: nil), forCellWithReuseIdentifier: ImageCollectionViewCell.identifier)
    }

    func setupVideoPlayer() {
        
        SVProgressHUD.show()
        if videoUrls.count != 0 {
            if self.currentCamaraMode == .boom {
                stopMotionCollectionView.isHidden = true
                segmentEditOptionView.isHidden = true
                soundView.isHidden = true
                DispatchQueue.global().async {
                    let factory = VideoFactory(type: .boom, video: VideoOrigin(mediaType: nil, mediaUrl: URL.init(fileURLWithPath: self.videoUrls[0].url!.path), referenceURL: nil))
                    factory.assetTOcvimgbuffer({ [weak self] (urls) in
                        guard let strongSelf = self else { return }
                        strongSelf.newVideoCreate(url: strongSelf.videoUrls[0].url!, newUrl: urls)
                        }, { (progress) in
                            DispatchQueue.main.async {
                                SVProgressHUD.show()
                                SVProgressHUD.showProgress(Float(Float(progress.completedUnitCount) / Float(progress.totalUnitCount)))
                                
                            }
                           
                            print(progress)
                    }, failure: { (error) in
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                            UIApplication.shared.endIgnoringInteractionEvents()
                        }
                        print(error)
                    })
                }
            }
            else if self.currentCamaraMode == .slideShow {
                stopMotionCollectionView.isHidden = false
                segmentEditOptionView.isHidden = true
                stopMotionCollectionView.reloadData()
                if let img = self.videoUrls[0].image {
//                    let mirrorImage = UIImage(cgImage: img.cgImage!, scale: 1.0, orientation: UIImageOrientation.right)
                    filterSwitcherView?.setImageBy(img)
                    isVideo = false
                }
                postStoryButton.isEnabled = false
                postStoryButton.alpha = 0.5
            }
            
            selectCoverButton.isHidden = false
            videoCoverView.isHidden = false
//            rearrangedView.isHidden = false
            coverImageView.image = self.videoUrls[0].image
            self.videoUrl = self.videoUrls[0]
            horzFlipView.isHidden = true
            vertFlipView.isHidden = true
//            self.connVideoPlay()
        }
    }
    
    func newVideoCreate(url: URL, newUrl urls: URL) {
        print(url)
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
            UIApplication.shared.endIgnoringInteractionEvents()
        }
        do {
            let fileManager = FileManager.default
            
            // Check if file exists
            if fileManager.fileExists(atPath: url.path) {
                // Delete file
                try fileManager.removeItem(atPath: url.path)
                
                print("File Delete")
                
//                try fileManager.copyItem(at: urls, to: url)
                 try fileManager.moveItem(at: urls, to: url)
                print("Recording completed \(String(describing: url.path))")
                if self.currentCamaraMode == .boom {
                    self.videoUrls[0].currentAsset = SegmentVideos.getRecordSession(videoModel: [self.videoUrls[0]])
                    connVideoPlay()
                }
            } else {
                print("File does not exist")
            }
            
        }
        catch let error as NSError {
            print("An error took place: \(error)")
        }
    }
    
    func connVideoPlay() {
        DispatchQueue.main.async {
            self.currentPlayVideo = self.currentPlayVideo + 1

            if self.currentPlayVideo == self.videoUrls.count {
                self.currentPlayVideo = 0
            }
            self.currentPage = self.currentPlayVideo
            self.stopMotionCollectionView.reloadData()
        
            if self.videoUrls.count != 0 {

                let item = self.videoUrls[self.currentPlayVideo].currentAsset
                print("PlayerItem Duration: \(item?.duration.seconds)") // Shows the duration of all tracks together
                print("PlayerItem Tracks: \(item?.tracks.count)") // Shows same number of tracks as the VideoComposition Track count
                
                self.scPlayer?.setItemBy(item)
            }
            if self.thumbHeight.constant == 0.0 {
                self.scPlayer?.play()
            }
            if self.thumbSelectorView != nil && self.scPlayer != nil && self.scPlayer?.currentItem?.asset != nil {
                self.thumbSelectorView.asset = self.scPlayer?.currentItem?.asset
            }
        }
    }

    deinit {
        print("PhotoEditiorVC Deinit")
    }

    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let player = scPlayer {
            player.play()
            // Invoke callback every second
            let interval = CMTime(seconds:1.0, preferredTimescale: CMTimeScale(NSEC_PER_MSEC))
            
            // Queue on which to invoke the callback
            let mainQueue = DispatchQueue.main
            
            // Keep the reference to remove
            self.playerObserver = player.addPeriodicTimeObserver(forInterval: interval, queue: mainQueue) { [weak self] time in
                
                guard let strongSelf = self else { return }
                if let player = strongSelf.scPlayer {
                    if !time.seconds.isNaN {
                        let persiontag = CGFloat((time.seconds / player.playableDuration.seconds) * 100)
                        for visible in strongSelf.stopMotionCollectionView.visibleCells {
                            if let cell = visible as? ImageCollectionViewCell
                            {
                                cell.lblVideoersiontag.isHidden = true
                            }
                        }
                        
                        if String(describing: persiontag) != "nan" && String(describing: persiontag) != "inf" {
                            if !persiontag.isNaN && !persiontag.isInfinite {
                                strongSelf.selectedVideoPercentage = Int(persiontag)
                                
                                if let cell : ImageCollectionViewCell = strongSelf.stopMotionCollectionView.cellForItem(at: IndexPath.init(row: strongSelf.currentPage, section: 0)) as? ImageCollectionViewCell {
                                    if strongSelf.selectedVideoPercentage <= 100 {
                                        cell.lblVideoersiontag.text = "\(strongSelf.selectedVideoPercentage) %"
                                        cell.lblVideoersiontag.isHidden = false
                                    }
                                    else
                                    {
                                        cell.lblVideoersiontag.isHidden = true
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        isViewAppear = true
        observeState()
//        self.deleteView.isHidden = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        filterSwitcherView?.addObserver(self, forKeyPath: "selectedFilter", options: .new, context: nil)
    }
    var isPlayerInitialize = false
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.deleterectFrame = deleteView.frame
        if !isPlayerInitialize {
            isPlayerInitialize = true
            if self.currentCamaraMode != .boom {
                connVideoPlay()
            }
        }
    }

    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let player = scPlayer {
            player.pause()
            if let observer = self.playerObserver {
                player.removeTimeObserver(observer)
                self.playerObserver = nil
            }
        }
        
        removeObserveState()
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
        filterSwitcherView?.removeObserver(self, forKeyPath: "selectedFilter", context: nil)
    }

    func configureCollectionView() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 30, height: 30)
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        colorsCollectionView.collectionViewLayout = layout
        colorsCollectionViewDelegate = ColorsCollectionViewDelegate()
        colorsCollectionViewDelegate.colorDelegate = self
        if !colors.isEmpty {
            colorsCollectionViewDelegate.colors = colors
        }
        colorsCollectionView.delegate = colorsCollectionViewDelegate
        colorsCollectionView.dataSource = colorsCollectionViewDelegate

        colorsCollectionView.register(
            UINib(nibName: "ColorCollectionViewCell", bundle: Bundle(for: ColorCollectionViewCell.self)),
            forCellWithReuseIdentifier: "ColorCollectionViewCell")
    }

    func hideToolbar(hide: Bool) {
        topToolbar.isHidden = hide
        topGradient.isHidden = hide
        bottomToolbar.isHidden = hide
        bottomGradient.isHidden = hide
        editOptionsToolBar.isHidden = hide
    }

}

extension PhotoEditorViewController: ColorDelegate {

    func didSelectColor(color: UIColor) {
        if isDrawing {
//            self.drawColor = color
            sketchView?.lineColor = color
        } else if activeTextView != nil {
            activeTextView?.textColor = color
            textColor = color
        }
    }
}

extension PhotoEditorViewController: SCPlayerDelegate {

//    public func player(_ player: SCPlayer, didUpdateLoadedTimeRanges timeRange: CMTimeRange) {
//        self.stopMotionCollectionView.reloadData()
//        self.stopMotionCollectionView.layoutIfNeeded()
//        self.stopMotionCollectionView.setNeedsLayout()
//    }
   
    public func player(_ player: SCPlayer, didPlay currentTime: CMTime, loopsCount: Int) {
        self.stopMotionCollectionView.reloadData()
    }
    
    public func player(_ player: SCPlayer, didReachEndFor item: AVPlayerItem) {
        DispatchQueue.main.async {
            self.currentPlayVideo = self.currentPlayVideo + 1

            if self.currentPlayVideo == self.videoUrls.count {
                self.currentPlayVideo = 0
            }
            self.currentPage = self.currentPlayVideo
            self.stopMotionCollectionView.reloadData()
            self.stopMotionCollectionView.layoutIfNeeded()
            self.stopMotionCollectionView.setNeedsLayout()

            if self.videoUrls.count != 0 {
                let item = self.videoUrls[self.currentPlayVideo].currentAsset
                print("PlayerItem Duration: \(item?.duration.seconds)") // Shows the duration of all tracks together
                print("PlayerItem Tracks: \(item?.tracks.count)") // Shows same number of tracks as the VideoComposition Track count
                self.scPlayer?.setItemBy(item)
               // self.scPlayer?.setItemBy(self.videoUrls[self.currentPlayVideo])
                self.coverImageView.image = self.videoUrls[self.currentPlayVideo].image

            }

        }
    }

}

extension PhotoEditorViewController: PickerViewDataSource {

    // MARK: - PickerViewDataSource

    public func pickerViewNumberOfRows(_ pickerView: PickerView) -> Int {
        return storyTimeOptions.count
    }

    public func pickerView(_ pickerView: PickerView, titleForRow row: Int, index: Int) -> String {
        return storyTimeOptions[index]
    }

}

extension PhotoEditorViewController: PickerViewDelegate {

    // MARK: - PickerViewDelegate

    public func pickerViewHeightForRows(_ pickerView: PickerView) -> CGFloat {
        return 134.0
    }

    public func pickerView(_ pickerView: PickerView, didSelectRow row: Int, index: Int) {
        print(index)
    }

    public func pickerView(_ pickerView: PickerView, styleForLabel label: UILabel, highlighted: Bool) {

        label.font = UIFont(name: "Avenir-Heavy", size: 72.0)
        if highlighted {
            label.textColor = UIColor.white
        } else {
            label.textColor = UIColor.white.withAlphaComponent(0.75)
        }

        if label.text == "∞" {
            pickerView.selectionTitle.text = "no limit"
        } else {
            pickerView.selectionTitle.text = "seconds"
        }

    }

    public func pickerView(_ pickerView: PickerView, viewForRow row: Int, index: Int, highlighted: Bool, reusingView view: UIView?) -> UIView? {
        return nil
    }

}

extension PhotoEditorViewController: ThumbSelectorViewDelegate {

    public func didChangeThumbPosition(_ imageTime: CMTime) {
        scPlayer?.seek(to: imageTime, toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero)
        coverImageView.image = thumbSelectorView.thumbView.image

        if !isImageCellSelect {
            let image = thumbSelectorView.thumbView.image!
            
            videoUrls[self.currentPage].image = image
            videoUrls[self.currentPage].thumbTime = imageTime
        }

        isImageCellSelect = false
        coverImageView.image = videoUrls[self.currentPage].image

        DispatchQueue.main.async {
            self.stopMotionCollectionView.reloadData()
            self.stopMotionCollectionView.layoutIfNeeded()
            self.stopMotionCollectionView.setNeedsLayout()

        }
    }

}

// MARK: UICollectionViewDataSource
extension PhotoEditorViewController: UICollectionViewDataSource, KDDragAndDropCollectionViewDataSource {
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.stopMotionCollectionView {
            if currentCamaraMode == .slideShow && videoUrls.count > 0 {
                coverImageView.image = self.videoUrls[0].image
            }
            return videoUrls.count
        }
        return 0
    }

    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageCollectionViewCell.identifier, for: indexPath) as? ImageCollectionViewCell else {
            fatalError("Unable to find cell with '\(ImageCollectionViewCell.identifier)' reuseIdentifier")
        }
        
        let character = videoUrls[(indexPath as NSIndexPath).row].image
        var borderColor: CGColor! = UIColor.clear.cgColor
        var borderWidth: CGFloat = 0

        if (currentCamaraMode != .slideShow && self.currentPage == indexPath.row) || videoUrls[indexPath.row].isSelected {
            borderColor = UIColor(red: 82 / 255, green: 152 / 255, blue: 63 / 255, alpha: 1.0).cgColor
            borderWidth = 3
            cell.lblVideoersiontag.isHidden = false
        } else {
            borderColor = UIColor.white.cgColor
            borderWidth = 3
            cell.lblVideoersiontag.isHidden = true
            self.setDeleteFrame(view: cell)
        }

//        cell.image.image = character
        cell.isHidden = false

//        cell.image.isHidden = true
        cell.imagesStackView.tag = indexPath.row
 
        let images = videoUrls[(indexPath as NSIndexPath).row].videos
        
        let views = cell.imagesStackView.subviews
        for view in views {
            cell.imagesStackView.removeArrangedSubview(view)
        }
    
        if isOriginalSequence {
            cell.lblSegmentCount.text = "\(indexPath.row + 1)"
        } else {
            cell.lblSegmentCount.text = videoUrls[(indexPath as NSIndexPath).row].numberOfSegementtext
        }
        
        if currentCamaraMode != .slideShow && self.currentPage == indexPath.row {
            if videoUrls[(indexPath as NSIndexPath).row].isCombineOneVideo {
                
                let mainView = UIView.init(frame: CGRect(x: 0, y: 3, width: Double(41 * 1.2), height: Double(cell.imagesView.frame.height * 1.18)))
                
                let imageView = UIImageView.init(frame: CGRect(x: 0, y: 0, width: Double(41 * 1.2), height: Double(cell.imagesView.frame.height * 1.18)))
                imageView.image = images[0].image
                imageView.contentMode = .scaleToFill
                imageView.clipsToBounds = true
                mainView.addSubview(imageView)
                cell.imagesStackView.addArrangedSubview(mainView)
                
            }
            else {
                for imageName in images {
                    let mainView = UIView.init(frame: CGRect(x: 0, y: 3, width: Double(41 * 1.2), height: Double(cell.imagesView.frame.height * 1.18)))
                    
                    let imageView = UIImageView.init(frame: CGRect(x: 0, y: 0, width: Double(41 * 1.2), height: Double(cell.imagesView.frame.height * 1.18)))
                    imageView.image = imageName.image
                    imageView.contentMode = .scaleToFill
                    imageView.clipsToBounds = true
                    mainView.addSubview(imageView)
                    cell.imagesStackView.addArrangedSubview(mainView)
                }
            }
        }
        else {
            if videoUrls[(indexPath as NSIndexPath).row].isCombineOneVideo {
                let mainView = UIView.init(frame: CGRect(x: 0, y: 3, width: 41, height: 52))
                
                let imageView = UIImageView.init(frame: CGRect(x: 0, y: 0, width: 41, height: 52))
                imageView.image = images[0].image
                imageView.contentMode = .scaleToFill
                imageView.clipsToBounds = true
                mainView.addSubview(imageView)
                cell.imagesStackView.addArrangedSubview(mainView)
            }
            else {
                if videoUrls[indexPath.row].isSelected {
                    for imageName in images {
                        let mainView = UIView.init(frame: CGRect(x: 0, y: 3, width: Double(41 * 1.2), height: Double(cell.imagesView.frame.height * 1.18)))
                        
                        let imageView = UIImageView.init(frame: CGRect(x: 0, y: 0, width: Double(41 * 1.2), height: Double(cell.imagesView.frame.height * 1.18)))
                        imageView.image = imageName.image
                        imageView.contentMode = .scaleToFill
                        imageView.clipsToBounds = true
                        mainView.addSubview(imageView)
                        cell.imagesStackView.addArrangedSubview(mainView)
                    }

                } else {
                    for imageName in images {
                        let mainView = UIView.init(frame: CGRect(x: 0, y: 3, width: 41, height: 52))
                        let imageView = UIImageView.init(frame: CGRect(x: 0, y: 0, width: 41, height: 52))
                        imageView.image = imageName.image
                        imageView.contentMode = .scaleToFill
                        imageView.clipsToBounds = true
                        mainView.addSubview(imageView)
                        cell.imagesStackView.addArrangedSubview(mainView)
                    }
                }
            }
        }
        
        cell.imagesView.layer.cornerRadius = 5
        cell.imagesView.layer.borderWidth = borderWidth
        cell.imagesView.layer.borderColor = borderColor
        
        
        if let kdCollectionView = stopMotionCollectionView {
            if let draggingPathOfCellBeingDragged = kdCollectionView.draggingPathOfCellBeingDragged
            {
                if draggingPathOfCellBeingDragged.item == indexPath.item {
                    cell.isHidden = true
                    draggingCell = indexPath
                }
            }
        }
        
        return cell
    }

    // MARK : KDDragAndDropCollectionViewDataSource
    public func collectionView(_ collectionView: UICollectionView, dataItemForIndexPath indexPath: IndexPath) -> AnyObject {
        return videoUrls[indexPath.item]
    }

    public func collectionView(_ collectionView: UICollectionView, insertDataItem dataItem: AnyObject, atIndexPath indexPath: IndexPath) -> Void {

        if let di = dataItem as? SegmentVideos {
            videoUrls.insert(di, at: indexPath.item)
        }

    }
    public func collectionView(_ collectionView: UICollectionView, deleteDataItemAtIndexPath indexPath: IndexPath) -> Void {
        
        videoUrls.remove(at: indexPath.item)
        
    }
  
    func getUndo(index: Int) -> (()->Void) {
        return { ()->Void in
            self.videoUrls.remove(at: index)
        }
    }
    func getRedo(model: SegmentVideos, index: Int) -> (()->Void) {
        // This capture could work as long as non-class type object. The point is pass-by-value and record the value to data
        return { ()->Void in
            self.videoUrls.insert(model, at: index)
        }
    }
    
    func registerNewData(data: SegmentVideos, index: Int) {
        undoMgr.add(undo: getUndo(index: index), redo: getRedo(model: data, index: index))
    }
    
    func getDeleteUndo(model: SegmentVideos, data: Int) -> (()->Void) {
        return { ()->Void in
            self.videoUrls.insert(model, at: data)
        }
    }
    
    func getDeleteRedo(data: Int) -> (()->Void) {
        return { ()->Void in
            self.videoUrls.remove(at: data)
        }
    }
    
    func registerDeleteData(model: SegmentVideos, data: Int) {
        undoMgr.add(undo: getDeleteUndo(model: model, data: data), redo: getDeleteUndo(model: model, data: data))
        
    }
    
    func getNullDeleteUndo(model: SegmentVideos, data: Int) -> (()->Void) {
        return { ()->Void in
            
        }
    }
    
    func registerNullDeleteData(model: SegmentVideos, data: Int) {
        undoMgr.add(undo: getNullDeleteUndo(model: model, data: data), redo: getNullDeleteUndo(model: model, data: data))
        
    }
    
    public func collectionView(_ collectionView: UICollectionView, moveDataItemFromIndexPath from: IndexPath, toIndexPath to: IndexPath) -> Void {

        if isMovable {
            let videourl: SegmentVideos = videoUrls[from.item]
            let model = self.videoUrls[from.item].copy() as? SegmentVideos
            let modelTo = self.videoUrls[to.item].copy() as? SegmentVideos
            registerDeleteData(model: model!, data: from.item)
            videoUrls.remove(at: from.item)
            registerNewData(data: modelTo!, index: to.item)
            videoUrls.insert(videourl, at: to.item)
        }
        
        print("from item: \(from.item)")
        
        if fromCell == nil
        {
            if let kdCollectionView = collectionView as? KDDragAndDropCollectionView {
                if let draggingPathOfCellBeingDragged = kdCollectionView.draggingPathOfCellBeingDragged
                {
                    fromCell = draggingPathOfCellBeingDragged
                }
            }
        }
        
        print("to row : \(to.item)")
        lastMargeCell = to
        rearrangedView.isHidden = false
        
        if let kdCollectionView = collectionView as? KDDragAndDropCollectionView {
            if let draggingPathOfCellBeingDragged = kdCollectionView.draggingPathOfCellBeingDragged {
                if self.draggingCell != nil {
                    if !isMovable {
                        for visible in kdCollectionView.visibleCells {
                            self.setDeleteFrame(view: visible)
                        }
                        if draggingCell != to {
                            if let cell = kdCollectionView.cellForItem(at: to) {
                                self.setStickerObject(view: cell)
                            }
                        }
                    }
                    if to.item != self.currentPlayVideo {
                        self.selectedItem = to.row
                        self.currentPlayVideo = to.row
                        self.currentPage = self.currentPlayVideo
                    }
                }
            }
        }
    }

    public func collectionView(_ collectionView: UICollectionView, indexPathForDataItem dataItem: AnyObject) -> IndexPath? {

        guard let candidate = dataItem as? SegmentVideos else { return nil }
        
        for (i, item) in videoUrls.enumerated() {
            if candidate != item {
                continue
            }
            return IndexPath(item: i, section: 0)
        }
        
        return nil
    }

    public func collectionView(_ collectionView: UICollectionView, cellIsDraggableAtIndexPath indexPath: IndexPath) -> Bool {
        if self.thumbHeight.constant == 0.0 {
            return true
        }
        return false
    }

    public func collectionViewEdgesAndScroll(_ collectionView: UICollectionView, rect: CGRect) {
        
        if videoUrls.count > 1 {
            deleteView.isHidden = false
        }
        else {
            deleteView.isHidden = false
        }
        let newrect = rect.origin.y + collectionView.frame.origin.y
        let newrectData = CGRect.init(x: rect.origin.x, y: newrect, width: rect.width, height: rect.height)
       
        if !isMovable {
            guard let currentPlayer = self.scPlayer else { return }
            currentPlayer.pause()
            return
        }
        
        if deleterectFrame!.intersects(newrectData) == true {
            self.setDeleteFrame(view: storyView)
            self.setDeleteFrame(view: outtakesView)
            self.setDeleteFrame(view: notesView)
            self.setDeleteFrame(view: chatView)
            self.setDeleteFrame(view: feedView)
            UIView.animate(withDuration: 0.1, animations: { [weak self] () -> Void in
                guard let strongSelf = self else { return }
                strongSelf.setStickerObject(view: strongSelf.deleteView)
            })
        }
        else if outtakesFrame!.intersects(newrectData) == true {
            self.setDeleteFrame(view: deleteView)
            self.setDeleteFrame(view: notesView)
            self.setDeleteFrame(view: chatView)
            self.setDeleteFrame(view: feedView)
            self.setDeleteFrame(view: storyView)
            UIView.animate(withDuration: 0.1, animations: { [weak self] () -> Void in
                guard let strongSelf = self else { return }
                strongSelf.setStickerObject(view: strongSelf.outtakesView)
            })
        }
        else if notesFrame!.intersects(newrectData) == true {
            self.setDeleteFrame(view: deleteView)
            self.setDeleteFrame(view: outtakesView)
            self.setDeleteFrame(view: chatView)
            self.setDeleteFrame(view: feedView)
            self.setDeleteFrame(view: storyView)
            UIView.animate(withDuration: 0.1, animations: { [weak self] () -> Void in
                guard let strongSelf = self else { return }
                strongSelf.setStickerObject(view: strongSelf.notesView)
            })
        }
        else if chatFrame!.intersects(newrectData) == true {
            self.setDeleteFrame(view: deleteView)
            self.setDeleteFrame(view: outtakesView)
            self.setDeleteFrame(view: notesView)
            self.setDeleteFrame(view: feedView)
            self.setDeleteFrame(view: storyView)
            UIView.animate(withDuration: 0.1, animations: { [weak self] () -> Void in
                guard let strongSelf = self else { return }
                strongSelf.setStickerObject(view: strongSelf.chatView)
            })
        }
        else if feedFrame!.intersects(newrectData) == true {
            self.setDeleteFrame(view: deleteView)
            self.setDeleteFrame(view: outtakesView)
            self.setDeleteFrame(view: notesView)
            self.setDeleteFrame(view: chatView)
            self.setDeleteFrame(view: storyView)
            UIView.animate(withDuration: 0.1, animations: { [weak self] () -> Void in
                guard let strongSelf = self else { return }
                strongSelf.setStickerObject(view: strongSelf.feedView)
            })
        }
        else if storyFrame!.intersects(newrectData) == true {
            self.setDeleteFrame(view: deleteView)
            self.setDeleteFrame(view: outtakesView)
            self.setDeleteFrame(view: notesView)
            self.setDeleteFrame(view: chatView)
            self.setDeleteFrame(view: feedView)
            UIView.animate(withDuration: 0.1, animations: { [weak self] () -> Void in
                guard let strongSelf = self else { return }
                strongSelf.setStickerObject(view: strongSelf.storyView)
            })
        }
        else {
            self.setDeleteFrame(view: deleteView)
            self.setDeleteFrame(view: outtakesView)
            self.setDeleteFrame(view: notesView)
            self.setDeleteFrame(view: chatView)
            self.setDeleteFrame(view: feedView)
            self.setDeleteFrame(view: storyView)
        }

        guard let currentPlayer = self.scPlayer else { return }
        currentPlayer.pause()
    }

    public func collectionViewLastEdgesAndScroll(_ collectionView: UICollectionView, rect: CGRect) {
        let newrect = rect.origin.y + collectionView.frame.origin.y
        let newrectData = CGRect.init(x: rect.origin.x, y: newrect, width: rect.width, height: rect.height)
        if !isMovable {
            if self.lastMargeCell != self.draggingCell {
                stopMotionCollectionView.isUserInteractionEnabled = false
                segmentEditOptionView.isHidden = true
                segmentTypeMergeView.isHidden = false
            }
            else
            {
                DispatchQueue.main.async {
                    guard let currentPlayer = self.scPlayer else { return }
                    currentPlayer.play()
                }
            }
            return
        }
        
//        let model = self.videoUrls[self.draggingCell!.item].copy() as? SegmentVideos
//        let modelTo = self.videoUrls[self.lastMargeCell!.item].copy() as? SegmentVideos
//        registerDeleteData(model: model!, data: self.fromCell!.item)
//        registerNewData(data: modelTo!, index: self.lastMargeCell!.item)
//        fromCell = nil
        
        if deleterectFrame!.intersects(newrectData) == true {
            print("DELETEEEE.Done...")
            if let kdCollectionView = collectionView as? KDDragAndDropCollectionView {

                if let draggingPathOfCellBeingDragged = kdCollectionView.draggingPathOfCellBeingDragged {
                    if draggingCell != nil {
                        if draggingPathOfCellBeingDragged.item == draggingCell!.item {
//                            if videoUrls.count > 1 {
                                let album = SCAlbum.shared
                                album.albumName = "\(AppSetting.appName) – Recycle"
                                album.saveMovieToLibrary(movieURL: videoUrls[draggingPathOfCellBeingDragged.item].url!)
                                let model = self.videoUrls[draggingPathOfCellBeingDragged.row].copy() as? SegmentVideos
                                registerNullDeleteData(model: model!, data: draggingPathOfCellBeingDragged.row)
                                registerDeleteData(model: model!, data: draggingPathOfCellBeingDragged.row)
                                videoUrls.remove(at: draggingPathOfCellBeingDragged.row)
                                self.currentPlayVideo = -1
                                self.connVideoPlay()
//                            }
                        }
                    }
                }
            }
        }
        else if outtakesFrame!.intersects(newrectData) == true {
            print("Outtakes Done...")
            if let kdCollectionView = collectionView as? KDDragAndDropCollectionView {
                if let draggingPathOfCellBeingDragged = kdCollectionView.draggingPathOfCellBeingDragged {
                    if draggingCell != nil {
                        if draggingPathOfCellBeingDragged.item == draggingCell!.item {
                            selectedVideoUrlSave = videoUrls[draggingPathOfCellBeingDragged.item]
                            outtakesView.isUserInteractionEnabled = false
                            self.saveButtonTapped(outtakesView)
                           
                            videoUrls.remove(at: draggingPathOfCellBeingDragged.item)
                            self.currentPlayVideo = -1
                            self.connVideoPlay()
                        }
                    }
                }
            }
        }
        else if notesFrame!.intersects(newrectData) == true {
            print("Notes Done...")
            if let kdCollectionView = collectionView as? KDDragAndDropCollectionView {
                if let draggingPathOfCellBeingDragged = kdCollectionView.draggingPathOfCellBeingDragged {
                    if draggingCell != nil {
                        if draggingPathOfCellBeingDragged.item == draggingCell!.item {
                            selectedVideoUrlSave = videoUrls[draggingPathOfCellBeingDragged.item]
                            self.notesButtonClicked(notesView)
                        }
                    }
                }
            }
        }
        else if chatFrame!.intersects(newrectData) == true {
            print("Chat Done...")
            if let kdCollectionView = collectionView as? KDDragAndDropCollectionView {
                if let draggingPathOfCellBeingDragged = kdCollectionView.draggingPathOfCellBeingDragged {
                    if draggingCell != nil {
                        if draggingPathOfCellBeingDragged.item == draggingCell!.item {
                            selectedVideoUrlSave = videoUrls[draggingPathOfCellBeingDragged.item]
                            self.messageClicked(chatView)
                        }
                    }
                }
            }
        }
        else if feedFrame!.intersects(newrectData) == true {
            print("Feed Done...")
            if let kdCollectionView = collectionView as? KDDragAndDropCollectionView {
                if let draggingPathOfCellBeingDragged = kdCollectionView.draggingPathOfCellBeingDragged {
                    if draggingCell != nil {
                        if draggingPathOfCellBeingDragged.item == draggingCell!.item {
                            selectedVideoUrlSave = videoUrls[draggingPathOfCellBeingDragged.item]
                            self.btnPostToFeedClick(feedView)
                        }
                    }
                }
            }
        }
        else if storyFrame!.intersects(newrectData) == true {
            print("Story Done...")
            if let kdCollectionView = collectionView as? KDDragAndDropCollectionView {
                if let draggingPathOfCellBeingDragged = kdCollectionView.draggingPathOfCellBeingDragged {
                    if draggingCell != nil {
                        if draggingPathOfCellBeingDragged.item == draggingCell!.item {
                            selectedVideoUrlSave = videoUrls[draggingPathOfCellBeingDragged.item]
                            self.continueButtonPressed(storyView)
                        }
                    }
                }
            }
        }
        DispatchQueue.main.async {
            guard let currentPlayer = self.scPlayer else { return }
            currentPlayer.play()
        }
    }

    // Delegate for highlighting Delete Button
    func setStickerObject(view: UIView) {
        UIView.animate(withDuration: 0.2) { () -> Void in
            view.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
        }

    }

    // Delegate for delete button Normal Position
    func setDeleteFrame(view: UIView) {
        UIView.animate(withDuration: 0.2) { () -> Void in
            view.transform = CGAffineTransform(scaleX: 1, y: 1)
        }
    }

}


extension PhotoEditorViewController {

    func observeState() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.enterBackground), name: .UIApplicationDidEnterBackground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.enterForeground), name: .UIApplicationDidBecomeActive, object: nil)
    }

    func removeObserveState() {
        NotificationCenter.default.removeObserver(self, name: .UIApplicationDidEnterBackground, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIApplicationDidBecomeActive, object: nil)
    }

    func enterBackground(_ notifi: Notification) {
        if isViewAppear {
            guard let currentPlayer = self.scPlayer else { return }
            currentPlayer.pause()
        }
    }

    func enterForeground(_ notifi: Notification) {
        if isViewAppear {
            guard let currentPlayer = self.scPlayer else { return }
            currentPlayer.play()
        }
    }

}

// MARK: UICollectionViewDelegate
extension PhotoEditorViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.stopMotionCollectionView {
            if currentCamaraMode == .slideShow {
                let filteredVideoUrls = self.videoUrls.filter({ (segmentVideo) -> Bool in
                    return segmentVideo.isSelected
                })
                if !(filteredVideoUrls.count == 6 && !self.videoUrls[indexPath.row].isSelected) {
                    if let img = self.videoUrls[indexPath.row].image {
//                        let mirrorImage = UIImage(cgImage: img.cgImage!, scale: 1.0, orientation: UIImageOrientation.right)
                        filterSwitcherView?.setImageBy(img)
                        isVideo = false
                    }
                    //                self.currentPage = indexPath.row
                    self.videoUrls[indexPath.row].isSelected = !self.videoUrls[indexPath.row].isSelected
                    self.stopMotionCollectionView.reloadData()
                    let filteredVideoUrls = self.videoUrls.filter({ (segmentVideo) -> Bool in
                        return segmentVideo.isSelected
                    })
                    if filteredVideoUrls.count == 6 {
                        postStoryButton.isEnabled = true
                        postStoryButton.alpha = 1.0
                    } else {
                        postStoryButton.isEnabled = false
                        postStoryButton.alpha = 0.5
                    }
                }
            } else {
                selectedItem = indexPath.row
                isImageCellSelect = true
                self.currentPlayVideo = (indexPath as NSIndexPath).row - 1
                self.connVideoPlay()
            }
            
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let images = self.videoUrls[indexPath.row].videos
        if videoUrls[indexPath.row].isCombineOneVideo {
            if currentPage == indexPath.row {
                return CGSize(width: 41 * 1.2 , height: Double(98 * 1.17))
            }
            else {
                return CGSize(width: 41, height: 98)
            }
        }
        else {
            if currentPage == indexPath.row && currentCamaraMode != .slideShow {
                return CGSize(width: (Double(images.count * 41) * 1.2) , height: Double(98 * 1.17))
            }
            else {
                if self.videoUrls[indexPath.row].isSelected {
                    return CGSize(width: (Double(images.count * 41) * 1.2) , height: Double(98 * 1.17))
                } else {
                    return CGSize(width: (images.count * 41), height: 98)
                }
            }
        }
    }

}

extension PhotoEditorViewController: CLLocationManagerDelegate {
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            lat = "\(location.coordinate.latitude)"
            long = "\(location.coordinate.longitude)"
            manager.stopUpdatingLocation()
            saveAddressFor(location)
        }
    }

    func saveAddressFor(_ location: CLLocation) {
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(location) { [weak self] (placemarks, error) in
            guard let strongSelf = self else { return }
            if let placeMarks = placemarks {
                let placemark = placeMarks.first
                let number = placemark?.subThoroughfare
                let bairro = placemark?.subLocality
                let street = placemark?.thoroughfare
                print("\(street ?? ""),\(number ?? "") - \(bairro ?? "")")
                strongSelf.address = street ?? ""
            }
        }
    }
}

