
//
//  AppDelegate.swift
//  ProManager
//
//  Created by Jatin Kathrotiya on 16/05/17.
//  Copyright © 2017 Jatin Kathrotiya. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import AWSS3
import AWSCore
import Fabric
import Crashlytics
import TwitterKit
import Alamofire
import SystemConfiguration
import FBSDKCoreKit
import GoogleMaps
import GooglePlaces
import Firebase
import UserNotifications
import GoogleSignIn
import Stickerpipe
import Pageboy
import UserNotifications
import PushKit
import Intents
import CallKit
import OpenTok
import TwilioVideo

let apiKey = "70e36a7b2ca143977e700a83dc03c82e"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    let callManager = SpeakerboxCallManager()
    var accessToken = ""
    var voipPushToken = ""
    var voipDict: [AnyHashable: Any] = [:]
    var window: UIWindow?
    var sessionToken: String?
    var activeUser: User?
    var placeHolder: PlaceHolder?
    var socialId: String?
    var PostURl: String?
    var CoverUrl: String?
    var locK: Bool = false
    var receiverObj: ChatListModel?
    var coverPicImg: UIImage = UIImage(named: "bgSidemenuTop")!
    var placeHolderImg: UIImage = UIImage(named: "userProfilePlaceholder")!
    var videos: [Item] = [] // Videos For You tube
    var fcmDeviceToken: String = ""
    internal var shouldRotate = false
    var needtoRefresh: Bool = false
    var mainChatList = [ChatListModel]()
    var messageRefM: DatabaseReference?
    var isAppActive: Bool = true
    var userGroups: [ChatListModel] = [ChatListModel]()
    var callkitProvide: CXProvider?
    var isOnCall: Bool = false
    var calluuid: UUID?

    var viewDrag: UIView!
    var viewDrag2: UIView!
    var viewDrag3: UIView!
    var viewDrag4: UIView!

    var panGesture2 = UIPanGestureRecognizer()
    var tapGesture2 = UITapGestureRecognizer()

    var panGesture3 = UIPanGestureRecognizer()
    var tapGesture3 = UITapGestureRecognizer()

    var panGesture4 = UIPanGestureRecognizer()
    var tapGesture4 = UITapGestureRecognizer()

    var panGesture = UIPanGestureRecognizer()
    var tapGesture = UITapGestureRecognizer()

    var callingVC: CallingVC!
    var addPeopleOnCall: AddPeopleVC!
    var objVideoAdVC: VideoAdVC!

    var pinchZoom = UIPinchGestureRecognizer()
    var pinchZoom2 = UIPinchGestureRecognizer()
    var pinchZoom3 = UIPinchGestureRecognizer()
    var pinchZoom4 = UIPinchGestureRecognizer()

    var isMultiPhotoLayout: Bool {
        get {
            return UserDefaults.standard.bool(forKey: "isMultiPhotoLayout")
        } set {
            UserDefaults.standard.set(newValue, forKey: "isMultiPhotoLayout")
        }
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        self.setPanAndTapGestures()
        UIApplication.shared.applicationIconBadgeNumber = 0
        self.registerForPushKit()
        removeKeyChainDataIfNewUser()
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().shouldToolbarUsesTextFieldTintColor = true
        IQKeyboardManager.sharedManager().disabledDistanceHandlingClasses = [SignUpStepTwoViewController.self, PostCommentsViewController.self, LRListViewController.self, ChattingVC.self]
        IQKeyboardManager.sharedManager().disabledToolbarClasses = [SignUpStepTwoViewController.self, PostCommentsViewController.self, LRListViewController.self, ChattingVC.self]

        print("Post Total in Upload Count: \(SaveUploadModel.sharedInstance.saveUploadModel.count)")
        print("Story Total in Upload Count: \(SaveUploadModel.sharedInstance.saveStoryUploadModel.count)")
        for (_, item) in SaveUploadModel.sharedInstance.saveUploadModel.enumerated()
        {
            print("Remaining Post Links count : \(SaveUploadModel.sharedInstance.saveUploadModel.count)")
            print("Remaining Post States : \(item.state!)")
            print("Remaining Post effectString : \(String(describing: item.effectString))")
            print("Remaining Post URL : \(String(describing: item.urlOfFile))")
            print("Remaining Post Links : \(item.url?.absoluteString ?? "")")
        }

        for (_, item) in SaveUploadModel.sharedInstance.saveStoryUploadModel.enumerated()
        {
            print("Remaining Story Links count : \(SaveUploadModel.sharedInstance.saveStoryUploadModel.count)")
            print("Remaining Story States : \(item.state!)")
            print("Remaining Story effectString : \(String(describing: item.effectString))")
            print("Remaining Story URL : \(String(describing: item.urlOfFile))")
            print("Remaining Story Links : \(item.url?.absoluteString ?? "")")
        }

        InternetConnectionAlert.shared.enable = true

        // IF NEED EXTRA CONFIGURATION
        var config = InternetConnectionAlert.Configuration()
        config.kBG_COLOR = UIColor.green8215284
        InternetConnectionAlert.shared.config = config


        // Initialize sign-in
//        var configureError: NSError?
//        GGLContext.sharedInstance().configureWithError(&configureError)
        // assert(configureError == nil, "Error configuring Google services: \(configureError)")

        STKStickersManager.initWithApiKey(apiKey)
        STKStickersManager.setStartTimeInterval()
        STKStickersManager.setUserAsSubscriber(true)
        STKStickersManager.setUserKey("11")
        Twitter.sharedInstance().start(withConsumerKey: "pqsDvPyRwFKtwWAGFtxUWpP2C", consumerSecret: "a5ZhCvWFPJIR3g7vzzGWMGUh3CuzbnFh2cROxHjEdUlvnERtMB")
        Fabric.with([Crashlytics.self])
        Fabric.with([Twitter.self])
        let ud = UserDefaults.init(suiteName: AppSetting.sharedIdent)
        if let data = ud!.object(forKey: "PlaceHolder") as? Data {
            NSKeyedUnarchiver.setClass(PlaceHolder.self, forClassName: "PlaceHolder")
            let unarc = NSKeyedUnarchiver(forReadingWith: data)
            if let placeHolder = unarc.decodeObject(forKey: "root") as? PlaceHolder {
                self.placeHolder = placeHolder
            }
        }
        if let placeHolder = self.placeHolder {
            let lastPath = placeHolder.profilePic?.components(separatedBy: "/").last
            let dirPath = FileManager.documentsDir().appendingFormat("/%@", lastPath!)
            if let placeHolderImage = UIImage(contentsOfFile: dirPath) {
                self.placeHolderImg = placeHolderImage
            }
            let lastPathCover = placeHolder.coverPic?.components(separatedBy: "/").last
            let dirPathcover = FileManager.documentsDir().appendingFormat("/%@", lastPathCover!)
            self.coverPicImg = UIImage(contentsOfFile: dirPathcover) ?? UIImage()
        }
        GMSServices.provideAPIKey("AIzaSyDRFfyh2Dce1bj7e8WuJyY8ub7sNLPfwrY")
        GMSPlacesClient.provideAPIKey("AIzaSyDRFfyh2Dce1bj7e8WuJyY8ub7sNLPfwrY")
        FirebaseApp.configure()
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        //Messaging.messaging().delegate = self
        self.loadPlaceHolder()
        LocationManager.sharedInstance.refreshLocation()
        _ = DataBaseManager.sharedInstance
        if let data = ud!.object(forKey: "loginUser") as? Data {
            NSKeyedUnarchiver.setClass(User.self, forClassName: "User")
            let unarc = NSKeyedUnarchiver(forReadingWith: data)
            if let user = unarc.decodeObject(forKey: "root") as? User {
                if UserDefaults.standard.value(forKey: Constants.messageFilterType) != nil {
                    let chatType: Int = UserDefaults.standard.integer(forKey: Constants.messageFilterType)
                    FirebaseChatHelper.sharedInstance.chatFilterType = chatType
                }
                appDelegate?.activeUser = user
                FirebaseChatHelper.sharedInstance.createOrUpdateUserInFirebase(online: true, userObj: (appDelegate?.activeUser)!, isLogout: false)
                self.checkUnreadCount(isDelay: false)
                FirebaseChatHelper.sharedInstance.getUserProfile(userObj: (appDelegate?.activeUser)!, block: { (snapshot) in
                    print(snapshot)
                    if let isForward: Bool = snapshot["isforward"] as? Bool {
                        appDelegate?.activeUser?.isAllowForward = isForward
                    }
                })
            }
        }
        sessionToken = ud?.object(forKey: "sessionToken") as? String
        if sessionToken != nil {
            if let ch = appDelegate?.activeUser?.channelName, ch.count > 0 {
//              let mainNav = Controllers.ProTabBarController.controller as? ProTabBarController
                let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                let mainNav = storyboard.instantiateViewController(withIdentifier: "PageViewController")
                appDelegate?.window?.rootViewController = mainNav
               
                
                InternetConnectionAlert.shared.internetConnectionHandler = { reachability in
                    if reachability.connection != .none {
                        StoryDataManager.shared.startUpload()
                        PostDataManager.shared.startUpload()
                    }
                }
            } else {
                guard let loginNav = Controllers.LoginNavigation.controller as? UINavigationController else { return false }
                appDelegate?.window?.rootViewController = loginNav
            }
        } else {
            guard let loginNav = Controllers.LoginNavigation.controller as? UINavigationController else {
                return false
            }
            appDelegate?.window?.rootViewController = loginNav
        }
        
        var path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let url = URL(fileURLWithPath: path)
        print("Application Path :- \(url)")
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        self.registorRemoteNotification(application: application)
        return true
    }

    func removeKeyChainDataIfNewUser() {
        let isFirstTime = UserDefaults.standard.object(forKey: "isFirstTime")
        if isFirstTime == nil {
            UserDefaults.standard.set(false, forKey: "isFirstTime")
            TouchIdAuth.auth.removeKeyChainData()
            SaveUploadModel.sharedInstance.clear()
        }
    }

    func registorRemoteNotification(application: UIApplication) {
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self as? UNUserNotificationCenterDelegate
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: { _, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
    }

    func loadPlaceHolder() {
        ProManagerApi.getPlaceHolder.request(Result<PlaceHolder>.self).subscribe(onNext: { (response) in
            if response.status == ResponseType.success {
                self.placeHolder = response.result
                if let placeholder = self.placeHolder {
                    let ud = UserDefaults.standard
                    NSKeyedArchiver.setClassName("PlaceHolder", for: PlaceHolder.self)
                    ud.set(NSKeyedArchiver.archivedData(withRootObject: placeholder), forKey: "PlaceHolder")
                }
                UserDefaults.standard.synchronize()
                let lastPath = self.placeHolder?.profilePic?.components(separatedBy: "/").last
                let dirPath = FileManager.documentsDir().appendingFormat("/%@", lastPath!)
                if !FileManager.default.fileExists(atPath: dirPath) {
                    let destination = DownloadRequest.suggestedDownloadDestination()
                    Alamofire.download(URL.init(string: (self.placeHolder?.profilePic)!)!, to: destination).response(completionHandler: { response in
                        if let placeholderImage = UIImage(contentsOfFile: dirPath) {
                            self.placeHolderImg = placeholderImage
                        }
                    })
                } else {
                    if let placeHolderImage = UIImage(contentsOfFile: dirPath) {
                        self.placeHolderImg = placeHolderImage
                    }
                }
                let lastPathCover = self.placeHolder?.coverPic?.components(separatedBy: "/").last
                let dirPathcover = FileManager.documentsDir().appendingFormat("/%@", lastPathCover!)
                if !FileManager.default.fileExists(atPath: dirPathcover) {
                    let destination = DownloadRequest.suggestedDownloadDestination()
                    Alamofire.download(URL.init(string: (self.placeHolder?.coverPic)!)!, to: destination).response(completionHandler: { response in
                        if let coverPic = UIImage(contentsOfFile: dirPathcover) {
                            self.coverPicImg = coverPic
                        }
                    })
                } else {
                    if let coverPic = UIImage(contentsOfFile: dirPathcover) {
                        self.coverPicImg = coverPic
                    }
                }
            }

        }, onError: { error in

        }, onCompleted: {

        }).addDisposableTo(rx_disposeBag)
    }

//    func removeKeyChainDataIfNewUser() {
//        let isFirstTime = UserDefaults.standard.object(forKey: "isFirstTime")
//        if isFirstTime == nil {
//            UserDefaults.standard.set(false, forKey: "isFirstTime")
//            TouchIdAuth.auth.removeKeyChainData()
//        }
//    }


    // MARK: To get chat list and remove chat list observer

    func removeChatListObserver() {
        if messageRefM != nil {
            messageRefM?.removeAllObservers()
        }
    }

    func getRecentChatList(currentUser: User, block: @escaping chatBlock) {
        messageRefM = FirebaseChatHelper.sharedInstance.ref.child("\(FirebaseChatHelper.DatabaseTableKeys.kMessageStoreKey)").child("\((String(describing: currentUser.id!)))")
        _ = messageRefM?.observe(.value, with: {(snapshot) -> Void in
            if snapshot.exists() {
                block(snapshot.value as? NSDictionary ?? NSDictionary())
            } else {
                block(NSDictionary())
            }
        })
    }

    func checkUnreadCount(isDelay: Bool) {
        self.getRecentChatList(currentUser: (appDelegate?.activeUser)!, block: { [weak self] (snapshot) in
            self?.makeArrayFromFirebase(arrChatList: snapshot, isDelay: isDelay)
        })
    }

    func makeArrayFromFirebase(arrChatList: NSDictionary, isDelay: Bool) {
        appDelegate?.userGroups.removeAll()
        var unreadCount: Int = 0
        appDelegate?.mainChatList.removeAll()
        for obj in arrChatList {
            if let _: NSDictionary = obj.value as? NSDictionary {
                let objChat = ChatListModel(JSON: (obj.value as? [String: AnyObject])!)
                objChat?.id = obj.key as? String ?? ""
                unreadCount = unreadCount + (objChat?.unreadCount)!
                if objChat?.conversation_type == 1 {
                    FirebaseChatHelper.sharedInstance.updateTokenInGroups(groupObj: objChat!)
                    appDelegate?.userGroups.append(objChat!)
                }
                if objChat?.conversation_type == 1 {
                    // FirebaseChatHelper.sharedInstance.updateTokenInGroups(groupObj: objChat!)
                    // appDelegate?.userGroups.append(objChat!)
                    if objChat?.displayName.count == 0 {
                        print("issue here")
                    } else {
                        appDelegate?.mainChatList.append(objChat!)
                    }
                } else {
                    if objChat?.displayName.count == 0 {
                        print("issue here")
                    } else {
                        if objChat?.lastMessage.count == 0 {
                            
                        } else {
                            appDelegate?.mainChatList.append(objChat!)
                        }
                    }
                }
            }
        }
        if let list = appDelegate?.mainChatList{
            if list.count >= 1{
                appDelegate?.mainChatList.sort(by: { $0.dateObj.compare($1.dateObj) == .orderedDescending })
            }
        }
        let countDict: [String: Int] = ["count": unreadCount]
        NotificationCenter.default.post(name: NSNotification.Name(Constants.unreadChatCount), object: nil, userInfo: countDict)
        print("Unread count is \(unreadCount)")
    }

    func application(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Void) {
        /*
         Store the completion handler.
         */
        //  AWSS3TransferUtility.interceptApplication(application, handleEventsForBackgroundURLSession: identifier, completionHandler: completionHandler)
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {

        self.voipDict = [:]
        if activeUser != nil {
            if activeUser?.id?.count != 0 {
                FirebaseChatHelper.sharedInstance.createOrUpdateUserInFirebase(online: false, userObj: (activeUser)!, isLogout: false)
                NotificationCenter.default.post(name: NSNotification.Name(Constants.appBackGround), object: nil, userInfo: nil)
                UserDefaults.init(suiteName: AppSetting.sharedIdent)!.set(FirebaseChatHelper.sharedInstance.chatFilterType, forKey: Constants.messageFilterType)
            }

        }
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {

        if activeUser != nil {
            if activeUser?.id?.count != 0 {
                FirebaseChatHelper.sharedInstance.createOrUpdateUserInFirebase(online: true, userObj: (activeUser)!, isLogout: false)
                if self.voipDict.count > 1 {
                    self.retriveTokenAndStartCalling()
                }
            }
        }
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        FBSDKAppEvents.activateApp()
        sessionToken = UserDefaults.init(suiteName: AppSetting.sharedIdent)!.object(forKey: "sessionToken") as? String

        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        saveContext()
        if activeUser != nil {
            if activeUser?.id?.characters.count != 0 {
                FirebaseChatHelper.sharedInstance.setCallStateForUser(onCall: false, userObj: (appDelegate?.activeUser)!)
                NotificationCenter.default.post(name: NSNotification.Name(Constants.appTerminate), object: nil, userInfo: nil)
            }

        }
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey: Any] = [:]) -> Bool {
        if Twitter.sharedInstance().application(app, open: url, options: options) {
            return true
        }
        if url.absoluteString.contains("com.googleusercontent.apps.494482100501-87mtftfvdo6ct5v5b8kdbfh82gm674oo") {
            return GIDSignIn.sharedInstance().handle(url,
                                                     sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                                     annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        }

        let handled = FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String ?? "", annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        // If you handle other (non Twitter Kit) URLs elsewhere in your app, return true. Otherwise
        return handled
    }

    func isValidEmail(testStr: String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }

    func showAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) {
            (action) in
            alertController .dismiss(animated: true, completion: nil)
        }
        alertController.addAction(OKAction)
        self.window!.rootViewController!.present(alertController, animated: true) {

        }
    }

    func connectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) { zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        var flags: SCNetworkReachabilityFlags = []
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
    
    @discardableResult
    func checkInternetConnection()->Bool {
        if !self.connectedToNetwork() {
            self.showAlert(title: AppSetting.appName, message: Messages.kInternet)
            return false
        }
        return true
    }

    /*   func application(_ application: UIApplication,
                     supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
//        if window == MMLandscapeWindow.shared {
//            return .allButUpsideDown
//        } else {
           return shouldRotate ? .allButUpsideDown : .portrait
//        }
       
    } */

//    func adding<T:Addable>(a:T,b:T) -> T {
//        return a + b
//    }

    @nonobjc func application(application: UIApplication,
                              didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
       // Messaging.messaging().apnsToken = deviceToken
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {

        if let messageID = userInfo["gcmMessageIDKey"] {
            print("Message ID: \(messageID)")
        }
        print(userInfo)
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        if response.notification.request.content.categoryIdentifier == "TIMER_EXPIRED" {
            // Handle the actions for the expired timer.
            if response.actionIdentifier == "SNOOZE_ACTION" {
                // Invalidate the old timer and create a new one. . .
            }
            else if response.actionIdentifier == "STOP_ACTION" {
                // Invalidate the timer. . .
            }
        }
        // Else handle actions for other notification types. . .
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        UIApplication.shared.applicationIconBadgeNumber = 0
        if application.applicationState.hashValue == 1 {

            if let messageID = userInfo["gcm.message_id"] {
                print("Message ID: \(messageID)")
                sessionToken = UserDefaults.init(suiteName: AppSetting.sharedIdent)!.object(forKey: "sessionToken") as? String
                if sessionToken != nil {
                    if let ch = appDelegate?.activeUser?.channelName, ch.count > 0 {

                        if let type = userInfo["gcm.notification.type"] as? String, type == "others" {
                            if let rootController = appDelegate?.window?.rootViewController as? PageboyViewController {
                                if rootController.currentIndex == 2 {

                                    if let objVc = rootController.dataSource?.viewController(for: rootController, at: 2) as? ProTabBarController {
                                        objVc.selectedIndex = 3
                                    }
                                    //                                rootController.navigationController?.popViewController(animated: false)
                                } else {
                                    if let objVc = rootController.dataSource?.viewController(for: rootController, at: 2) as? ProTabBarController {
                                        objVc.selectedIndex = 3
                                    }

                                    rootController.scrollToPage(PageboyViewController.Page.at(index: 2), animated: false)
                                }
                            }

                        } else {
                            if let rootController = appDelegate?.window?.rootViewController as? PageboyViewController {
                                if rootController.currentIndex == 0 {
                                    rootController.navigationController?.popViewController(animated: false)
                                } else {
                                    rootController.scrollToPage(PageboyViewController.Page.first, animated: false)
                                }
                                print("test")

                            }
                        }
                        // controllr.navigationController?.pushViewController(chatController, animated: false)

                    } else {
                        if let loginNav = Controllers.LoginNavigation.controller as? UINavigationController {
                            appDelegate?.window?.rootViewController = loginNav
                        }
                    }
                }
            }
            print(userInfo)
            completionHandler(UIBackgroundFetchResult.newData)
        }
    }

    func datePhraseRelativeToToday(from date: Date, islastSeen: Bool) -> String {

        // Don't use the current date/time. Use the end of the current day
        // (technically 0h00 the next day). Apple's calculation of
        // doesRelativeDateFormatting niavely depends on this start date.
        guard let todayEnd = dateEndOfToday() else {
            return ""
        }
        let calendar = Calendar.autoupdatingCurrent
        let units = Set([Calendar.Component.year,
                            Calendar.Component.month,
                            Calendar.Component.weekOfMonth,
                            Calendar.Component.day])
        let difference = calendar.dateComponents(units, from: date, to: todayEnd)
        guard let year = difference.year,
            let month = difference.month,
            let week = difference.weekOfMonth,
            let day = difference.day else {
                return ""
        }
        let timeAgo = NSLocalizedString("%@ ago", comment: "x days ago")
        let dateFormatter: DateFormatter = {
            let formatter = DateFormatter()
            formatter.locale = Locale.autoupdatingCurrent
            formatter.dateStyle = .medium
            formatter.doesRelativeDateFormatting = true
            return formatter
        }()
        if year > 0 {
            // sample output: "Jan 23, 2014"
            return dateFormatter.string(from: date)
        } else if month > 0 {
            let formatter = DateComponentsFormatter()
            formatter.unitsStyle = .brief // sample output: "1mth"
            formatter.allowedUnits = .month
            guard let timePhrase = formatter.string(from: difference) else {
                return ""
            }
            if islastSeen == false {
                return dateFormatter.string(from: date)
            }
            return String(format: timeAgo, timePhrase)
        } else if week > 0 {
            let formatter = DateComponentsFormatter()
            formatter.unitsStyle = .brief // sample output: "2wks"
            formatter.allowedUnits = .weekOfMonth
            guard let timePhrase = formatter.string(from: difference) else {
                return ""
            }
            if islastSeen == false {
                return dateFormatter.string(from: date)
            }
            return String(format: timeAgo, timePhrase)
        } else if day > 1 {
            let formatter = DateComponentsFormatter()
            formatter.unitsStyle = .abbreviated // sample output: "3d"
            formatter.allowedUnits = .day
            guard let timePhrase = formatter.string(from: difference) else {
                return ""
            }
            if islastSeen == false {
                return dateFormatter.string(from: date)
            }
            return String(format: timeAgo, timePhrase)
        } else {
            // sample output: "Yesterday" or "Today"
            return dateFormatter.string(from: date)
        }
    }

    func dateEndOfToday() -> Date? {
        let calendar = Calendar.autoupdatingCurrent
        let now = Date()
        let todayStart = calendar.startOfDay(for: now)
        var components = DateComponents()
        components.day = 1
        let todayEnd = calendar.date(byAdding: components, to: todayStart)
        return todayEnd
    }


    // MARK: Pushkit Methods

    func registerForPushKit() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
            // Parse errors and track state
            // register for voip notifications
            let voipRegistry = PKPushRegistry(queue: DispatchQueue.main)
            voipRegistry.desiredPushTypes = Set([.voIP])
            voipRegistry.delegate = self
        }
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "ProManager")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK:- Core Data Saving support
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}


//extension AppDelegate: MessagingDelegate {
//
//    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
//        fcmDeviceToken = fcmToken
//        print(fcmToken)
//    }
//
//    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
//        fcmDeviceToken = fcmToken
//        print("Firebase registration token: \(fcmToken)")
//
//    }
//
//}

extension AppDelegate: PKPushRegistryDelegate, CXProviderDelegate {

    func retrieveToken() {
        let userId = self.activeUser?.id
        self.fetchVideoToken(["identity": userId!, "device": "iPhone", "roomName": ""]) { response, error in
            if error == nil {
                self.accessToken = response["token"] as? String ?? ""
                print("Video Calling initilization with token '\(self.accessToken)'")
                // self.retriveTokenAndStartCalling()
            } else {
                // funcs.showToast("\(error!.localizedDescription)")
            }
        }
    }

    func convertStringToDictionary(text: Data) -> NSDictionary? {
        do {
            return try JSONSerialization.jsonObject(with: text, options: []) as? NSDictionary
        } catch let error as NSError {
            print(error)
            return nil
        }
    }

    func fetchVideoToken(_ params: [String: String], completion: @escaping (NSDictionary, Error?) -> Void) {
        if let filePath = Bundle.main.path(forResource: "Keys", ofType: "plist"),
            let dictionary = NSDictionary(contentsOfFile: filePath) as? [String: AnyObject],
            let tokenRequestUrl = dictionary["VideoTokenUrl"] as? String {

            Alamofire.request(tokenRequestUrl, method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).validate()
                .responseString { response in
                    switch response.result {
                    case .success:
                        completion(self.convertStringToDictionary(text: response.data!)!, nil)
                    case .failure(let error):
                        completion(NSDictionary(), error as Error?)
                    }
            }
        }
    }

    func startVideoCallFromUser(_ params: [String: Any], completion: @escaping (NSDictionary, Error?) -> Void) {
        print(params)
        Alamofire.request((URLS.root + "sendTwillioNotification"), method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).validate()
            .responseString { response in
                print(response.description)
                switch response.result {
                case .success:
                    completion(self.convertStringToDictionary(text: response.data!)!, nil)
                case .failure(let error):
                    completion(NSDictionary(), error as Error?)
                }
        }
    }

    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, forType type: PKPushType) {
        if isOnCall == true {

        }
        else {
            let dicttt = payload.dictionaryPayload
            print(dicttt)
            self.voipDict = dicttt
            if let nameStr: String = self.voipDict["name"] as? String {
                if let timestamp: String = self.voipDict["timestamp"] as? String {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = Constants.standardDateFormate
                    let date1: NSDate = Date(timeIntervalSince1970: TimeInterval(timestamp)! / 1000) as NSDate
                    let date3: NSDate = Date() as NSDate
                    let difference = date3.timeIntervalSince(date1 as Date)
                    let minutes = (Int(difference) / 60) % 60

                    if minutes == 0 {
                        let state: UIApplicationState = UIApplication.shared.applicationState // or use  let state =  UIApplication.sharedApplication().applicationState
                        if state == .background {
                            self.openCallKit(strname: nameStr)
                            isAppActive = false
                        }
                        else if state == .active {
                            isAppActive = true
                            self.retriveTokenAndStartCalling()
                        }
                        else {
                            self.openCallKit(strname: nameStr)
                            isAppActive = false
                        }
                    }
                }
            }
        }
    }
    func convertToUTC(dateToConvert: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let convertedDate = formatter.date(from: dateToConvert)
        formatter.timeZone = TimeZone(identifier: "UTC")
        return formatter.string(from: convertedDate!)
    }

    private func pushRegistry(registry: PKPushRegistry!, didInvalidatePushTokenForType type: String!) {
        NSLog("token invalidated")
    }

    // MARK: Call Pickup Endcall delegate

    private func providerDidBegin(provider: CXProvider) {

    }

    func providerDidReset(_ provider: CXProvider) {
        provider.invalidate()
    }

    func provider(_ provider: CXProvider, perform action: CXAnswerCallAction) {
        self.callkitProvide = provider
        if isAppActive == true {
            RunLoop.current.run(until: Date(timeIntervalSinceNow: 1.0))
            let state: UIApplicationState = UIApplication.shared.applicationState // or use  let state =  UIApplication.sharedApplication().applicationState
            if state == .active {
                self.retriveTokenAndStartCalling()
            }
        }
        action.fulfill()
    }

    func provider(_ provider: CXProvider, perform action: CXEndCallAction) {
        if self.voipDict.count > 1 {
            if let _: String = self.voipDict["room"] as? String {
                //let messageRefM = FirebaseChatHelper.sharedInstance.ref.child(FirebaseChatHelper.DatabaseTableKeys.kMessageStoreKey).child(idStr.replace((self.activeUser?.id)!, with: "")).child((appDelegate?.activeUser?.id)!)
                    // messageRefM.child("isvideocancel").setValue(true)

            }
        }
        action.fulfill()
        self.voipDict = [:]
    }


    //MARK: Set Video bubble
    func setPanAndTapGestures() {
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapView(_:)))
        panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.draggedView(_:)))
        pinchZoom = UIPinchGestureRecognizer(target: self, action: #selector(self.pinchView(_:)))

        tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(self.tapView(_:)))
        panGesture2 = UIPanGestureRecognizer(target: self, action: #selector(self.draggedView(_:)))
        pinchZoom2 = UIPinchGestureRecognizer(target: self, action: #selector(self.pinchView2(_:)))

        tapGesture3 = UITapGestureRecognizer(target: self, action: #selector(self.tapView(_:)))
        panGesture3 = UIPanGestureRecognizer(target: self, action: #selector(self.draggedView(_:)))
        pinchZoom3 = UIPinchGestureRecognizer(target: self, action: #selector(self.pinchView3(_:)))

        tapGesture4 = UITapGestureRecognizer(target: self, action: #selector(self.tapView(_:)))
        panGesture4 = UIPanGestureRecognizer(target: self, action: #selector(self.draggedView(_:)))
        pinchZoom4 = UIPinchGestureRecognizer(target: self, action: #selector(self.pinchView4(_:)))

    }
    func setPanGestureinVideoBubble() {
        let width = (appDelegate?.window?.frame.size.width)

        if viewDrag == nil {
            viewDrag = UIView(frame: CGRect(x: 0, y: 50, width: width! / 4, height: width! / 4))
            self.window?.addSubview(viewDrag)
            viewDrag.layer.cornerRadius = (width! / 8)
            viewDrag.clipsToBounds = true
            viewDrag.isUserInteractionEnabled = true
            viewDrag.addGestureRecognizer(panGesture)
            viewDrag.addGestureRecognizer(pinchZoom)
        }
        if viewDrag2 == nil {
            viewDrag2 = UIView(frame: CGRect(x: (appDelegate?.window?.frame.size.width)! - 100, y: 50, width: width! / 4, height: width! / 4))
            self.window?.addSubview(viewDrag2)
            viewDrag2.layer.cornerRadius = (width! / 8)
            viewDrag2.clipsToBounds = true
            viewDrag2.isUserInteractionEnabled = true
            viewDrag2.addGestureRecognizer(panGesture2)
            viewDrag2.addGestureRecognizer(pinchZoom2)

        }
        if viewDrag3 == nil {
            viewDrag3 = UIView(frame: CGRect(x: 0, y: 200, width: width! / 4, height: width! / 4))
            self.window?.addSubview(viewDrag3)
            viewDrag3.layer.cornerRadius = (width! / 8)
            viewDrag3.clipsToBounds = true
            viewDrag3.isUserInteractionEnabled = true
            viewDrag3.addGestureRecognizer(panGesture3)
            viewDrag3.addGestureRecognizer(pinchZoom3)

        }
        if viewDrag4 == nil {
            viewDrag4 = UIView(frame: CGRect(x: 0, y: 350, width: width! / 4, height: width! / 4))
            self.window?.addSubview(viewDrag4)
            viewDrag4.layer.cornerRadius = (width! / 8)
            viewDrag4.clipsToBounds = true
            viewDrag4.isUserInteractionEnabled = true
            viewDrag4.addGestureRecognizer(panGesture4)
            viewDrag4.addGestureRecognizer(pinchZoom4)

        }
    }
    func removeVideoBubble() {
        if viewDrag != nil {
            viewDrag.removeFromSuperview()
            viewDrag = nil
        }
        if viewDrag2 != nil {
            viewDrag2.removeFromSuperview()
            viewDrag2 = nil
        }
        if viewDrag3 != nil {
            viewDrag3.removeFromSuperview()
            viewDrag3 = nil
        }
        if viewDrag4 != nil {
            viewDrag4.removeFromSuperview()
            viewDrag4 = nil
        }

    }
    func draggedView(_ sender: UIPanGestureRecognizer) {
        self.window?.bringSubview(toFront: sender.view!)
        let translation = sender.translation(in: self.window)
        sender.view!.center = CGPoint(x: sender.view!.center.x + translation.x, y: sender.view!.center.y + translation.y)
        sender.setTranslation(CGPoint.zero, in: self.window)
    }
    func pinchView(_ sender: UIPinchGestureRecognizer) {

        let trnsform = CGAffineTransform(scaleX: sender.scale, y: sender.scale)
        viewDrag.transform = trnsform
        if sender.state == .ended {
            print(viewDrag.frame)
            if self.callingVC.room != nil {
                if self.callingVC.room?.remoteParticipants.count != 0 {
                    if self.callingVC.room?.remoteParticipants[0].videoTracks[0].videoTrack?.isEnabled == true {

                    }
                    else {
                        if self.callingVC.localVideoTrack != nil {
//                            self.callingVC.previewView.removeFromSuperview()
//                            self.callingVC.localVideoTrack?.removeRenderer(self.callingVC.previewView)
//                            self.callingVC.previewView.frame = CGRect(x: 0, y: 0, width: viewDrag.frame.size.width, height: viewDrag.frame.size.height)
//                            self.callingVC.previewView.clipsToBounds = true
//                            self.callingVC.previewView?.layer.cornerRadius = viewDrag.frame.size.width / 2
//                            self.callingVC.localVideoTrack!.addRenderer(self.callingVC.previewView)
//                            viewDrag.addSubview(self.callingVC.previewView)
                        }
                    }
                }
                else {
                    if self.callingVC.localVideoTrack != nil {
//                        self.callingVC.previewView.removeFromSuperview()
//                        self.callingVC.localVideoTrack?.removeRenderer(self.callingVC.previewView)
//                        self.callingVC.previewView.frame = CGRect(x: 0, y: 0, width: viewDrag.frame.size.width, height: viewDrag.frame.size.height)
//                        self.callingVC.previewView.clipsToBounds = true
//                        self.callingVC.previewView?.layer.cornerRadius = viewDrag.frame.size.width / 2
//                        self.callingVC.localVideoTrack!.addRenderer(self.callingVC.previewView)
//                        viewDrag.addSubview(self.callingVC.previewView)
                    }
                }
            }
        }
    }
    func pinchView2(_ sender: UIPinchGestureRecognizer) {
        let trnsform = CGAffineTransform(scaleX: sender.scale, y: sender.scale)
        viewDrag2.transform = trnsform
    }
    func pinchView3(_ sender: UIPinchGestureRecognizer) {
        let trnsform = CGAffineTransform(scaleX: sender.scale, y: sender.scale)
        viewDrag3.transform = trnsform
    }
    func pinchView4(_ sender: UIPinchGestureRecognizer) {
        let trnsform = CGAffineTransform(scaleX: sender.scale, y: sender.scale)
        viewDrag4.transform = trnsform
    }
    func tapView(_ sender: UITapGestureRecognizer) {
        if let rootController = appDelegate?.window?.rootViewController as? PageboyViewController {
            if let _ = rootController.currentViewController as? CameraPageNavigationController {
                rootController.scrollToPage(PageboyViewController.Page.next, animated: true)
            }
        }
        NotificationCenter.default.post(name: NSNotification.Name(Constants.removeCameraController), object: nil, userInfo: nil)

        if self.viewDrag != nil {
            self.viewDrag.isHidden = true
        }
        if self.viewDrag2 != nil {
            self.viewDrag2.isHidden = true
        }
        if self.viewDrag3 != nil {
            self.viewDrag3.isHidden = true
        }
        if self.viewDrag4 != nil {
            self.viewDrag4.isHidden = true
        }
        if self.callingVC != nil {
            self.callingVC.view.isHidden = false
            if self.callingVC.room != nil {
                if self.callingVC.room?.remoteParticipants.count != 0 {
                    self.callingVC.checkVideoCaller()
                    self.callingVC.isMenuHidden = true
                    self.callingVC.viewAction.isHidden = true
                    self.callingVC.showHideMenu()
                    self.callingVC.viewAddMember.isHidden = true
                }
                else {
                    RunLoop.current.run(until: Date.init(timeIntervalSinceNow: 0.2))
                    self.callingVC.displayPreviewView()
                }
            }
            self.window?.bringSubview(toFront: self.callingVC.view)
        }
        if self.objVideoAdVC != nil {
            self.objVideoAdVC.view.removeFromSuperview()
        }
    }

    // MARK: Call End
    func performEndCallAction() {
        if self.calluuid != nil {
            let endCallAction = CXEndCallAction(call: self.calluuid!)

            let transaction = CXTransaction(action: endCallAction)

            let callController = CXCallController()

            callController.request(transaction) { error in

                if let error = error {

                    print("Error requesting transaction: \(error)")

                } else {

                    print("Requested transaction successfully")

                    // self.performEndCallProcess()

                }

            }
        }
    }

    func pushRegistry(_ registry: PKPushRegistry, didUpdate pushCredentials: PKPushCredentials, forType type: PKPushType) {
        print("Hii +++ " + pushCredentials.token.map { String(format: "%02.2hhx", $0) }.joined())
        self.voipPushToken = pushCredentials.token.map { String(format: "%02.2hhx", $0) }.joined()
        if self.activeUser != nil {
            FirebaseChatHelper.sharedInstance.createOrUpdateUserInFirebase(online: true, userObj: (appDelegate?.activeUser)!, isLogout: false)
            self.retrieveToken()
        }
    }

    func openCallKit(strname: String) {
        let config = CXProviderConfiguration(localizedName: "\(AppSetting.appName)")
        config.maximumCallsPerCallGroup = 1
        // config.iconTemplateImageData = UIImagePNGRepresentation(UIImage(named: "Test")!)
        config.ringtoneSound = "ringtone.caf"
        if #available(iOS 11.0, *) {
            config.includesCallsInRecents = true
        } else {
            // Fallback on earlier versions
        }
        config.supportsVideo = true

        let provider = CXProvider(configuration: config)
        provider.setDelegate(self, queue: nil)
        let update = CXCallUpdate()
        update.remoteHandle = CXHandle(type: .generic, value: strname)
        update.hasVideo = true
        // update.supportsGrouping = true
        self.calluuid = UUID()
        provider.reportNewIncomingCall(with: self.calluuid!, update: update, completion: { error in
        })
    }

    func retriveTokenAndStartCalling() {
        if self.voipPushToken.count == 0 {
            self.retrieveToken()
        }
        if self.activeUser != nil {
            if self.voipDict.count > 1 {
                if let idStr: String = self.voipDict["room"] as? String {
                    if callingVC != nil {
                        callingVC = nil
                    }
                    callingVC = Storyboards.ChatMain.storyboard.instantiateViewController(withIdentifier: "CallingVC") as? CallingVC
                    let objCall = Call(JSON: [:])
                    objCall?.toId = [""]
                    objCall?.fromId = ""
                    objCall?.roomID = idStr
                    if let groupImgstr: String = self.voipDict["isgroup"] as? String {
                        objCall?.imageURL = groupImgstr
                    }
                    else {
                        objCall?.imageURL = ""
                    }
                    callingVC.objCall = objCall
                    callingVC.chatObj?.id = idStr.replace((self.activeUser?.id)!, with: "")
                    callingVC.isDirectRoomJoin = true
                    callingVC.isCallNeedsAccept = isAppActive
                    if let nameStr: String = self.voipDict["name"] as? String {
                        callingVC.strName = nameStr
                    }
                    if let groupstr: String = self.voipDict["isgroup"] as? String {
                        if groupstr == "false" || groupstr.count == 0 {

                        } else {
                            callingVC.isGroup = true
                        }
                    }
                    callingVC.view.isHidden = false
                    self.window?.addSubview(callingVC.view)
                }
            }
        }
    }
    func getThumbnailFrom(path: URL) -> UIImage? {
        
        do {
            
            let asset = AVURLAsset(url: path , options: nil)
            let imgGenerator = AVAssetImageGenerator(asset: asset)
            imgGenerator.appliesPreferredTrackTransform = true
            let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(0, 1), actualTime: nil)
            let thumbnail = UIImage(cgImage: cgImage)
            
            return thumbnail
            
        } catch let error {
            
            print("*** Error generating thumbnail: \(error.localizedDescription)")
            return nil
            
        }
        
    }
}
// protocol Addable {
//    static func +(lhs: Self, rhs: Self) -> Self
// }
//
// extension Int: Addable {}
// extension Double: Addable {}
// extension Float: Addable {}
// extension String: Addable {}
